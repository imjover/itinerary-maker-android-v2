/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.exception.RemoteAccessException
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.view.login.AbstractLoginActivity
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [AbstractLoginActivity].
 */
abstract class AbstractLoginViewModel : BaseLoginViewModel() {

    @Inject lateinit var userManager: UserManager

    private val _loginInProgress = MutableLiveData<Boolean>()
    val loginInProgress: LiveData<Boolean> get() = _loginInProgress

    init {
        injectDependencies()
    }

    /**
     * Try to login a user.
     *
     * @param username The username of the user.
     * @param password The password of the user.
     */
    fun login(username: String, password: String) {
        viewModelScope.launch {
            _loginInProgress.value = true
            try {
                _isLoggedIn.value = userManager.findUserByLogin(username, password)
            } catch (e: RemoteAccessException) {
                _errorMessage.value = e.message
            }
            _loginInProgress.value = false
        }
    }
}