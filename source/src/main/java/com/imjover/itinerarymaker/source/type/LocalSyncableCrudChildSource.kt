/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.type

import com.imjover.itinerarymaker.entity.SyncableEntity

/**
 * Defines local sources that are syncable, has CRUD, and is a child.
 */
interface LocalSyncableCrudChildSource<SE : SyncableEntity, ParentId> :
    LocalSyncableSource<SE, ParentId>,
    LocalCrudSource<SE>,
    ChildSource<SE, ParentId> // TODO : I think ChildSource#getChildrenOf is NOT used anymore?