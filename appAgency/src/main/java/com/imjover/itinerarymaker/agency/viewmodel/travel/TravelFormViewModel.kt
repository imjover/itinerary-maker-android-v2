/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.travel

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.travel.TravelFormDialog
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.manager.TravelManager
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject


/**
 * The view model of [TravelFormDialog].
 */
class TravelFormViewModel : AbstractViewModel() {

    @Inject lateinit var travelManager: TravelManager
    lateinit var parentUserId: Array<String>

    private val _oldTravel: MutableLiveData<Travel> = MutableLiveData()
    val oldTravel: LiveData<Travel>
        get() = _oldTravel

    private val _dismiss: MutableLiveData<String?> = MutableLiveData()
    val dismiss: LiveData<String?>
        get() = _dismiss

    val isLoading = ObservableBoolean()

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Retrieves the details of the [Travel].
     *
     * @param travelId The unique ID of the travel.
     */
    fun getTravel(travelId: String) {
        viewModelScope.launch {
            _oldTravel.value = travelManager.getTravel(parentUserId, travelId)
        }
    }

    /**
     * Inserts a [Travel].
     *
     * @param description The label to be used for this record.
     * @param startDate The date when the travel should start.
     * @param endDate The date when the travel should have ended.
     */
    fun insert(description: String, startDate: Date, endDate: Date) {
        viewModelScope.launch {
            isLoading.set(true)
            val travelId = travelManager.insert(
                parentUserId,
                description,
                startDate,
                endDate
            )
            _dismiss.value = travelId
            isLoading.set(false)
        }
    }

    /**
     * Updates a [Travel].
     *
     * @param description The label to be used for this record.
     * @param startDate The date when the travel should start.
     * @param endDate The date when the travel should have ended.
     */
    fun update(description: String, startDate: Date, endDate: Date) {
        viewModelScope.launch {
            isLoading.set(true)
            travelManager.update(parentUserId, oldTravel.value!!, description, startDate, endDate)
            _dismiss.value = null
            isLoading.set(false)
        }
    }
}