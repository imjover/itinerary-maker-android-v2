/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * Current state of the synchronization.
 *
 * @param id The unique ID.
 */
enum class SyncStatus(val id: Int) {

    /**
     * The record is clean and currently up-to-date.
     */
    CLEAN(0),

    /**
     * The record needs to be inserted on the remote source.
     */
    TO_INSERT(1),

    /**
     * The record needs to be updated on the remote source.
     */
    TO_UPDATE(2),

    /**
     * The record needs to be deleted on the remote and local source.
     */
    TO_DELETE(3);


    companion object {
        /**
         * Retrieves the equivalent [SyncStatus] base on the given ID.
         *
         * @param id The id of the [SyncStatus].
         * @return The retrieved [SyncStatus].
         */
        fun fromId(id: Int): SyncStatus {
            values().forEach {
                if (it.id == id) {
                    return it
                }
            }
            throw IllegalStateException()
        }
    }
}