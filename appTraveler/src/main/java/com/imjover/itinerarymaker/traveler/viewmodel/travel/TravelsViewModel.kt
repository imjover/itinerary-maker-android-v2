/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.viewmodel.travel

import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.manager.NotificationManager
import com.imjover.itinerarymaker.manager.SyncableManager
import com.imjover.itinerarymaker.manager.TravelManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.traveler.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.traveler.view.travel.TravelsFragment
import com.imjover.itinerarymaker.viewmodel.AbstractSyncableViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [TravelsFragment].
 */
class TravelsViewModel : AbstractSyncableViewModel<Travel>() {

    @Inject lateinit var travelManager: TravelManager
    @Inject lateinit var userManager: UserManager
    @Inject lateinit var notificationManager: NotificationManager
    private val parentIds: Array<String>

    init {
        ViewModelComponentFactory.singleton().inject(this)
        parentIds = arrayOf(userManager.getLoggedInUser().id)
        viewModelScope.launch {
            notificationManager.syncChildrenOf(emptyArray())
        }
    }

    /**
     * Retrieves the total count of new notifications of the currently logged-in user
     * New notifications are those that [Notification.isSeen] is false.
     *
     * @return Stream of total count.
     */
    fun getNewNotificationCount(): Flow<Int> =
        notificationManager.getNewNotificationCount()


    /***********************************************************************************************
     * Required functions of [AbstractSyncableViewModel].
     **********************************************************************************************/

    override fun getParentIds(): Array<String> =
        parentIds

    override fun getSyncableManager(): SyncableManager<Travel> =
        travelManager
}