/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency

import android.content.Context
import androidx.work.WorkerParameters
import com.imjover.itinerarymaker.AbstractUpdateDeviceTokenWorker
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory


class UpdateDeviceTokenWorkerImpl(appContext: Context, workerParams: WorkerParameters) :
    AbstractUpdateDeviceTokenWorker(appContext, workerParams) {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }
}