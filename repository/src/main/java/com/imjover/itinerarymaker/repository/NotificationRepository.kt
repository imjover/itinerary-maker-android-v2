/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository

import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.User
import kotlinx.coroutines.flow.Flow


/**
 * Represents the repository data source of [Notification].
 */
interface NotificationRepository {

    /**
     * Retrieves the total count of new notifications.
     * New notifications are those that [Notification.isSeen] is false.
     *
     * @param recipient The [User] who receives the notification.
     * @return Stream of total count.
     */
    fun getNewNotificationCountOf(recipient: String): Flow<Int>

    /**
     * Retrieves all notifications of the [recipient].
     *
     * @param recipient The [User] who receives the notification.
     * @return Stream of notifications.
     */
    fun getNotificationsOf(recipient: String): Flow<Notification>

    /**
     * Synchronizes notifications of the [recipient].
     *
     * @param recipient The [User] who receives the notification.
     */
    @Deprecated("Firebase automatically syncs Firestore")
    suspend fun syncNotificationsOf(recipient: String)

    /**
     * Set the given notification as seen.
     *
     * @param travelerId The ID of the traveler.
     * @param id The unique ID of the notification.
     */
    suspend fun setAsSeen(travelerId: String, id: String)
}