/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.helper

import androidx.annotation.ColorRes
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.entity.Status


/**
 * Retrieves the equivalent Color resource of the [Status].
 */
@ColorRes
fun Status.toColorRes(): Int =
    when (this) {
        Status.OK -> R.color.text_colored_positive
        Status.IN_PROGRESS -> R.color.text_colored_neutral
        Status.FOR_REVIEW -> R.color.text_colored_neutral
        Status.REVIEWED_OK -> R.color.text_colored_positive
        Status.REVIEWED_DECLINED -> R.color.text_colored_negative
        // End state
        Status.PAID -> R.color.text_colored_positive_secondary
        Status.DELETED -> R.color.text_colored_neutral
        Status.CANCELLED -> R.color.text_colored_negative
        else -> throw IllegalStateException()
    }