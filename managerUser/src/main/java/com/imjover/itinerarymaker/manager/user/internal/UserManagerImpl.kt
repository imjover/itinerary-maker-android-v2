/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.user.internal

import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.repository.UserRepository
import com.imjover.itinerarymaker.repository.di.RepositoryFactory
import com.imjover.itinerarymaker.util.Result
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


internal class UserManagerImpl @Inject constructor() : UserManager {

    private val userRepository: UserRepository =
        RepositoryFactory.singleton().getUserRepository()

    override suspend fun findUserByLogin(username: String, password: String): Result<User> =
        userRepository.findUserByLogin(username, password)

    override suspend fun logout(): Result<Unit> =
        userRepository.logout()

    override suspend fun register(userType: UserType): Result<String> =
        userRepository.register(userType)

    override suspend fun approveRegistration(user: User): Result<String> =
        userRepository.approveRegistration(user)

    override fun getLoggedInUser(): User =
        userRepository.getLoggedInUser()

    override suspend fun getUser(id: String): Result<User> =
        userRepository.getUser(id)

    override fun getUsers(nameQuery: String, vararg userTypes: UserType): Flow<User> =
        userRepository.getUsers(nameQuery, *userTypes)

    override suspend fun updateDeviceToken(token: String): Result<Unit> =
        userRepository.updateDeviceToken(token)
}