/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.about

import android.os.Bundle
import android.view.View
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.FragmentAboutBinding
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.viewmodel.about.AboutViewModel


/**
 * The screen that holds all information about the app.
 */
class AboutFragment : AbstractFragment<AboutViewModel, FragmentAboutBinding>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadAboutInfo()
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_about

    override fun getViewModelClass(): Class<AboutViewModel> =
        AboutViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}
