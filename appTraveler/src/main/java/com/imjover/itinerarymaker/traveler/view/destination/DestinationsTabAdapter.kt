/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.destination

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.view.destination.AbstractDestinationListFragment
import com.imjover.itinerarymaker.view.destination.AbstractDestinationMapFragment


/**
 * Tab adapter for [AbstractDestinationListFragment] and [AbstractDestinationMapFragment].
 *
 * @param parentIds The parent [Itinerary] of the destination to be displayed.
 */
internal class DestinationsTabAdapter(fragment: Fragment, private val parentIds: Array<String>) :
    FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment =
        when (position) {
            0 -> DestinationListFragment.newInstance(parentIds)
            1 -> DestinationMapFragment.newInstance(parentIds)
            else -> throw IllegalStateException()
        }
}