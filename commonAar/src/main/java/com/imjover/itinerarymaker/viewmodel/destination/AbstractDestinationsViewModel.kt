/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.destination

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.manager.DestinationManager
import com.imjover.itinerarymaker.manager.ItineraryManager
import com.imjover.itinerarymaker.view.destination.AbstractDestinationsFragment
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import com.imjover.itinerarymaker.viewmodel.InjectableViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [AbstractDestinationsFragment].
 */
abstract class AbstractDestinationsViewModel : AbstractViewModel(), InjectableViewModel {

    @Inject lateinit var itineraryManager: ItineraryManager
    @Inject lateinit var destinationManager: DestinationManager
    lateinit var parentIds: Array<String>
    private val _destination = MutableLiveData<Destination>()
    internal val destination: LiveData<Destination>
        get() = _destination

    init {
        injectDependencies()
    }

    /**
     * Retrieves the stream of [Itinerary].
     *
     * @return Stream of [Itinerary].
     */
    fun getItineraryAsStream(): Flow<Itinerary> {
        return itineraryManager.getItineraryAsStream(
            parentIds.dropLast(1).toTypedArray(),
            parentIds.last()
        )
    }

    /**
     * Retrieves the [Destination].
     *
     * @param id The ID of the [Destination].
     */
    fun getDestination(id: String) {
        viewModelScope.launch {
            _destination.value = destinationManager.getDestination(parentIds, id)
        }
    }
}