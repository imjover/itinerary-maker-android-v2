/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * Defines that an [Entity] can be synced in both remote and local sources.
 */
interface SyncableEntity : Entity {

    /**
     * Retrieves the sync status.
     *
     * Note: This is intentionally named `getSyncableStatus` (instead of `getSyncStatus`) because it
     * clashes with the implementing object's getter of [SyncStatus] (named also `getSyncStatus`).
     *
     * @return The sync status.
     */
    fun getSyncableStatus(): SyncStatus

    /**
     * Sets the arguments then creates a clone.
     *
     * @return The clone of this object.
     */
    fun cloneAndSetArg(newSyncStatus: SyncStatus): SyncableEntity

    /**
     * Sets the arguments then creates a clone.
     *
     * @return The clone of this object.
     */
    fun cloneAndSetArg(newId: String, newSyncStatus: SyncStatus): SyncableEntity
}