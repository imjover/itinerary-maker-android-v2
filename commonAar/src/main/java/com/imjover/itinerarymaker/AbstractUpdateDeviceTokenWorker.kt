/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.imjover.itinerarymaker.manager.UserManager
import kotlinx.coroutines.runBlocking
import javax.inject.Inject


abstract class AbstractUpdateDeviceTokenWorker(
    appContext: Context,
    workerParams: WorkerParameters
) :
    Worker(appContext, workerParams) {

    @Inject
    lateinit var userManager: UserManager

    init {
        injectDependencies()
    }

    abstract fun injectDependencies()

    override fun doWork(): Result {
        Log.d(toString(), "Performing job to update device token")
        val token = inputData.getString("TOKEN") ?: return Result.failure()

        return runBlocking {
            try {
                userManager.getLoggedInUser()
            } catch (e: NullPointerException) {
                // If exception is caught, then the user has not logged-in yet, please retry.
                return@runBlocking Result.retry()
            }
            val result = userManager.updateDeviceToken(token)
            if (result.isSuccess()) {
                return@runBlocking Result.success()
            } else {
                return@runBlocking Result.failure()
            }
        }
    }

    override fun toString(): String =
        if (BuildConfig.DEBUG) "UpdateDeviceTokenWorker" else super.toString()
}