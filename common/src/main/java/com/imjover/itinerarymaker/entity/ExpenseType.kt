/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * The types of expenses.
 *
 * @param id The unique ID.
 */
enum class ExpenseType(val id: String) {

    ACCOMMODATION("accom"),
    TRANSPORTATION("transpo"),
    FOOD("food"),
    VISA("visa"),
    TOUR("tour"),
    OTHERS("others");


    companion object {
        /**
         * Retrieves the equivalent [ExpenseType] base on the given ID.
         *
         * @param id The id of the [ExpenseType].
         * @return The retrieved [ExpenseType].
         */
        fun fromId(id: String): ExpenseType {
            values().forEach {
                if (it.id == id) {
                    return it
                }
            }
            throw IllegalStateException()
        }
    }
}