/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.traveler

import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.traveler.TravelersFragment
import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.manager.NotificationManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [TravelersFragment].
 */
class TravelersViewModel : AbstractViewModel() {

    @Inject lateinit var userManager: UserManager
    @Inject lateinit var notificationManager: NotificationManager

    init {
        ViewModelComponentFactory.singleton().inject(this)
        viewModelScope.launch {
            notificationManager.syncChildrenOf(emptyArray())
        }
    }

    /**
     * Retrieves all travelers.
     *
     * @param nameQuery The name to be searched for.
     * @return Stream of [User]s.
     */
    fun getTravelers(nameQuery: String): Flow<User> =
        userManager.getUsers(nameQuery, UserType.TRAVELER)


    /**
     * Retrieves the total count of new notifications of the currently logged-in user
     * New notifications are those that [Notification.isSeen] is false.
     *
     * @return Stream of total count.
     */
    fun getNewNotificationCount(): Flow<Int> =
        notificationManager.getNewNotificationCount()
}