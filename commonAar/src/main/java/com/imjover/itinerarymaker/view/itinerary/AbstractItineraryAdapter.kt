/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.itinerary

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imjover.itinerarymaker.databinding.RowItineraryBinding
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter

/**
 * List adapter of [Itinerary] item.
 */
abstract class AbstractItineraryAdapter :
    UpdatableRecyclerViewAdapter<Itinerary, RowItineraryBinding>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterViewHolder<RowItineraryBinding> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = RowItineraryBinding.inflate(layoutInflater, parent, false)
        return AdapterViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: AdapterViewHolder<RowItineraryBinding>, position: Int) {
        val itinerary = getData(position)
        val showDivider = position + 1 < itemCount
        // Bind instances of [Itinerary] to the row.
        holder.binding.itinerary = itinerary
        holder.binding.divider.visibility = if (showDivider) View.VISIBLE else View.GONE
        holder.binding.container.setOnClickListener(getOnClickListener(itinerary.id))
        holder.binding.executePendingBindings()
    }
}