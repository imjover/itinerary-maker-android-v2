/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source.internal

import android.util.Log
import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.repository.NotificationRepository
import com.imjover.itinerarymaker.source.local.NotificationLocalSource
import com.imjover.itinerarymaker.source.remote.NotificationRemoteSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext


class NotificationRepositoryImpl(
    private val localSource: NotificationLocalSource,
    private val remoteSource: NotificationRemoteSource,
    private val dispatcher: CoroutineDispatcher
) : NotificationRepository {

    override fun getNewNotificationCountOf(recipient: String): Flow<Int> =
        localSource.getNewNotificationCountOf(recipient)

    override fun getCachedNotificationsOf(recipient: String): Flow<List<Notification>> =
        localSource.getStreamChildrenOf(recipient)

    override suspend fun syncNotificationsOf(recipient: String) {
        withContext(dispatcher) {
            // Pull latest data from remote source.
            val remoteNotifications = remoteSource.getListChildrenOf(recipient)

            // TODO: Implement handling of different dirty records coming from multiple offline devices.
            //  Compare clean records vs records that are dirty (and has temporary ID?).
            // Imagine two devices are offline for a while but used at the same time that created
            //  multiple new records (dirty or has temporary id), then both were attempted to synced
            //  records at the server.

            // Insert or update data cached in local source.
            remoteNotifications.forEach {
                localSource.insertOrUpdate(it)
            }

            // Synchronize ALL dirty records from local to remote source.
            try {
                val localNotifications = localSource.getListChildrenOf(recipient, SyncStatus.TO_UPDATE)
                remoteSource.setAsSeen(localNotifications.map { it.getUniqueId() })
                localNotifications.forEach {
                    localSource.update(it.cloneAndSetArg(SyncStatus.CLEAN))
                }
            } catch (exception: Exception) {
                // FIXME: Change exception to remote-caused exception ONLY e.g. no internet
                Log.e(toString(), exception.message, exception)
            }

            // Delete cached travels that are deleted already in remote source
            localSource.getListChildrenOf(recipient, SyncStatus.CLEAN)
                .subtract(remoteNotifications)
                .forEach { localSource.delete(it) }
        }
    }

    override suspend fun setAsSeen(id: String) {
        withContext(dispatcher) {
            val updated = try {
                remoteSource.setAsSeen(listOf(id))
                true
            } catch (exception: Exception) {
                // FIXME: Change exception to remote-caused exception ONLY e.g. no internet
                Log.e(toString(), exception.message, exception)
                false
            }
            val syncStatus = if (updated) SyncStatus.CLEAN else SyncStatus.TO_UPDATE
            localSource.setAsSeen(id, syncStatus)
        }
    }
}