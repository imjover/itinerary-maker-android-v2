/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.travel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.util.Pair
import androidx.navigation.fragment.findNavController
import com.google.android.material.datepicker.MaterialDatePicker
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.DialogTravelFormBinding
import com.imjover.itinerarymaker.agency.viewmodel.travel.TravelFormViewModel
import com.imjover.itinerarymaker.util.DateFormat
import com.imjover.itinerarymaker.util.toStringFormat
import com.imjover.itinerarymaker.view.AbstractDialogFragment
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.FormOption
import com.imjover.itinerarymaker.view.widget.validator.FormValidator
import com.imjover.itinerarymaker.view.widget.validator.NonEmptyValidator
import java.util.*


/**
 * The screen that holds the form to add or edit travels.
 */
class TravelFormDialog : AbstractDialogFragment<TravelFormViewModel, DialogTravelFormBinding>() {

    private lateinit var formValidator: FormValidator
    private lateinit var formOption: FormOption
    private lateinit var startDate: Date
    private lateinit var endDate: Date

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        isCancelable = false
        dataBinding.tvTravelDate.setOnClickListener {
            val builder = MaterialDatePicker.Builder.dateRangePicker()
            builder.setSelection(Pair(startDate.time, endDate.time))
            val materialDatePicker = builder.build()
            materialDatePicker.addOnNegativeButtonClickListener { materialDatePicker.dismiss() }
            materialDatePicker.addOnPositiveButtonClickListener {
                updateTravelDates(Date(it.first!!), Date(it.second!!))
            }
            materialDatePicker.show(
                requireActivity().supportFragmentManager,
                materialDatePicker.toString()
            )
        }
        dataBinding.btnSave.setOnClickListener {
            save()
        }
        dataBinding.btnCancel.setOnClickListener {
            dismiss()
        }

        // Setup validators
        val nonEmptyValidator = NonEmptyValidator()
        formValidator = FormValidator()
        formValidator.addValidator(dataBinding.etName, nonEmptyValidator)
        formValidator.addValidator(dataBinding.tvTravelDate, nonEmptyValidator)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Get the arguments needed
        val args = TravelFormDialogArgs.fromBundle(requireArguments())
        formOption = args.formOption
        viewModel.parentUserId = args.parentIds
        when (formOption) {
            FormOption.ADD -> {
                if (args.travelId != null) {
                    throw IllegalStateException()
                }
                val now = Date()
                startDate = now
                endDate = now
            }
            FormOption.EDIT -> {
                if (args.travelId == null) {
                    throw IllegalStateException()
                }
                viewModel.getTravel(args.travelId)
            }
        }
        // Setup observers on view model
        viewModel.dismiss.observe(this, { travelId ->
            when (formOption) {
                FormOption.ADD -> {
                    val action =
                        TravelFormDialogDirections.actionTravelFormFragmentToItinerariesFragment(
                            viewModel.parentUserId.plus(travelId!!), true
                        )
                    findNavController().navigate(action)
                }
                FormOption.EDIT -> {
                    // Do nothing because the calling Fragment/Activity should collect from stream.
                }
            }
            dismiss()
        })
        viewModel.oldTravel.observe(this, {
            updateTravelDates(it.startDate, it.endDate)
        })
    }

    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Updates and shows the travel dates in the UI.
     *
     * @param startDate The start of date range.
     * @param endDate The end of date range.
     */
    private fun updateTravelDates(startDate: Date, endDate: Date) {
        this.startDate = startDate
        this.endDate = endDate
        dataBinding.tvTravelDate.text =
            getString(
                R.string.travel_form_fragment__travel_date__format,
                this.startDate.toStringFormat(DateFormat.DATE_FORMAT),
                this.endDate.toStringFormat(DateFormat.DATE_FORMAT)
            )
    }

    /**
     * Handles the saving process of the inputs.
     */
    private fun save() {
        if (!formValidator.validateAll()) {
            return
        }
        val description = dataBinding.etName.text.toString().trim()
        when (formOption) {
            FormOption.ADD -> viewModel.insert(description, startDate, endDate)
            FormOption.EDIT -> viewModel.update(description, startDate, endDate)
        }
    }

    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.dialog_travel_form

    override fun getViewModelClass(): Class<TravelFormViewModel> =
        TravelFormViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}