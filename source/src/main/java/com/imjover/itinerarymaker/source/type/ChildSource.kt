/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.type

import com.imjover.itinerarymaker.entity.Entity

/**
 * Defines sources that has parents.
 */
interface ChildSource<E: Entity, ParentId> {

    /**
     * Retrieves all children of a given [ParentId].
     *
     * @param parentId The ID of the parent.
     */
    suspend fun getListChildrenOf(parentId: ParentId): List<E>
}