/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.internal

import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.source.local.UserLocalSource
import com.imjover.itinerarymaker.source.local.room.DatabaseFactory
import com.imjover.itinerarymaker.source.local.room.dao.UserDao
import kotlinx.coroutines.flow.Flow

internal class UserLocalSourceImpl constructor(
    private val userDao: UserDao = DatabaseFactory.getDatabase().userDao()
) : UserLocalSource {

    override suspend fun insertOrUpdate(vararg users: User) {
        userDao.insertOrUpdate(*users)
    }

    override fun getUsers(nameQuery: String, vararg userTypes: UserType): Flow<User> =
        userDao.getUsers(nameQuery, *userTypes)
}