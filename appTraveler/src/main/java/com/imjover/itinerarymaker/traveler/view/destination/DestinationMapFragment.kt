/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.destination

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.traveler.BuildConfig
import com.imjover.itinerarymaker.traveler.R
import com.imjover.itinerarymaker.traveler.databinding.FragmentDestinationMapBinding
import com.imjover.itinerarymaker.traveler.viewmodel.destination.DestinationMapViewModel
import com.imjover.itinerarymaker.util.PermissionUtil.Companion.isPermissionGranted
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.destination.ARG_PARENT_IDS
import com.imjover.itinerarymaker.view.destination.AbstractDestinationMapFragment


/**
 * The screen that holds the MAP representation of all destinations.
 */
class DestinationMapFragment :
    AbstractDestinationMapFragment<DestinationMapViewModel, FragmentDestinationMapBinding>() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private var nextPriorityDestination: Destination? = null
    private var currentlyDisplayedDestination: Destination? = null
    private var requestingLocationUpdates = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        updateValuesFromBundle(savedInstanceState)

        if (!isPermissionsAllowed()) {
            // Permission to access the location is missing. Show rationale and request permission
            dataBinding.btnShareLocation.visibility = View.VISIBLE
        } else {
            requestingLocationUpdates = true
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.showMessage.observe(viewLifecycleOwner, {
            it.consume { msg ->
                Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show()
            }
        })
        dataBinding.btnShareLocation.setOnClickListener {
            requestPermission()
        }
    }

    override fun onResume() {
        super.onResume()
        if (requestingLocationUpdates) {
            startLocationUpdates()
        }
    }

    override fun onPause() {
        super.onPause()
        if (requestingLocationUpdates) {
            stopLocationUpdates()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, requestingLocationUpdates)
        super.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        if (isPermissionsAllowed()) {
            googleMap.isMyLocationEnabled = true
        }
        super.onMapReady(googleMap)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return
        }
        if (isPermissionGranted(
                permissions,
                grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            // Enable the my location layer if the permission has been granted.
            requestingLocationUpdates = true
            dataBinding.btnShareLocation.visibility = View.GONE
            startLocationUpdates()
        } else {
            // Permission was denied. Display an error message
            // Display the missing permission error dialog when the fragments resume.
            dataBinding.btnShareLocation.visibility = View.VISIBLE
        }
    }


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Inform the user the he/she arrived at his destination.
     */
    private fun informUserArrivedAtDestination(
        destination: Destination,
        nextPriorityDestination: Destination?
    ) {
        if (currentlyDisplayedDestination?.id == destination.id) {
            return
        }
        Log.i(toString(), "Destination visited: ${destination.id}, ${destination.name}")
        viewModel.setDestinationAsVisited(destination.id)
        this.nextPriorityDestination = nextPriorityDestination
        currentlyDisplayedDestination = destination
        context?.let {
            // FIXME: java.lang.IllegalStateException: Fragment DestinationMapFragment not attached to a context.
            //  Apparently, sometimes the context here is null, even though the map is already loaded.
            //  So supposedly, the fragment itself is also loaded.
            AlertDialog.Builder(it)
                .setMessage(
                    getString(R.string.destination_map_fragment__user_arrived__info, destination.name)
                )
                .setPositiveButton(R.string.button__confirm) { _, _ ->
                    currentlyDisplayedDestination = null
                }.show()
        }
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private fun isPermissionsAllowed(): Boolean =
        PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            requireContext(), Manifest.permission.ACCESS_FINE_LOCATION
        )

    /**
     * Requests permission.
     */
    private fun requestPermission() {
        requestPermissions(
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            LOCATION_PERMISSION_REQUEST_CODE
        )
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        if (!isPermissionsAllowed()) {
            return
        }
        getMap()?.let {
            it.isMyLocationEnabled = true
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (currentLocation in locationResult.locations) {
                    Log.i(
                        toString(),
                        "Location: ${currentLocation.latitude}, ${currentLocation.longitude}"
                    )

                    nextPriorityDestination?.let {
                        val priorityNextLocation = Location("priority").apply {
                            latitude = it.latitude
                            longitude = it.longitude
                        }
                        if (currentLocation.distanceTo(priorityNextLocation) <= VISITED_MIN_DISTANCE) {
                            informUserArrivedAtDestination(it, null)
                            return
                        }
                    }

                    // Check other location if it's near
                    with(viewModel.getUnvisitedDestinations()) {
                        forEachIndexed { index, destination ->
                            val location = Location("temp").apply {
                                latitude = destination.latitude
                                longitude = destination.longitude
                            }
                            if (currentLocation.distanceTo(location) <= VISITED_MIN_DISTANCE) {
                                informUserArrivedAtDestination(
                                    destination, if (size == 1) {
                                        null
                                    } else {
                                        this[if (size == index + 1) 0 // Get the 1st item
                                        else index + 1 // Get the next item after the current
                                        ]
                                    }
                                )
                                return
                            }
                        }
                        if (isNotEmpty()) {
                            nextPriorityDestination = first()
                        }
                    }
                }
            }
        }
        val locationRequest = LocationRequest.create().apply {
            interval = 60000
            fastestInterval = 30000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        savedInstanceState ?: return

        // Update the value of requestingLocationUpdates from the Bundle.
        if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
            requestingLocationUpdates = savedInstanceState.getBoolean(
                REQUESTING_LOCATION_UPDATES_KEY
            )
        }
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getViewModelClass(): Class<DestinationMapViewModel> =
        DestinationMapViewModel::class.java

    override fun getLayout(): Int =
        R.layout.fragment_destination_map

    override fun getMapFragment(): SupportMapFragment =
        childFragmentManager.findFragmentById(R.id.fragment_map) as SupportMapFragment


    /***********************************************************************************************
     * Others
     **********************************************************************************************/

    override fun toString(): String =
        if (BuildConfig.DEBUG) "DestinationMapFragment" else super.toString()

    companion object {
        /**
         * Creates an instance of this fragment.
         *
         * @param itineraryId The parent [Itinerary] of the destination to be displayed.
         */
        fun newInstance(itineraryId: Array<String>) =
            DestinationMapFragment().apply {
                arguments = Bundle().apply {
                    putStringArray(ARG_PARENT_IDS, itineraryId)
                }
            }

        /**
         * Request code for location permission request.
         *
         * @see .onRequestPermissionsResult
         */
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1

        private const val REQUESTING_LOCATION_UPDATES_KEY = "REQUESTING_LOCATION_UPDATES_KEY"

        /**
         * Minimum distance (in meters) to treat a location to be visited.
         */
        private const val VISITED_MIN_DISTANCE = 100
    }
}