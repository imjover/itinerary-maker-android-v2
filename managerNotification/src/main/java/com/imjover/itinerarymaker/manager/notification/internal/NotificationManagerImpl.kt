/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.notification.internal

import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.manager.NotificationManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.repository.NotificationRepository
import com.imjover.itinerarymaker.repository.di.RepositoryFactory
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject


internal class NotificationManagerImpl @Inject constructor() : NotificationManager {

    @Inject lateinit var userManager: UserManager

    private val notificationRepository: NotificationRepository =
        RepositoryFactory.singleton().getNotificationRepository()


    /***********************************************************************************************
     * Required functions of [NotificationManager].
     **********************************************************************************************/

    override fun getNewNotificationCount(): Flow<Int> {
        val loggedInUser = userManager.getLoggedInUser().id
        return notificationRepository.getNewNotificationCountOf(loggedInUser)
    }

    override suspend fun setAsSeen(userId: String, id: String) {
        notificationRepository.setAsSeen(userId, id)
    }


    /***********************************************************************************************
     * Sync-able related functions.
     **********************************************************************************************/

    override fun getStreamChildrenOf(parentIds: Array<String>): Flow<Notification> {
        if (parentIds.isNotEmpty()) {
            throw IllegalStateException("Only notification of currently logged-in user is viewable.")
        }
        val loggedInUser = userManager.getLoggedInUser().id
        return notificationRepository.getNotificationsOf(loggedInUser)
    }

    override suspend fun syncChildrenOf(parentIds: Array<String>) {
        if (parentIds.isNotEmpty()) {
            throw IllegalStateException("Only notification of currently logged-in user is viewable.")
        }
        // FIXME
//        val loggedInUser = userManager.getLoggedInUser().id
//        notificationRepository.syncNotificationsOf(loggedInUser)
    }
}