/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager

import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.entity.Travel
import kotlinx.coroutines.flow.Flow


/**
 * Manages the itineraries of the application.
 */
interface ItineraryManager : SyncableManager<Itinerary>, ChildManager<Itinerary> {


    /***********************************************************************************************
     * CRUD methods.
     **********************************************************************************************/

    /**
     * Retrieves the [Itinerary].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Itinerary].
     * @return The [Itinerary].
     */
    suspend fun getItinerary(parentIds: Array<String>, id: String): Itinerary

    /**
     * Retrieves the [Itinerary] as a stream.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Itinerary].
     * @return The [Itinerary].
     */
    fun getItineraryAsStream(parentIds: Array<String>, id: String): Flow<Itinerary>

    /**
     * Inserts an [Itinerary].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param description The label to be used for this record.
     * @return The ID of the [Travel].
     */
    suspend fun insert(parentIds: Array<String>, description: String): String

    /**
     * Updates a [Itinerary].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param oldItinerary The old itinerary record to be updated.
     * @param description The label to be used for this record.
     */
    suspend fun update(
        parentIds: Array<String>,
        oldItinerary: Itinerary,
        description: String
    )

    /**
     * Deletes the [Itinerary]s.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param itineraries The [Itinerary]s to be deleted.
     */
    suspend fun delete(parentIds: Array<String>, vararg itineraries: Itinerary)
}