/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.itinerary

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.itinerary.ItineraryFormDialog
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.manager.ItineraryManager
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [ItineraryFormDialog].
 */
class ItineraryFormViewModel : AbstractViewModel() {

    @Inject lateinit var itineraryManager: ItineraryManager

    internal lateinit var parentIds: Array<String>
    private val _oldItinerary: MutableLiveData<Itinerary> = MutableLiveData()
    val oldItinerary: LiveData<Itinerary>
        get() = _oldItinerary
    private val _dismiss: MutableLiveData<String?> = MutableLiveData()
    val dismiss: LiveData<String?>
        get() = _dismiss
    val isLoading = ObservableBoolean()

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Retrieves the details of the [Itinerary].
     *
     * @param itineraryId The unique ID of the itinerary.
     */
    fun getItinerary(itineraryId: String) {
        viewModelScope.launch {
            _oldItinerary.value = itineraryManager.getItinerary(parentIds, itineraryId)
        }
    }

    /**
     * Inserts an [Itinerary].
     *
     * @param description The label to be used for this record.
     */
    fun insert(description: String) {
        viewModelScope.launch {
            isLoading.set(true)
            val itineraryId = itineraryManager.insert(
                parentIds,
                description
            )
            _dismiss.value = itineraryId
            isLoading.set(false)
        }
    }

    /**
     * Updates an [Itinerary].
     *
     * @param description The label to be used for this record.
     */
    fun update(description: String) {
        viewModelScope.launch {
            isLoading.set(true)
            itineraryManager.update(parentIds, oldItinerary.value!!, description)
            _dismiss.value = null
            isLoading.set(false)
        }
    }
}