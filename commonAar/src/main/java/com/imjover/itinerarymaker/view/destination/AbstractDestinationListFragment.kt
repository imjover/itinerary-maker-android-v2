/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.destination

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.FragmentDestinationListBinding
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.viewmodel.destination.AbstractDestinationListViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


/**
 * The screen that holds the LIST representation of all destinations.
 */
abstract class AbstractDestinationListFragment :
    AbstractFragment<AbstractDestinationListViewModel, FragmentDestinationListBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        // Get the arguments passed from previous screen
        viewModel.parentIds =
            requireArguments().getStringArray(ARG_PARENT_IDS) ?: throw IllegalStateException()
        // Setup UI
        setupTravelList()
        viewModel.refreshDestinations()
        return view
    }


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Prepares the recycler view for the list of travels.
     */
    private fun setupTravelList() {
        val recyclerView: RecyclerView = dataBinding.rvDestinationList
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = DestinationListAdapter(parentFragment as DestinationsHandlerListener)

        lifecycleScope.launch {
            viewModel.getDestinations().collect {
                (recyclerView.adapter as DestinationListAdapter).updateData(it)
            }
        }
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_destination_list

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}