/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local

import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.source.type.LocalSyncableCrudChildSource

/**
 * Represents the local source of [Destination].
 */
interface DestinationLocalSource :
    LocalSyncableCrudChildSource<Destination, String>