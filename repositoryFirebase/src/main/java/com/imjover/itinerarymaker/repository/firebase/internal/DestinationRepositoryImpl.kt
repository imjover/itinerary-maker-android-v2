/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase.internal

import android.util.Log
import com.google.firebase.Timestamp
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.repository.DestinationRepository
import com.imjover.itinerarymaker.util.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


internal const val DESTINATIONS_COLLECTION = "destinations"

internal class DestinationRepositoryImpl(
    private val dispatcher: CoroutineDispatcher,
    private val db: FirebaseFirestore
) : AbstractSyncableCrudRepository<Destination>(dispatcher),
    DestinationRepository {


    override suspend fun updateIsVisited(
        parentIds: Array<String>,
        id: String,
        isVisited: Boolean
    ): Result<Unit> =
        withContext(dispatcher) {
            suspendCoroutine { cont ->
                getEventsCollection(parentIds)
                    .document(id)
                    .update("is_visited", isVisited)
                    .addOnSuccessListener {
                        Log.i(toString(), "Updated $id")
                        cont.resume(Result.success(Unit))
                    }
                    .addOnFailureListener {
                        it.logException(toString())
                        cont.resume(Result.failure(it))
                    }
            }
        }


    /***********************************************************************************************
     * Abstract functions from AbstractSyncableCrudRepository.
     **********************************************************************************************/

    override fun getEventsCollection(parentIds: Array<String>): CollectionReference {
        if (parentIds.size != 3) {
            throw IllegalArgumentException()
        }
        return db.collection(USERS_COLLECTION)
            .document(parentIds[0])
            .collection(TRAVELS_COLLECTION)
            .document(parentIds[1])
            .collection(ITINERARIES_COLLECTION)
            .document(parentIds[2])
            .collection(DESTINATIONS_COLLECTION)
    }

    override fun getOrderBy(): Pair<String, Query.Direction> =
        Pair("timestamp", Query.Direction.ASCENDING)


    override fun MutableMap<String, Any?>.toEntity(
        id: String,
        syncStatus: SyncStatus
    ): Destination =
        Destination(
            id,
            this["name"] as String,
            (this["timestamp"] as Timestamp).toDate(),
            DestinationType.fromId(this["destination_type"].toString()),
            TransportationMode.fromId(this["transportation_mode"].toString()),
            this["latitude"] as Double,
            this["longitude"] as Double,
            this["is_visited"] as Boolean,
            (this["last_updated"] as Timestamp).toDate(),
            this["fk_last_updated_by"] as String,
            syncStatus
        )

    override fun Destination.toHashMap(): HashMap<String, Any?> =
        hashMapOf(
            "name" to name,
            "timestamp" to timestamp,
            "destination_type" to destinationType.id,
            "transportation_mode" to transportationMode.id,
            "latitude" to latitude,
            "longitude" to longitude,
            "is_visited" to isVisited,
            "last_updated" to lastUpdated,
            "fk_last_updated_by" to lastUpdatedBy
        )
}