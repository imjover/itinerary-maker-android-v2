/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency

import com.imjover.itinerarymaker.AbstractFirebaseMessagingService
import com.imjover.itinerarymaker.AbstractUpdateDeviceTokenWorker


class FirebaseMessagingServiceImpl : AbstractFirebaseMessagingService() {

    override fun getClassOfUpdateDeviceTokenWorker(): Class<AbstractUpdateDeviceTokenWorker> =
        UpdateDeviceTokenWorkerImpl::class.java as Class<AbstractUpdateDeviceTokenWorker>
}