/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.destination.di

import com.imjover.itinerarymaker.manager.DestinationManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.manager.destination.internal.DestinationManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the [DestinationManager].
 */
@Module
abstract class DestinationManagerModule(userManager: UserManager) {

    @Singleton
    @Binds
    internal abstract fun bindDestinationManager(destinationManager: DestinationManagerImpl): DestinationManager
}