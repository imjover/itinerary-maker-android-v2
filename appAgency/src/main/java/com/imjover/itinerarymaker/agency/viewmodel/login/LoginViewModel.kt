/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.login.LoginActivity
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.viewmodel.login.AbstractLoginViewModel
import kotlinx.coroutines.launch


/**
 * The view model of [LoginActivity].
 */
class LoginViewModel : AbstractLoginViewModel() {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }

    private val _registerUser = MutableLiveData<Void>()
    val registerUser: LiveData<Void> get() = _registerUser

    /**
     * Do fake login, this is to fetch the logged in user in Firebase
     */
    override fun doFakeLogin() {
        _errorMessage.value = ""
        viewModelScope.launch {
            userManager.findUserByLogin("", "").fold({
                when (it.userType) {
                    UserType.TRAVELER -> {
                        if (it.isApproved) {
                            // This TRAVELER user can be a new employee.
                            _registerUser.value = null
                            return@fold
                        }
                    }
                    UserType.ADMIN,
                    UserType.AGENT,
                    UserType.ACCOUNTANT -> {
                        if (it.isApproved) {
                            // Valid user
                            _isLoggedIn.value = true
                            return@fold
                        } else {
                            // This user's account approval is still pending.
                            _errorMessage.value =
                                AppComponent.appContext.getString(R.string.login_activity__user_pending_registration__message)
                            return@fold
                        }
                    }
                }
                // INVALID USER
                _errorMessage.value =
                    AppComponent.appContext.getString(R.string.login_activity__unauthorized_user__message)
                logout()
            }, {
                Log.w(toString(), it)
                _errorMessage.value = it.getMessageForUI()
            })
        }
    }
}