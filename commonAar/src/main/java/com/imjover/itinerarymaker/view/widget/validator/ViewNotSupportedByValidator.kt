/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget.validator

import android.view.View

/**
 * Thrown when a [View] is not supported by the [Validator].
 */
class ViewNotSupportedByValidator : RuntimeException {

    constructor(view: View, validator: Validator) : super(
        String.format(
            "The view %s is not supported by the validator %s.",
            view.javaClass.simpleName,
            validator.javaClass.simpleName
        )
    )

    constructor(view1: View, view2: View, validator: Validator) : super(
        String.format(
            "The combination of view %s and %s is not supported by the validator %s.",
            view1.javaClass.simpleName,
            view2.javaClass.simpleName,
            validator.javaClass.simpleName
        )
    )
}