/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.util

/**
 * A discriminated union that encapsulates a successful outcome with a value of type [T]
 * or a failure with an arbitrary [Throwable] exception.
 *
 * Based on Kotlin's [kotlin.Result].
 * @see https://android.jlelse.eu/just-another-result-monad-with-kotlin-4744f662349d>
 */
class Result<T : Any> private constructor(val value: Any) {

    companion object {
        fun <T : Any> success(value: T): Result<T> =
            Result(value)

        fun <T : Any> failure(error: Throwable): Result<T> =
            Result(Failure(error))
    }

    fun isSuccess(): Boolean = this.value !is Failure

    fun isFailure(): Boolean = this.value is Failure

    @Suppress("UNCHECKED_CAST")
    fun materialize(onFailure: (error: Throwable) -> T): T =
        when (this.value) {
            is Failure -> onFailure.invoke(this.value.error)
            else -> this.value as T
        }

    @Suppress("UNCHECKED_CAST")
    fun <U : Any> map(f: (T) -> U): Result<U> =
        when (this.value) {
            is Failure -> failure(this.value.error)
            else -> success(f.invoke(this.value as T))
        }

    /**
     * Returns the encapsulated value if this instance represents [success][Result.isSuccess] or `null`
     * if it is [failure][Result.isFailure].
     *
     * This function is a shorthand for `getOrElse { null }` (see [getOrElse]) or
     * `fold(onSuccess = { it }, onFailure = { null })` (see [fold]).
     */
    fun getOrNull(): T? =
        when (this.value) {
            is Failure -> null
            else -> value as T
        }

    /**
     * Returns the result of [onSuccess] for the encapsulated value if this instance represents [success][Result.isSuccess]
     * or the result of [onFailure] function for the encapsulated [Throwable] exception if it is [failure][Result.isFailure].
     *
     * Note, that this function rethrows any [Throwable] exception thrown by [onSuccess] or by [onFailure] function.
     */
    @Suppress("UNCHECKED_CAST")
    fun <R> fold(
        onSuccess: (value: T) -> R,
        onFailure: (exception: Throwable) -> R
    ): R =
        when (this.value) {
            is Failure -> onFailure.invoke(this.value.error)
            else -> onSuccess(value as T)
        }

    private class Failure(val error: Throwable)
}