/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote

import com.imjover.itinerarymaker.common.di.ApplicationType
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.exception.RemoteAccessException
import com.imjover.itinerarymaker.util.Result

/**
 * Represents the remote source of [User].
 */
interface UserRemoteSource {

    /**
     * Find a user by logging in.
     *
     * @param username The username of the user.
     * @param password The password of the user.
     * @param applicationType The type of application used in logging in.
     * @return The user.
     */
    suspend fun findUserByLogin(
        username: String,
        password: String,
        applicationType: ApplicationType
    ): Result<User>

    /**
     * Logs out the current logged-in user.
     */
    suspend fun logout(): Result<Unit>

    /**
     * Registers the current user.
     *
     * @param userType The user type.
     * @return The response message.
     */
    suspend fun register(userType: UserType): Result<String>

    /**
     * Approves a user's registration.
     *
     * @param user The user to be registered.
     * @return The response message.
     */
    suspend fun approveRegistration(user: User): Result<String>

    /**
     * Retrieves the current logged-in user. The [findUserByLogin] must be called first successfully.
     *
     * @throws IllegalStateException When there is no logged-in user.
     */
    fun getLoggedInUser(): User

    /**
     * Retrieves all [User]s with the given [UserType].
     *
     * @param userTypes The user types.
     * @return List of [User]s.
     */
    suspend fun getUsers(vararg userTypes: UserType): List<User>
}