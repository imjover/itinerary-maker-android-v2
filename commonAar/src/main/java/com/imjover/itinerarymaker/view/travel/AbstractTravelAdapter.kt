/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.travel

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.RowTravelBinding
import com.imjover.itinerarymaker.entity.Status
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.util.DateFormat
import com.imjover.itinerarymaker.helper.toColorRes
import com.imjover.itinerarymaker.util.toStringFormat
import com.imjover.itinerarymaker.helper.toStringRes
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter

/**
 * List adapter of [Travel] item.
 */
abstract class AbstractTravelAdapter : UpdatableRecyclerViewAdapter<Travel, RowTravelBinding>() {

    private lateinit var context: Context

    /**
     * Checks whether the current logged-in user is allowed to edit
     */
    abstract fun isUserAllowedToEdit(): Boolean

    /**
     * Called when an item in the Adapter is clicked.
     *
     * @param id The unique ID of the clicked item.
     * @param isEditable Whether this travel can be modified then true, else false.
     */
    abstract fun getOnClickListener(id: String, isEditable: Boolean): View.OnClickListener?

    /***********************************************************************************************
     * Overriden functions.
     **********************************************************************************************/

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterViewHolder<RowTravelBinding> {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = RowTravelBinding.inflate(layoutInflater, parent, false)
        return AdapterViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: AdapterViewHolder<RowTravelBinding>, position: Int) {
        val travel = getData(position)
        val showDivider = position + 1 < itemCount
        // Bind instances of [Travel] to the row.
        holder.binding.travel = travel
        when (travel.status) {
            Status.CANCELLED -> {
                holder.binding.tvName.setTextColor(context.resources.getColor(R.color.text_negative))
                holder.binding.container.setOnClickListener(null)
            }
            else -> {
                holder.binding.tvName.setTextColor(context.resources.getColor(R.color.text_primary))
                val isEditable = isUserAllowedToEdit() && !travel.status.isEndState()
                holder.binding.container.setOnClickListener(getOnClickListener(travel.id, isEditable))
            }
        }
        holder.binding.tvStatus.setText(travel.status.toStringRes())
        holder.binding.tvStatus.setTextColor(context.resources.getColor(travel.status.toColorRes()))
        holder.binding.tvTravelDate.text = context.getString(
            R.string.travel_fragment__travel_date__format,
            travel.startDate.toStringFormat(DateFormat.DATE_FORMAT),
            travel.endDate.toStringFormat(DateFormat.DATE_FORMAT)
        )
        holder.binding.divider.visibility = if (showDivider) View.VISIBLE else View.GONE
        holder.binding.executePendingBindings()
    }
}