/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.imjover.itinerarymaker.entity.*
import kotlinx.coroutines.flow.Flow
import java.util.*

/**
 * Data Access Object of the [Travel].
 */
@Dao
internal interface TravelDao : CrudDao<Travel> {

    /**
     * Retrieve a [Travel] with the given [id].
     *
     * @param id The ID of this record.
     * @return The found [Travel].
     */
    @Query("SELECT * FROM tbl_travel WHERE pk_travel_id = :id LIMIT 1")
    suspend fun getTravel(id: String): Travel

    /**
     * Retrieve a stream [Travel] with the given [id].
     *
     * @param id The ID of this record.
     * @return The found stream [Travel].
     */
    @Query("SELECT * FROM tbl_travel WHERE pk_travel_id = :id LIMIT 1")
    fun getTravelAsStream(id: String): Flow<Travel>

    /**
     * Retrieve all [Travel]s of a [User] and converts it into a stream.
     *
     * @param userId The owner of the records.
     * @return A stream of [Travel]s.
     */
    @Query("SELECT * FROM tbl_travel WHERE fk_owner = :userId ORDER BY status")
    fun getStreamTravelsOf(userId: String): Flow<List<Travel>>

    /**
     * Retrieve all [Travel]s of a [User].
     *
     * @param userId The owner of the records.
     * @return A list of [Travel]s.
     */
    @Query("SELECT * FROM tbl_travel WHERE fk_owner = :userId")
    suspend fun getTravelsOf(userId: String): List<Travel>

    /**
     * Retrieves all [Travel]s that has [syncStatus] and owned by the given [userId].
     *
     * @param userId The owner of the records.
     * @param syncStatus The synchronization status.
     * @return A list of [Travel]s.
     */
    @Query("SELECT * FROM tbl_travel WHERE fk_owner = :userId AND sync_status = :syncStatus")
    suspend fun getTravelsOf(userId: String, syncStatus: SyncStatus): List<Travel>


    /**
     * Retrieve the status of a [Travel].
     *
     * @param travelId The ID of this record.
     * @return The status of the [Travel].
     */
    @Query("SELECT status FROM tbl_travel WHERE pk_travel_id = :travelId")
    suspend fun getStatus(travelId: String): Status

    /**
     * Updates the primary key to [newId] and [syncStatus] of the record with [currentId].
     *
     * @param currentId The current ID of this record.
     * @param newId The new ID to be used on this record.
     * @param syncStatus The synchronization status.
     */
    @Query("UPDATE tbl_travel SET pk_travel_id = :newId, sync_status = :syncStatus WHERE pk_travel_id = :currentId")
    suspend fun updateId(currentId: String, newId: String, syncStatus: SyncStatus)

    /**
     * Updates the [status] of the record with [travelId].
     *
     * @param travelId The ID of this record.
     * @param status The status.
     */
    @Query("UPDATE tbl_travel SET status = :status WHERE pk_travel_id = :travelId")
    suspend fun updateStatus(travelId: String, status: Status)

    /**
     * Updates the [paymentMode] of the record with [travelId].
     *
     * @param travelId The ID of this record.
     * @param paymentMode The payment mode.
     * @param lastUpdatedBy The user who updated this record.
     * @param lastUpdated The date this record was updated.
     */
    @Query("UPDATE tbl_travel SET payment_mode = :paymentMode, fk_last_updated_by = :lastUpdatedBy, last_updated = :lastUpdated, status = :status WHERE pk_travel_id = :travelId")
    suspend fun updatePaymentMode(travelId: String, paymentMode: PaymentMode, lastUpdatedBy: String, lastUpdated: Date, status: Status)
}