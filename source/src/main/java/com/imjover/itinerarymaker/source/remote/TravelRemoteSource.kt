/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote

import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.source.type.RemoteCrudChildSource
import java.util.*

/**
 * Represents the remote source of [Travel].
 */
interface TravelRemoteSource : RemoteCrudChildSource<Travel, String> {

    /**
     * Cancels the travel.
     *
     * @param travelId The ID of the travel.
     */
    suspend fun cancel(travelId: String)

    /**
     * Updates the [paymentMode] of the travel.
     *
     * @param travelId The ID of this record.
     * @param paymentMode The payment mode.
     */
    suspend fun updatePaymentMode(travelId: String, paymentMode: PaymentMode)
}