/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

import androidx.room.*
import androidx.room.Entity
import java.util.*


/**
 * The itinerary of a [User].
 *
 * @param id The unique ID and primary key.
 * @param travelId The foreign key of the parent of this object.
 * @param name The label to be used for this record.
 * @param lastUpdated The most recent date this record was updated.
 * @param lastUpdatedBy The most recent user who updated this record.
 * @param syncStatus The current synchronization state of this record.
 */
@Entity(
    tableName = "tbl_itinerary"
)
data class Itinerary(
    @PrimaryKey @ColumnInfo(name = "pk_itinerary_id") val id: String,
    @ColumnInfo(name = "name") val name: String,
    // Audit Trail
    @ColumnInfo(name = "last_updated") val lastUpdated: Date,
    @ColumnInfo(name = "fk_last_updated_by") val lastUpdatedBy: String,
    // Syncable
    @ColumnInfo(name = "sync_status") val syncStatus: SyncStatus
) : SyncableChildEntity {

    override fun getUniqueId(): String =
        id

    override fun getForeignKey(): String =
        TODO("To be implemented")

    override fun getSyncableStatus(): SyncStatus =
        syncStatus

    override fun cloneAndSetArg(newSyncStatus: SyncStatus): Itinerary =
        cloneAndSetArg(this.id, newSyncStatus)

    override fun cloneAndSetArg(newId: String, newSyncStatus: SyncStatus): Itinerary =
        Itinerary(
            newId,
            this.name,
            this.lastUpdated,
            this.lastUpdatedBy,
            newSyncStatus
        )
}