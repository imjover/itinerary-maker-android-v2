/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote

import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.source.type.RemoteCrudChildSource

/**
 * Represents the remote source of [Itinerary].
 */
interface ItineraryRemoteSource :
    RemoteCrudChildSource<Itinerary, String>