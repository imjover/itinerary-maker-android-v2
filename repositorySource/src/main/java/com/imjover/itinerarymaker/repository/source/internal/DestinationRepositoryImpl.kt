/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source.internal

import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.repository.BuildConfig
import com.imjover.itinerarymaker.repository.DestinationRepository
import com.imjover.itinerarymaker.source.local.DestinationLocalSource
import com.imjover.itinerarymaker.source.remote.DestinationRemoteSource
import kotlinx.coroutines.CoroutineDispatcher


internal class DestinationRepositoryImpl(
    localSource: DestinationLocalSource,
    remoteSource: DestinationRemoteSource,
    dispatcher: CoroutineDispatcher
) : AbstractSyncableCrudRepository<Destination, String, DestinationLocalSource, DestinationRemoteSource>(
    localSource,
    remoteSource,
    dispatcher
), DestinationRepository {

    override fun toString(): String =
        if (BuildConfig.DEBUG) "DestinationRepositoryImpl" else super.toString()
}