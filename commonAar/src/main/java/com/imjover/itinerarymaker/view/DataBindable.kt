/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel


/**
 * Defines the screens (Activity, Fragment, etc.) that can be databinded with view models.
 */
interface DataBindable<VM : AbstractViewModel> {

    /**
     * Retrieves the layout of this [Fragment].
     */
    fun getLayout(): Int

    /**
     * Retrieves the class type of the [ViewModel].
     */
    fun getViewModelClass(): Class<VM>

    /**
     * Perform data binding of the layout and ViewModel.
     */
    fun bindLayoutAndViewModel()
}