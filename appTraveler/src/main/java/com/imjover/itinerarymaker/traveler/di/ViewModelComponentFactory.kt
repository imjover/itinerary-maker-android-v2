/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.di


/**
 * Handles the static instance of [ViewModelComponent].
 */
internal class ViewModelComponentFactory private constructor() {

    companion object {

        private lateinit var viewModelComponent: ViewModelComponent

        fun singleton(): ViewModelComponent {
            if (!this::viewModelComponent.isInitialized) {
                viewModelComponent = DaggerViewModelComponent.create()
            }
            return viewModelComponent
        }
    }
}