/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.splash

import android.app.Activity
import com.imjover.itinerarymaker.traveler.view.login.LoginActivity
import com.imjover.itinerarymaker.view.splash.AbstractSplashActivity


/**
 * The first screen to launch in the app.
 */
class SplashActivity : AbstractSplashActivity() {

    override fun getClassOfNextActivity(): Class<Activity> =
        LoginActivity::class.java as Class<Activity>
}