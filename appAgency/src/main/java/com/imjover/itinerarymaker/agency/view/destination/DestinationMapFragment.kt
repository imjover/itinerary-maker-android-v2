/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.destination

import android.os.Bundle
import com.google.android.gms.maps.SupportMapFragment
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.FragmentDestinationMapBinding
import com.imjover.itinerarymaker.agency.viewmodel.destination.DestinationMapViewModel
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.destination.ARG_PARENT_IDS
import com.imjover.itinerarymaker.view.destination.AbstractDestinationMapFragment


/**
 * The screen that holds the MAP representation of all destinations.
 */
class DestinationMapFragment :
    AbstractDestinationMapFragment<DestinationMapViewModel, FragmentDestinationMapBinding>() {


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getViewModelClass(): Class<DestinationMapViewModel> =
        DestinationMapViewModel::class.java

    override fun getLayout(): Int =
        R.layout.fragment_destination_map

    override fun getMapFragment(): SupportMapFragment =
        childFragmentManager.findFragmentById(R.id.fragment_map) as SupportMapFragment


    /***********************************************************************************************
     * Other functions.
     **********************************************************************************************/

    companion object {
        /**
         * Creates an instance of this fragment.
         *
         * @param parentIds The parent [Itinerary] of the destination to be displayed.
         */
        fun newInstance(parentIds: Array<String>) =
            DestinationMapFragment().apply {
                arguments = Bundle().apply {
                    putStringArray(ARG_PARENT_IDS, parentIds)
                }
            }
    }
}