/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget.validator

import android.view.View

/**
 * Used by [FormValidator] to validate the input of a [View].
 */
interface Validator {

    /**
     * Checks whether the input of a [View] is valid.
     *
     * @param view The [View] to be validated.
     * @return Whether the input of a [View] is valid, then true, else false.
     */
    fun isValid(view: View): Boolean

    /**
     * Retrieves the error message to be displayed when [isValid] is false.
     */
    fun getErrorMsg(): String
}