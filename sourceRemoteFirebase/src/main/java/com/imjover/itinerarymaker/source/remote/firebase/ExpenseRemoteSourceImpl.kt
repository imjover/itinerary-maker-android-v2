/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.firebase

import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.source.remote.ExpenseRemoteSource


class ExpenseRemoteSourceImpl : ExpenseRemoteSource {

    override suspend fun insert(entity: Expense): String {
        TODO("Not yet implemented")
    }

    override suspend fun update(entity: Expense) {
        TODO("Not yet implemented")
    }

    override suspend fun delete(entity: Expense) {
        TODO("Not yet implemented")
    }

    override suspend fun getListChildrenOf(parentId: String): List<Expense> {
        TODO("Not yet implemented")
    }
}