/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.firebase

import android.net.Uri
import android.util.Log
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.functions.FirebaseFunctions
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.common.di.ApplicationType
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.source.remote.UserRemoteSource
import com.imjover.itinerarymaker.util.Result
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


internal class UserRemoteSourceImpl(
    private val auth: FirebaseAuth,
    private val functions: FirebaseFunctions
) : UserRemoteSource {

    private var user: User? = null

    override suspend fun findUserByLogin(
        username: String,
        password: String,
        applicationType: ApplicationType
    ): Result<User> {
        if (username != "" || password != "") {
            throw IllegalStateException()
        }
        if (user != null) {
            return Result.success(user!!)
        }
        // Since the login is handled by Firebase-Auth-UI, let's fetch the user instead
        val firebaseUser = auth.currentUser!!
        val claims = suspendCoroutine<Map<String, Any>> { cont ->
            firebaseUser.getIdToken(false)
                .addOnSuccessListener {
                    cont.resume(it.claims)
                }
        }
        val userType: UserType
        val isApproved: Boolean
        if (!claims.containsKey("userType") && !claims.containsKey("isApproved")) {
            // By default, user is treated as traveler.
            // But both claims, userType and isApproved, SHOULD not be found.
            userType = UserType.TRAVELER
            isApproved = true
        } else {
            // Else, manually retrieved the claims.
            userType =
                if (claims.containsKey("userType")) {
                    UserType.fromId(claims["userType"].toString().toInt())
                } else {
                    val ex = RemoteSourceFirebaseException("Invalid user type.")
                    Log.w(toString(), ex)
                    throw ex
                }
            isApproved =
                if (claims.containsKey("isApproved")) {
                    claims["isApproved"].toString().toBoolean()
                } else {
                    false
                }
        }

        // Create user
        val uid = firebaseUser.uid
        var username: String = firebaseUser.email ?: uid
        var name: String = firebaseUser.displayName ?: username
        var photoUrl = firebaseUser.photoUrl
        // Try to get user's info from other provider, if available.
        for (profile in firebaseUser.providerData) {
            profile.email?.let { username = it }
            profile.displayName?.let { name = it }
            profile.photoUrl.let { photoUrl = it }
        }
        user = User(uid, username, name, photoUrl, userType, isApproved)
        return Result.success(user!!)
    }

    override suspend fun logout(): Result<Unit> =
        suspendCoroutine { cont ->
            AuthUI.getInstance()
                .signOut(AppComponent.appContext)
                .addOnSuccessListener {
                    user = null
                    cont.resume(Result.success(Unit))
                }
                .addOnFailureListener {
                    it.logException(toString())
                    cont.resume(Result.failure(it))
                }
        }

    override suspend fun register(userType: UserType): Result<String> {
        val data: MutableMap<String, Any> = HashMap()
        data["userType"] = userType.id

        return suspendCoroutine { cont ->
            functions
                .getHttpsCallable("users-requestRegistration")
                .call(data)
                .continueWith { task ->
                    task.result?.data as String
                }
                .addOnSuccessListener {
                    cont.resume(Result.success(it))
                }
                .addOnFailureListener {
                    it.logException(toString())
                    cont.resume(Result.failure(it))
                }
        }
    }

    override suspend fun approveRegistration(user: User): Result<String> {
        val data: MutableMap<String, Any> = HashMap()
        data["uid"] = user.id

        return suspendCoroutine { cont ->
            functions
                .getHttpsCallable("users-approveRegistration")
                .call(data)
                .continueWith { task ->
                    task.result?.data as String
                }
                .addOnSuccessListener {
                    cont.resume(Result.success(it))
                }
                .addOnFailureListener {
                    it.logException(toString())
                    cont.resume(Result.failure(it))
                }
        }
    }

    override fun getLoggedInUser(): User =
        user!!

    override suspend fun getUsers(vararg userTypes: UserType): List<User> {
        val userTypesParam = mutableListOf<Int>()
        userTypes.forEach {
            userTypesParam.add(it.id)
        }
        val data: MutableMap<String, Any> = HashMap()
        data["userTypes"] = userTypesParam

        return suspendCoroutine { cont ->
            functions
                .getHttpsCallable("users-get")
                .call(data)
                .continueWith { task ->
                    val result =
                        task.result?.data as HashMap<String, ArrayList<HashMap<String, Any?>>>
                    result
                }
                .addOnSuccessListener { response ->
                    val users = mutableListOf<User>()
                    response["users"]?.forEach {
                        users.add(
                            User(
                                it["id"] as String,
                                it["username"] as String,
                                it["name"] as String,
                                it["photoUrl"]?.let { photoUrl -> Uri.parse(photoUrl as String) },
                                UserType.fromId(it["userType"] as Int),
                                it["isApproved"] as Boolean
                            )
                        )
                    }
                    cont.resume(users)
                }
                .addOnFailureListener {
                    it.logException(toString())
                    cont.resumeWithException(it)
                }
        }
    }

    override fun toString(): String =
        if (BuildConfig.DEBUG) "UserRemoteSourceImpl" else super.toString()
}
