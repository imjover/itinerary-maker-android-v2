/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.imjover.itinerarymaker.entity.SyncableEntity
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter
import com.imjover.itinerarymaker.viewmodel.AbstractSyncableViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


/**
 * Defines the fragment that can be synced in both remote and local sources.
 */
abstract class AbstractRefreshableFragment<
        SE : SyncableEntity,
        VM : AbstractSyncableViewModel<SE>,
        DB : ViewDataBinding> :
    AbstractFragment<VM, DB>() {

    private var jobGetSyncableItems: Job? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRefreshableList()

        viewModel.refresh()
    }

    override fun onResume() {
        super.onResume()
        jobGetSyncableItems = lifecycleScope.launch {
            viewModel.getSyncableItems().collect {
                (getRecyclerView().adapter as UpdatableRecyclerViewAdapter<SE, *>).updateData(it)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        jobGetSyncableItems?.let {
            it.cancel()
            jobGetSyncableItems = null
        }
    }


    /***********************************************************************************************
     * Abstract functions.
     **********************************************************************************************/

    /**
     * Retrieves the view that will hold the refreshable list.
     *
     * @return The RecyclerView.
     */
    abstract fun getRecyclerView(): RecyclerView

    /**
     * Retrieves the view that will refresh the refreshable list.
     *
     * @return The SwipeRefreshLayout.
     */
    abstract fun getSwipeRefreshLayout(): SwipeRefreshLayout

    /**
     * Retrieves the adapter for the refreshable list.
     *
     * @return The UpdatableRecyclerViewAdapter
     */
    abstract fun getAdapter(): UpdatableRecyclerViewAdapter<SE, *>


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Prepares the recycler view for the list of entities.
     */
    private fun setupRefreshableList() {
        // Setup SwipeRefreshLayout
        val swipeView = getSwipeRefreshLayout()
        swipeView.setOnRefreshListener {
            viewModel.refresh()
        }
        viewModel.isRefreshing.observe(viewLifecycleOwner, {
            swipeView.isRefreshing = it
        })

        // Setup RecyclerView
        val recyclerView = getRecyclerView()
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = getAdapter()
    }
}