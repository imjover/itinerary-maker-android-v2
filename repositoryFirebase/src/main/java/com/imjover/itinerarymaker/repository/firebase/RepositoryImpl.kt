/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.firestoreSettings
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import com.imjover.itinerarymaker.repository.*
import com.imjover.itinerarymaker.repository.firebase.internal.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Holds the instances of dao-access-objects (DAOs).
 */
internal class RepositoryImpl(
    defaultDispatcher: CoroutineDispatcher = Dispatchers.IO
) : Repository {

    /**
     * Firebase functions can point to Firebase local emulator by:
     *  functions = FirebaseFunctions.getInstance()
     *  functions.useEmulator("10.0.2.2", 5001)
     *
     * 10.0.2.2 is the special IP address to connect to the 'localhost' of
     * the host computer from an Android emulator.
     */
    private val functions: FirebaseFunctions = Firebase.functions
    private val auth: FirebaseAuth = FirebaseAuth.getInstance()
    private val db: FirebaseFirestore = Firebase.firestore

    private val userRepository: UserRepository
    private val travelRepository: TravelRepository
    private val itineraryRepository: ItineraryRepository
    private val destinationRepository: DestinationRepository
    private val expenseRepository: ExpenseRepository
    private val notificationRepository: NotificationRepository
    private val reportRepository: ReportRepository

    init {
//        functions.useEmulator("10.0.2.2", 5001)
//        auth.useEmulator("10.0.2.2", 5001)
//        db.useEmulator("10.0.2.2", 5001)
        db.firestoreSettings = firestoreSettings {
            // Note: Persistence is NOT supported on Firebase emulator
            isPersistenceEnabled = true
            cacheSizeBytes = FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED
        }

        userRepository = UserRepositoryImpl(defaultDispatcher, auth, functions, db)
        travelRepository = TravelRepositoryImpl(defaultDispatcher, functions, db)
        itineraryRepository = ItineraryRepositoryImpl(defaultDispatcher, db)
        destinationRepository = DestinationRepositoryImpl(defaultDispatcher, db)
        expenseRepository = ExpenseRepositoryImpl(defaultDispatcher, db)
        notificationRepository = NotificationRepositoryImpl(defaultDispatcher, db)
        reportRepository = ReportRepositoryImpl(functions)
    }

    override fun getUserRepository(): UserRepository = userRepository

    override fun getTravelRepository(): TravelRepository = travelRepository

    override fun getItineraryRepository(): ItineraryRepository = itineraryRepository

    override fun getDestinationRepository(): DestinationRepository = destinationRepository

    override fun getExpenseRepository(): ExpenseRepository = expenseRepository

    override fun getNotificationRepository(): NotificationRepository = notificationRepository

    override fun getReportRepository(): ReportRepository = reportRepository
}