/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.splash


/**
 * Serves as guide on what is the current progress of [SplashViewModel].
 */
enum class SplashProgress {

    /**
     * Required background processes has started.
     */
    STARTED,

    /**
     * All activities has ended.
     */
    ENDED
}