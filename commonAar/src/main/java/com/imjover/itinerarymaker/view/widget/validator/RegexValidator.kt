/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget.validator

import android.view.View
import android.widget.TextView
import java.util.regex.Pattern

/**
 * Validates a [View] and ensures that its input's value follows the format of the given regex.
 *
 * @param regex The regular expression.
 * @param errorMsg The error message to be displayed when [isValid] is false.
 */
class RegexValidator(private val regex: String, private val errorMsg: String) : Validator {

    private val pattern: Pattern = Pattern.compile(regex)

    override fun isValid(view: View): Boolean {
        when (view) {
            is TextView -> {
                val input = view.text.toString()
                return pattern.matcher(input).matches()
            }
        }
        throw ViewNotSupportedByValidator(view, this)
    }

    override fun getErrorMsg(): String =
        errorMsg
}