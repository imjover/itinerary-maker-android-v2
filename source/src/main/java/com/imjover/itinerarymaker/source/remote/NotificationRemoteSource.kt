/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote

import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.source.type.ChildSource
import com.imjover.itinerarymaker.source.type.LocalCrudSource

/**
 * Represents the remote source of [Notification].
 */
interface NotificationRemoteSource : ChildSource<Notification, String> {

    /**
     * Update notifications as seen.
     *
     * @param ids IDs of the notifications to be updated.
     */
    suspend fun setAsSeen(ids: List<String>)
}