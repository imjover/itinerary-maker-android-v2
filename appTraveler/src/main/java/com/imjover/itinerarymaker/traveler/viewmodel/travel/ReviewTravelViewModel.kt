/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.viewmodel.travel

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.exception.RemoteAccessException
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.manager.TravelManager
import com.imjover.itinerarymaker.traveler.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.traveler.view.travel.ReviewTravelDialog
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [ReviewTravelDialog].
 */
class ReviewTravelViewModel : AbstractViewModel() {

    @Inject
    lateinit var travelManager: TravelManager
    internal lateinit var parentIds: Array<String>
    internal lateinit var travelId: String

    private val _showMessage: MutableLiveData<Consumable<String>> = MutableLiveData()
    val showMessage: LiveData<Consumable<String>> get() = _showMessage

    private val _submitted: MutableLiveData<Unit> = MutableLiveData()
    val submitted: LiveData<Unit> get() = _submitted

    val isLoading = ObservableBoolean()

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }


    /**
     * Submits the review on the travel.
     *
     * @param isApprove Whether the review is approve, then true, else false.
     * @param reason The reasons for the status change.
     */
    fun submitReview(isApprove: Boolean, reason: String) {
        viewModelScope.launch {
            isLoading.set(true)
            _showMessage.value =
                travelManager.submitReview(
                    parentIds,
                    travelId,
                    isApprove,
                    if (reason.isEmpty()) null else reason
                )
                    .map {
                        _submitted.value = null
                        Consumable(it)
                    }
                    .materialize { Consumable(it.getMessageForUI()) }
            isLoading.set(false)
        }
    }
}