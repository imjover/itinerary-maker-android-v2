/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote

import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.source.type.RemoteCrudChildSource

/**
 * Represents the remote source of [Expense].
 */
interface ExpenseRemoteSource :
    RemoteCrudChildSource<Expense, String>