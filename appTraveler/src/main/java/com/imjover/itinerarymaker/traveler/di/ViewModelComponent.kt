/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.di

import com.imjover.itinerarymaker.manager.destination.di.DestinationManagerModule
import com.imjover.itinerarymaker.manager.expense.di.ExpenseManagerModule
import com.imjover.itinerarymaker.manager.itinerary.di.ItineraryManagerModule
import com.imjover.itinerarymaker.manager.notification.di.NotificationManagerModule
import com.imjover.itinerarymaker.manager.travel.di.TravelManagerModule
import com.imjover.itinerarymaker.manager.user.di.UsersManagerModule
import com.imjover.itinerarymaker.traveler.UpdateDeviceTokenWorkerImpl
import com.imjover.itinerarymaker.traveler.viewmodel.destination.DestinationListViewModel
import com.imjover.itinerarymaker.traveler.viewmodel.destination.DestinationMapViewModel
import com.imjover.itinerarymaker.traveler.viewmodel.destination.DestinationsViewModel
import com.imjover.itinerarymaker.traveler.viewmodel.expense.ExpensesViewModel
import com.imjover.itinerarymaker.traveler.viewmodel.itinerary.ItinerariesViewModel
import com.imjover.itinerarymaker.traveler.viewmodel.login.LoginViewModel
import com.imjover.itinerarymaker.traveler.viewmodel.main.MainViewModel
import com.imjover.itinerarymaker.traveler.viewmodel.travel.ReviewTravelViewModel
import com.imjover.itinerarymaker.viewmodel.notification.AbstractNotificationsViewModel
import com.imjover.itinerarymaker.traveler.viewmodel.travel.TravelsViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Injects the required modules of the view models.
 */
@Singleton
@Component(
    modules = [
        UsersManagerModule::class,
        TravelManagerModule::class,
        ItineraryManagerModule::class,
        DestinationManagerModule::class,
        ExpenseManagerModule::class,
        NotificationManagerModule::class
    ]
)
interface ViewModelComponent {

    fun inject(worker: UpdateDeviceTokenWorkerImpl)
    fun inject(viewModel: MainViewModel)
    fun inject(viewModel: LoginViewModel)
    fun inject(viewModel: TravelsViewModel)
    fun inject(viewModel: ReviewTravelViewModel)
    fun inject(viewModel: ItinerariesViewModel)
    fun inject(viewModel: DestinationsViewModel)
    fun inject(viewModel: DestinationListViewModel)
    fun inject(viewModel: DestinationMapViewModel)
    fun inject(viewModel: ExpensesViewModel)
    fun inject(viewModel: AbstractNotificationsViewModel)
}