/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * Types of the user of this application.
 *
 * @param id The unique ID.
 */
enum class UserType(val id: Int) {

    /**
     * The traveler, the main user of this app. Has basic access only.
     */
    TRAVELER(-1),

    /**
     * The administrator.
     */
    ADMIN(0),

    /**
     * The agent, who does the managing of traveler's travel.
     */
    AGENT(1),

    /**
     * The accountant, who manages accounting.
     */
    ACCOUNTANT(2);


    companion object {
        /**
         * Retrieves the equivalent [UserType] base on the given ID.
         *
         * @param id The id of the [UserType].
         * @return The retrieved [UserType].
         */
        fun fromId(id: Int): UserType {
            values().forEach {
                if (it.id == id) {
                    return it
                }
            }
            throw IllegalStateException()
        }
    }
}