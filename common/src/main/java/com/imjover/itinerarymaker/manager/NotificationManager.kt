/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager

import com.imjover.itinerarymaker.entity.Notification
import kotlinx.coroutines.flow.Flow

/**
 * Manages the notifications of the application.
 */
interface NotificationManager : SyncableManager<Notification> {

    /**
     * Retrieves the total count of new notifications of the currently logged-in user
     * New notifications are those that [Notification.isSeen] is false.
     *
     * @return Stream of total count.
     */
    fun getNewNotificationCount(): Flow<Int>

    /**
     * Set the given notification as seen.
     *
     * @param travelerId The ID of the traveler.
     * @param id The unique ID of the notification.
     */
    suspend fun setAsSeen(travelerId: String, id: String)
}