/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.util

import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.NumberFormat


/**
 * Formats the object [BigDecimal] to its monetary String format without currency.
 *
 * @return The formatted String.
 */
fun BigDecimal.toStringMonetary(): String {
    val formatter = NumberFormat.getCurrencyInstance() as DecimalFormat
    val symbols = formatter.decimalFormatSymbols
    symbols.currencySymbol = "" // Remove currency. Don't use null.
    formatter.decimalFormatSymbols = symbols
    return NumberFormat.getInstance().format(this);
}