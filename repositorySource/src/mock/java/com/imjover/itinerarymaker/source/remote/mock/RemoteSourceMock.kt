/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.mock

import com.imjover.itinerarymaker.source.RemoteSource
import com.imjover.itinerarymaker.source.remote.*
import javax.inject.Inject

/**
 * Remote implementation of repository that uses mocking.
 */
internal class RemoteSourceMock @Inject constructor() : RemoteSource {

    override fun getUserRemoteSource(): UserRemoteSource =
        UserRemoteSourceMock()

    override fun getTravelRemoteSource(): TravelRemoteSource =
        TravelRemoteSourceMock(getNotificationRemoteSource() as NotificationRemoteSourceMock)

    override fun getItineraryRemoteSource(): ItineraryRemoteSource =
        ItineraryRemoteSourceMock()

    override fun getDestinationRemoteSource(): DestinationRemoteSource =
        DestinationRemoteSourceMock()

    override fun getExpenseRemoteSource(): ExpenseRemoteSource =
        ExpenseRemoteSourceMock()

    override fun getNotificationRemoteSource(): NotificationRemoteSource =
        NotificationRemoteSourceMock()
}