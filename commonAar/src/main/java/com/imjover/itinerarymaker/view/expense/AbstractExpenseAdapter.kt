/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.expense

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.common.di.ApplicationType
import com.imjover.itinerarymaker.databinding.RowExpenseBinding
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.helper.toImageRes
import com.imjover.itinerarymaker.util.toStringMonetary
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter


/**
 * List adapter of [Expense] item.
 */
abstract class AbstractExpenseAdapter : UpdatableRecyclerViewAdapter<Expense, RowExpenseBinding>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterViewHolder<RowExpenseBinding> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = RowExpenseBinding.inflate(layoutInflater, parent, false)
        return AdapterViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: AdapterViewHolder<RowExpenseBinding>, position: Int) {
        val expense = getData(position)
        val showDivider = position + 1 < itemCount
        // Bind instances of [Expense] to the row.
        holder.binding.expense = expense
        holder.binding.cbIsDone.isChecked = expense.isDone
        holder.binding.ivExpenseType.setImageResource(expense.expenseType.toImageRes())
        holder.binding.tvPrice.text = expense.price.toStringMonetary()
        holder.binding.divider.visibility = if (showDivider) View.VISIBLE else View.GONE
        if (AppComponent.appType == ApplicationType.AppAgency) {
            holder.binding.container.setOnClickListener(getOnClickListener(expense.id))
        }
        holder.binding.executePendingBindings()
    }
}