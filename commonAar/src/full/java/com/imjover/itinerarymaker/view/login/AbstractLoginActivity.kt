/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.ActivityLoginBinding
import com.imjover.itinerarymaker.view.AbstractActivity
import com.imjover.itinerarymaker.viewmodel.login.AbstractLoginViewModel


private const val SIGN_IN_KEY = 1000

/**
 * The login screen.
 */
abstract class AbstractLoginActivity<VM : AbstractLoginViewModel> :
    BaseLoginActivity<VM, ActivityLoginBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        login()
        viewModel.loggedOut.observe(this, { it.consume {
            // Login again so the user can choose different account
            login()
        }})
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SIGN_IN_KEY) {
            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                viewModel.doFakeLogin()
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                val response = IdpResponse.fromResultIntent(data)
                showMessage(
                    if (response == null) {
                        getString(R.string.login_activity__login_cancelled__message)
                    } else {
                        getString(
                            R.string.login_activity__login_failed__message,
                            response.error?.errorCode
                        )
                    }
                )
                finish()
            }
        }
    }

    override fun showMessage(message: String) {
        dataBinding.tvMessage.text = message
    }

    protected fun login() {
        // Choose authentication providers
        val providers = arrayListOf(
            AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.FacebookBuilder().build()
        )
        // Create and launch sign-in intent
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                // .setLogo(R.drawable.my_great_logo) TODO: Add logo
                .setTheme(R.style.AppTheme)
                .build(),
            SIGN_IN_KEY
        )
    }

    /***********************************************************************************************
     * Required functions of [AbstractActivity].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.activity_login

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}