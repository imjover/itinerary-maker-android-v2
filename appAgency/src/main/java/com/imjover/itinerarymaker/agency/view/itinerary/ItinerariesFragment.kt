/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.itinerary

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.FragmentItinerariesBinding
import com.imjover.itinerarymaker.agency.viewmodel.itinerary.ItinerariesViewModel
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.entity.Status
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.FormOption
import com.imjover.itinerarymaker.view.itinerary.AbstractItinerariesFragment
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


/**
 * The screen that holds all itineraries.
 */
class ItinerariesFragment :
    AbstractItinerariesFragment<ItinerariesViewModel, FragmentItinerariesBinding>() {

    private var isEditable: Boolean = false
    private lateinit var menu: Menu

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get the arguments passed from previous screen
        val args = ItinerariesFragmentArgs.fromBundle(requireArguments())
        isEditable = args.isEditable
        viewModel.setParentIds(args.parentIds)

        // Setup the UI
        if (isEditable) {
            dataBinding.fabAddItinerary.setOnClickListener {
                val action =
                    ItinerariesFragmentDirections.actionItinerariesFragmentToItineraryFormDialog(
                        FormOption.ADD,
                        viewModel.getParentIds(),
                        null
                    )
                findNavController().navigate(action)
            }
        } else {
            dataBinding.fabAddItinerary.visibility = View.GONE
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        dataBinding.fabSubmitReview.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())
            builder.setMessage(R.string.itinerary_fragment__submit_travel__question)
                .setPositiveButton(android.R.string.yes) { _, _ ->
                    viewModel.submitTravel()
                }
                .setNegativeButton(android.R.string.no) { _, _ ->
                    // Do nothing
                }.show()
        }
        viewModel.travelIsCancelled.observe(viewLifecycleOwner, {
            findNavController().popBackStack()
        })
    }

    override fun onResume() {
        super.onResume()
        // Update Activity's title
        jobGetTravel = lifecycleScope.launch {
            viewModel.getTravel().collect {
                // FIXME: Activity's title is NOT updated sometimes (usually starting 2nd attempt).
                // Why not pass the name directly instead of just ID? then manually update the title bar
                requireActivity().title = it.name

                // Update visibility of menus
                menu.findItem(R.id.pay_invoice_menu).isVisible = !it.status.isEndState()
                dataBinding.fabSubmitReview.visibility =
                    if (TRAVEL_STATUS_TO_SHOW_SUBMIT_MENU.contains(it.status) && isEditable)
                        View.VISIBLE else View.GONE
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.itineraries_menu, menu)
        this.menu = menu
        menu.findItem(R.id.edit_menu).isVisible = isEditable
        menu.findItem(R.id.cancel_menu).isVisible = isEditable
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.invoice_menu -> {
                generateInvoice()
                true
            }
            R.id.pay_invoice_menu -> {
                val builder = AlertDialog.Builder(requireContext())
                builder.setMessage(R.string.itinerary_fragment__pay_invoice__question)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        val action =
                            ItinerariesFragmentDirections.actionItinerariesFragmentToPaymentFormDialog(
                                viewModel.getParentTravelId(),
                                viewModel.getParentUserId()
                            )
                        findNavController().navigate(action)
                    }
                    .setNegativeButton(android.R.string.no) { _, _ ->
                        // Do nothing
                    }.show()
                true
            }
            R.id.edit_menu -> {
                val action =
                    ItinerariesFragmentDirections.actionItinerariesFragmentToTravelFormFragment2(
                        FormOption.EDIT,
                        viewModel.getParentTravelId(),
                        viewModel.getParentUserId()
                    )
                findNavController().navigate(action)
                true
            }
            R.id.cancel_menu -> {
                val builder = AlertDialog.Builder(requireContext())
                builder.setMessage(R.string.itinerary_fragment__cancel_travel__question)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        viewModel.cancelTravel()
                    }
                    .setNegativeButton(android.R.string.no) { _, _ ->
                        // Do nothing
                    }.show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showMessage(message: String) {
        Snackbar.make(dataBinding.fabAddItinerary, message, Snackbar.LENGTH_LONG).show()
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_itineraries

    override fun getViewModelClass(): Class<ItinerariesViewModel> =
        ItinerariesViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }

    override fun getRecyclerView(): RecyclerView =
        dataBinding.rvItineraryList

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout =
        dataBinding.swipeContainer

    override fun getAdapter(): UpdatableRecyclerViewAdapter<Itinerary, *> =
        ItineraryAdapter(viewModel.getParentIds()) { isEditable }

    companion object {
        private val TRAVEL_STATUS_TO_SHOW_SUBMIT_MENU =
            listOf(Status.IN_PROGRESS, Status.REVIEWED_OK, Status.REVIEWED_DECLINED)
    }
}