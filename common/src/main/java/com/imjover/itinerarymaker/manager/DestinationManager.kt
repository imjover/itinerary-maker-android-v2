/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager

import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.DestinationType
import com.imjover.itinerarymaker.entity.Entity
import com.imjover.itinerarymaker.entity.TransportationMode
import com.imjover.itinerarymaker.util.Result
import java.util.*


/**
 * Manages the destinations of the application.
 */
interface DestinationManager : SyncableManager<Destination>,
    ChildManager<Destination> {

    /***********************************************************************************************
     * CRUD methods.
     **********************************************************************************************/

    /**
     * Retrieves the [Destination].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Destination].
     * @return The [Destination].
     */
    suspend fun getDestination(parentIds: Array<String>, id: String): Destination

    /**
     * Inserts a [Destination].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param description The label to be used for this record.
     * @param timestamp The date of travel.
     * @param destinationType The type of destination.
     * @param transportationMode The transportation mode to be used for this travel.
     * @param latitude The destination's latitude.
     * @param longitude The destination's longitude.
     * @return The ID of the [Destination].
     */
    suspend fun insert(
        parentIds: Array<String>,
        description: String,
        timestamp: Date,
        destinationType: DestinationType,
        transportationMode: TransportationMode,
        latitude: Double,
        longitude: Double
    ): String

    /**
     * Updates a [Destination].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param oldDestination The old destination record to be updated.
     * @param description The label to be used for this record.
     * @param timestamp The date of travel.
     * @param destinationType The type of destination.
     * @param transportationMode The transportation mode to be used for this travel.
     * @param latitude The destination's latitude.
     * @param longitude The destination's longitude.
     */
    suspend fun update(
        parentIds: Array<String>,
        oldDestination: Destination,
        description: String,
        timestamp: Date,
        destinationType: DestinationType,
        transportationMode: TransportationMode,
        latitude: Double,
        longitude: Double
    )

    /**
     * Deletes the [Destination]s.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param destinations The [Destination]s to be deleted.
     */
    suspend fun delete(parentIds: Array<String>, vararg destinations: Destination)


    /***********************************************************************************************
     * Other methods.
     **********************************************************************************************/

    /**
     * Update the is visited column of this destination.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Entity].
     * @param isVisited Whether this destination is visited, else, false.
     */
    suspend fun updateIsVisited(
        parentIds: Array<String>,
        id: String,
        isVisited: Boolean
    ): Result<Unit>
}