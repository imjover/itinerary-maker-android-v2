/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.notification

import androidx.navigation.fragment.findNavController
import com.imjover.itinerarymaker.agency.viewmodel.notification.NotificationsViewModel
import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.NotificationType
import com.imjover.itinerarymaker.view.AbstractRefreshableFragment
import com.imjover.itinerarymaker.view.notification.AbstractNotificationsFragment
import com.imjover.itinerarymaker.viewmodel.notification.AbstractNotificationsViewModel


/**
 * The screen that holds all notifications.
 */
class NotificationsFragment : AbstractNotificationsFragment() {

    override fun handleNotification(notification: Notification) {
        when (notification.notificationType) {
            NotificationType.TRAVEL_REVIEW -> {
                val action =
                    NotificationsFragmentDirections.actionNotificationsFragmentToItinerariesFragment(
                        arrayOf(notification.submittedBy, notification.transactionId)
                    )
                findNavController().navigate(action)
            }
            else -> throw IllegalStateException()
        }
    }


    /***********************************************************************************************
     * Required functions of [AbstractRefreshableFragment].
     **********************************************************************************************/

    override fun getViewModelClass(): Class<AbstractNotificationsViewModel> =
        NotificationsViewModel::class.java as Class<AbstractNotificationsViewModel>
}