/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository

import com.imjover.itinerarymaker.entity.Expense

/**
 * Represents the repository data source of [Expense].
 */
interface ExpenseRepository :
    CrudRepository<Expense>,
    SyncableRepository<Expense>