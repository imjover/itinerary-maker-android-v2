/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source.internal

import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.repository.BuildConfig
import com.imjover.itinerarymaker.repository.ExpenseRepository
import com.imjover.itinerarymaker.source.local.ExpenseLocalSource
import com.imjover.itinerarymaker.source.remote.ExpenseRemoteSource
import kotlinx.coroutines.CoroutineDispatcher


internal class ExpenseRepositoryImpl(
    localSource: ExpenseLocalSource,
    remoteSource: ExpenseRemoteSource,
    dispatcher: CoroutineDispatcher
) : AbstractSyncableCrudRepository<Expense, String, ExpenseLocalSource, ExpenseRemoteSource>(
    localSource,
    remoteSource,
    dispatcher
), ExpenseRepository {

    override fun toString(): String {
        return if (BuildConfig.DEBUG)
            "ExpenseRepositoryImpl"
        else
            super.toString()
    }
}