/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.travel

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.traveler.R
import com.imjover.itinerarymaker.traveler.databinding.FragmentTravelsBinding
import com.imjover.itinerarymaker.traveler.viewmodel.travel.TravelsViewModel
import com.imjover.itinerarymaker.view.AbstractRefreshableFragment
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


/**
 * The screen that holds all travels.
 */
class TravelsFragment :
    AbstractRefreshableFragment<Travel, TravelsViewModel, FragmentTravelsBinding>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.travels_menu, menu)

        val menuItem = menu.findItem(R.id.notification_menu)
        val actionView = menuItem.actionView
        val tvBadge: TextView = actionView.findViewById(R.id.tv_badge)
        actionView.setOnClickListener { onOptionsItemSelected(menuItem) }

        lifecycleScope.launch {
            viewModel.getNewNotificationCount().collect {
                tvBadge.text = it.toString()
                tvBadge.visibility = if (it > 0) View.VISIBLE else View.GONE
            }
        }

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.notification_menu -> {
                val action = TravelsFragmentDirections.actionTravelsFragmentToNotificationsFragment()
                findNavController().navigate(action)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    /***********************************************************************************************
     * Required functions of [AbstractRefreshableFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_travels

    override fun getViewModelClass(): Class<TravelsViewModel> =
        TravelsViewModel::class.java

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }

    override fun getRecyclerView(): RecyclerView =
        dataBinding.rvTravelList

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout =
        dataBinding.swipeContainer

    override fun getAdapter(): UpdatableRecyclerViewAdapter<Travel, *> =
        TravelAdapter(viewModel.getParentIds())
}