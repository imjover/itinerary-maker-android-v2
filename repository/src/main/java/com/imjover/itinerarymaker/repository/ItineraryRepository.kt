/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository

import com.imjover.itinerarymaker.entity.Itinerary

/**
 * Represents the repository data source of [Itinerary].
 */
interface ItineraryRepository :
    CrudRepository<Itinerary>,
    SyncableRepository<Itinerary>