/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository

import com.imjover.itinerarymaker.entity.SyncableEntity
import kotlinx.coroutines.flow.Flow


/**
 * Defines repositories that can be synced in both remote and local sources.
 */
interface SyncableRepository<Child: SyncableEntity> {

    /**
     * Retrieves the [Child]ren of the given [parentIds].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @return List of [Child]ren.
     */
    suspend fun getListChildrenOf(parentIds: Array<String>): List<Child>

    /**
     * Retrieves the [Child]ren of the given [parentIds].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @return Stream of [Child]ren.
     */
    fun getStreamChildrenOf(parentIds: Array<String>): Flow<Child>

    /**
     * Synchronize all [Child]ren of the given [parentIds] on both local and remote sources.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     */
    @Deprecated("Firebase automatically syncs Firestore")
    suspend fun syncChildrenOf(parentIds: Array<String>)
}