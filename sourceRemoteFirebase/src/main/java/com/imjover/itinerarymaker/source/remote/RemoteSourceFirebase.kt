/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import com.imjover.itinerarymaker.source.RemoteSource
import com.imjover.itinerarymaker.source.remote.firebase.*
import com.imjover.itinerarymaker.source.remote.firebase.UserRemoteSourceImpl
import javax.inject.Inject

/**
 * Remote implementation of repository that uses Firebase.
 */
internal class RemoteSourceFirebase @Inject constructor() : RemoteSource {

    /**
     * Firebase functions can point to Firebase local emulator by:
     *  functions = FirebaseFunctions.getInstance()
     *  functions.useEmulator("10.0.2.2", 5001)
     *
     * 10.0.2.2 is the special IP address to connect to the 'localhost' of
     * the host computer from an Android emulator.
     */
    private val functions: FirebaseFunctions = Firebase.functions
    private val auth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun getUserRemoteSource(): UserRemoteSource =
        UserRemoteSourceImpl(auth, functions)

    override fun getTravelRemoteSource(): TravelRemoteSource =
        TravelRemoteSourceImpl()

    override fun getItineraryRemoteSource(): ItineraryRemoteSource =
        ItineraryRemoteSourceImpl()

    override fun getDestinationRemoteSource(): DestinationRemoteSource =
        DestinationRemoteSourceImpl()

    override fun getExpenseRemoteSource(): ExpenseRemoteSource =
        ExpenseRemoteSourceImpl()

    override fun getNotificationRemoteSource(): NotificationRemoteSource =
        NotificationRemoteSourceImpl()
}