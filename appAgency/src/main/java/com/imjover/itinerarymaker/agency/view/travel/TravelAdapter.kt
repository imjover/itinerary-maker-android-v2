/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.travel

import android.view.View
import androidx.navigation.findNavController
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.view.travel.AbstractTravelAdapter


/**
 * List adapter of [Travel] item.
 */
class TravelAdapter(
    private val parentIds: Array<String>,
    private val isUserAllowedToEdit: Boolean
) : AbstractTravelAdapter() {

    override fun isUserAllowedToEdit(): Boolean =
        isUserAllowedToEdit

    override fun getOnClickListener(id: String, isEditable: Boolean): View.OnClickListener? =
        View.OnClickListener {
            val action = TravelsFragmentDirections.actionTravelsFragmentToItinerariesFragment(
                parentIds.plus(id),
                isEditable
            )
            it.findNavController().navigate(action)
        }

    override fun getOnClickListener(id: String): View.OnClickListener? =
        throw IllegalStateException()
}