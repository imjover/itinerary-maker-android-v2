/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.internal

import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.source.local.DestinationLocalSource
import com.imjover.itinerarymaker.source.local.room.DatabaseFactory
import com.imjover.itinerarymaker.source.local.room.dao.DestinationDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged


internal class DestinationLocalSourceImpl(
    private val destinationDao: DestinationDao = DatabaseFactory.getDatabase().destinationDao()
) : AbstractLocalCrudSource<Destination>(destinationDao),
    DestinationLocalSource {

    @ExperimentalCoroutinesApi
    override fun getStreamChildrenOf(parentId: String): Flow<List<Destination>> =
        destinationDao.getStreamDestinationsOf(parentId)
            .distinctUntilChanged()

    override suspend fun getListChildrenOf(parentId: String): List<Destination> =
        destinationDao.getDestinationsOf(parentId)

    override suspend fun getListChildrenOf(parentId: String, syncStatus: SyncStatus): List<Destination> =
        destinationDao.getDestinationsOf(parentId, syncStatus)

    override suspend fun updateId(toUpdate: Destination, newId: String, syncStatus: SyncStatus) {
        destinationDao.updateId(toUpdate.id, newId, syncStatus)
    }
}