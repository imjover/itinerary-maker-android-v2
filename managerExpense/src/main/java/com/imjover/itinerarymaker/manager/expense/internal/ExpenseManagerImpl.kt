/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.expense.internal

import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.entity.ExpenseType
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.manager.ExpenseManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.repository.ExpenseRepository
import com.imjover.itinerarymaker.repository.di.RepositoryFactory
import com.imjover.itinerarymaker.util.IdGenerator
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject


internal class ExpenseManagerImpl @Inject constructor() : ExpenseManager {

    @Inject lateinit var userManager: UserManager

    private val expenseRepository: ExpenseRepository =
        RepositoryFactory.singleton().getExpenseRepository()


    /***********************************************************************************************
     * Child-related methods.
     **********************************************************************************************/

    override suspend fun getListChildrenOf(parentIds: Array<String>): List<Expense> =
        expenseRepository.getListChildrenOf(parentIds)


    /***********************************************************************************************
     * Syncable-related methods.
     **********************************************************************************************/

    override fun getStreamChildrenOf(parentIds: Array<String>): Flow<Expense> =
        expenseRepository.getStreamChildrenOf(parentIds)

    override suspend fun syncChildrenOf(parentIds: Array<String>) {
        expenseRepository.syncChildrenOf(parentIds)
    }


    /***********************************************************************************************
     * CRUD methods.
     **********************************************************************************************/

    override suspend fun getExpense(parentIds: Array<String>, id: String): Expense =
        expenseRepository.getCached(parentIds, id)

    override suspend fun insert(
        parentIds: Array<String>,
        description: String,
        expenseType: ExpenseType,
        capital: BigDecimal,
        price: BigDecimal,
        remarks: String?,
        isDone: Boolean
    ): String {
        val loggedInUser = userManager.getLoggedInUser().id
        val expense = Expense(
            IdGenerator.generate("ELM", loggedInUser),
            description,
            expenseType,
            capital,
            price,
            remarks,
            isDone,
            Date(),
            loggedInUser,
            SyncStatus.TO_INSERT
        )
        return expenseRepository.insert(parentIds, expense)
    }

    override suspend fun update(
        parentIds: Array<String>,
        oldExpense: Expense,
        description: String,
        expenseType: ExpenseType,
        capital: BigDecimal,
        price: BigDecimal,
        remarks: String?,
        isDone: Boolean
    ) {
        val loggedInUser = userManager.getLoggedInUser().id
        val expense = Expense(
            oldExpense.id,
            description,
            expenseType,
            capital,
            price,
            remarks,
            isDone,
            Date(),
            loggedInUser,
            SyncStatus.TO_UPDATE
        )
        expenseRepository.update(parentIds, expense)
    }

    override suspend fun delete(parentIds: Array<String>, vararg expenses: Expense) {
        expenseRepository.delete(parentIds, *expenses)
    }
}