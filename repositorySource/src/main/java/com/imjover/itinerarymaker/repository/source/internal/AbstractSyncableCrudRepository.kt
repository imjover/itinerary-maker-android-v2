/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source.internal

import android.util.Log
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.entity.SyncableEntity
import com.imjover.itinerarymaker.repository.CrudRepository
import com.imjover.itinerarymaker.repository.SyncableRepository
import com.imjover.itinerarymaker.source.type.LocalSyncableCrudChildSource
import com.imjover.itinerarymaker.source.type.RemoteCrudChildSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

/**
 * Defines the repositories that can be synced, and has CRUD.
 */
@Suppress("UNCHECKED_CAST")
internal abstract class AbstractSyncableCrudRepository<
        SE : SyncableEntity,
        ParentId,
        LS : LocalSyncableCrudChildSource<SE, ParentId>,
        RS : RemoteCrudChildSource<SE, ParentId>>(
    protected val localSource: LS,
    protected val remoteSource: RS,
    protected val dispatcher: CoroutineDispatcher
) : CrudRepository<SE>,
    SyncableRepository<SE, ParentId> {


    /***********************************************************************************************
     * Syncable-related methods.
     **********************************************************************************************/

    override suspend fun getListChildrenOf(parentId: ParentId): List<SE> =
        localSource.getListChildrenOf(parentId)

    @ExperimentalCoroutinesApi
    override fun getStreamChildrenOf(parentId: ParentId): Flow<List<SE>> =
        localSource.getStreamChildrenOf(parentId)

    override suspend fun syncChildrenOf(parentId: ParentId) {
        withContext(dispatcher) {
            // Pull latest data from remote source.
            val remoteEntities = remoteSource.getListChildrenOf(parentId)

            // TODO: Implement handling of different dirty records coming from multiple offline devices.
            //  Compare clean records vs records that are dirty (and has temporary ID?).
            // Imagine two devices are offline for a while but used at the same time that created
            //  multiple new records (dirty or has temporary id), then both were attempted to synced
            //  records at the server.

            // Insert or update data cached in local source.
            remoteEntities.forEach {
                localSource.insertOrUpdate(it)
            }

            // Synchronize ALL dirty records from local to remote source.
            try {
                localSource.getListChildrenOf(parentId, SyncStatus.TO_INSERT)
                    .forEach {
                        val newId = remoteSource.insert(it)
                        localSource.updateId(it, newId, SyncStatus.CLEAN)
                    }

                localSource.getListChildrenOf(parentId, SyncStatus.TO_UPDATE)
                    .forEach {
                        remoteSource.update(it)
                        localSource.update(it.cloneAndSetArg(SyncStatus.CLEAN) as SE)
                    }

                localSource.getListChildrenOf(parentId, SyncStatus.TO_DELETE)
                    .forEach {
                        remoteSource.delete(it)
                        localSource.delete(it)
                    }
            } catch (exception: Exception) {
                // FIXME: Change exception to remote-caused exception ONLY e.g. no internet
                Log.e(toString(), exception.message, exception)
            }

            // Delete cached entities that are deleted already in remote source
            localSource.getListChildrenOf(parentId, SyncStatus.CLEAN)
                .subtract(remoteEntities)
                .forEach { localSource.delete(it) }
        }
    }


    /***********************************************************************************************
     * CRUD methods.
     **********************************************************************************************/

    override suspend fun getCached(id: String): SE =
        localSource.get(id)

    override fun getCachedAsStream(id: String): Flow<SE> =
        localSource.getAsStream(id)

    override suspend fun insert(entity: SE): String {
        if (entity.getSyncableStatus() != SyncStatus.TO_INSERT) {
            throw IllegalStateException()
        }
        return withContext(dispatcher) {
            val insertedEntity =
                try {
                    // Try to insert on remote source.
                    val remoteId = remoteSource.insert(entity)
                    entity.cloneAndSetArg(remoteId, SyncStatus.CLEAN)
                } catch (exception: Exception) {
                    // FIXME: Change exception to remote-caused exception ONLY e.g. no internet
                    Log.e(toString(), exception.message, exception)
                    entity
                } as SE
            localSource.insert(insertedEntity)
            return@withContext insertedEntity.getUniqueId()
        }
    }

    override suspend fun update(entity: SE) {
        if (entity.getSyncableStatus() != SyncStatus.TO_UPDATE) {
            throw IllegalStateException()
        }
        withContext(dispatcher) {
            val updatedEntity =
                try {
                    // Try to update on remote source.
                    remoteSource.update(entity)
                    entity.cloneAndSetArg(SyncStatus.CLEAN)
                } catch (exception: Exception) {
                    // FIXME: Change exception to remote-caused exception ONLY e.g. no internet
                    Log.e(toString(), exception.message, exception)
                    entity
                } as SE

            localSource.update(updatedEntity)
        }
    }

    override suspend fun delete(vararg entities: SE) {
        withContext(dispatcher) {
            entities.forEach { entity ->
                val isDeletedOnRemote =
                    try {
                        // Try to delete on remote source.
                        remoteSource.delete(entity)
                        true
                    } catch (exception: Exception) {
                        // FIXME: Change exception to remote-caused exception ONLY e.g. no internet
                        Log.e(toString(), exception.message, exception)
                        false
                    }

                if (isDeletedOnRemote) {
                    localSource.delete(entity)
                } else {
                    val toDeleteEntity = entity.cloneAndSetArg(SyncStatus.TO_DELETE) as SE
                    localSource.update(toDeleteEntity)
                }
            }
        }
    }
}