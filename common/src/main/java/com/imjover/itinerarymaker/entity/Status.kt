/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * Status of the entity.
 */
enum class Status(val id: Int) {

    OK(0),
    IN_PROGRESS(1),
    FOR_REVIEW(2),
    REVIEWED_OK(3),
    REVIEWED_DECLINED(4),
    // End state
    PAID(-1),
    DELETED(-2),
    CANCELLED(-3);


    companion object {
        /**
         * Retrieves the equivalent [Status] base on the given ID.
         *
         * @param id The id of the [Status].
         * @return The retrieved [Status].
         */
        fun fromId(id: Int): Status {
            values().forEach {
                if (it.id == id) {
                    return it
                }
            }
            throw IllegalStateException()
        }
    }

    /**
     * Retrieve whether the status is in end state, or not.
     *
     * @return Whether the status is in end state, then true, else false.
     */
    fun isEndState(): Boolean {
        return id < 0
    }
}