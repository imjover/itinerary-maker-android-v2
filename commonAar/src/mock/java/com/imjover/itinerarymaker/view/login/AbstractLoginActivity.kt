/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.login

import android.animation.LayoutTransition
import android.os.Bundle
import android.view.KeyEvent
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.ActivityLoginBinding
import com.imjover.itinerarymaker.view.AbstractActivity
import com.imjover.itinerarymaker.viewmodel.login.AbstractLoginViewModel


/**
 * The login screen.
 */
abstract class AbstractLoginActivity<VM : AbstractLoginViewModel> :
    BaseLoginActivity<VM, ActivityLoginBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Setup UI
        dataBinding.formContainer.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
        dataBinding.etPassword.setOnEditorActionListener { _, _, event ->
            var handled = false
            if (event.keyCode == KeyEvent.KEYCODE_ENTER) {
                dataBinding.btnLogin.performClick()
                handled = true
            }
            return@setOnEditorActionListener handled
        }
        // Setup ViewModel
        viewModel.errorMessage.observe(this, Observer {
            dataBinding.etPassword.setText("")
            Snackbar.make(dataBinding.btnLogin, it, Snackbar.LENGTH_LONG)
                .show()
        })
    }


    /***********************************************************************************************
     * Required functions of [BaseLoginActivity].
     **********************************************************************************************/

    override fun showMessage(message: String) {
        Toast.makeText(
            this,
            message,
            Toast.LENGTH_LONG
        ).show()
    }


    /***********************************************************************************************
     * Required functions of [AbstractActivity].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.activity_login

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}