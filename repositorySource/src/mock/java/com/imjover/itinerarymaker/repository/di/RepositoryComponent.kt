/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.di

import com.imjover.itinerarymaker.repository.Repository
import com.imjover.itinerarymaker.repository.internal.RepositoryImpl
import com.imjover.itinerarymaker.source.local.di.LocalSourceRoomModule
import com.imjover.itinerarymaker.source.remote.di.RemoteSourceMockModule
import dagger.Component
import javax.inject.Singleton

/**
 * Injects the required modules of the [Repository].
 */
@Singleton
@Component(modules = [LocalSourceRoomModule::class, RemoteSourceMockModule::class])
internal interface RepositoryComponent {

    fun inject(repository: RepositoryImpl)
}