/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.util

/**
 * Consumes a value only once.
 */
class Consumable<T>(private val data: T) {

    private var consumed = false

    fun consume(block: Consumable<T>.(T) -> Unit) {
        if (!consumed) {
            consumed = true
            block(data)
        }
    }
}