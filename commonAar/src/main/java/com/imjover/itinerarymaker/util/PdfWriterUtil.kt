/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.util

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.core.content.FileProvider
import com.itextpdf.text.Document
import com.itextpdf.text.DocumentException
import com.itextpdf.text.Image
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.tool.xml.XMLWorkerHelper
import java.io.*


/**
 * Utility class to handle writing of PDF files.
 */
class PdfWriterUtil {

    companion object {
        /**
         * Writes a PDF file.
         *
         * @param filePath The path + filename where the PDF file will be saved (written).
         * @param contentHtml The content of the PDF in HTML format.
         */
        fun write(filePath: String, contentHtml: String) {
            write(File(filePath), null, contentHtml)
        }

        /**
         * Writes a PDF file.
         *
         * @param file The path + filename where the PDF file will be saved (written).
         * @param contentHtml The content of the PDF in HTML format.
         */
        fun write(file: File, logo: Drawable?, contentHtml: String) {
            if (file.exists()) {
                // Delete the file so we can create new one
                file.delete()
            }
            if (!file.createNewFile()) {
                throw IOException("Failed to generate PDF file at: ${file.absolutePath}")
            }

            val document = Document()
            val pdfWriter = try {
                PdfWriter.getInstance(document, FileOutputStream(file.absoluteFile))
            } catch (ex: DocumentException) {
                throw IOException(ex)
            }
            val fis: InputStream = contentHtml.byteInputStream(Charsets.UTF_8)
            document.open()

            logo?.let {
                val bmpDrawable = it as BitmapDrawable
                val bmp = bmpDrawable.bitmap
                val stream = ByteArrayOutputStream()
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val image: Image = Image.getInstance(stream.toByteArray())
                image.alignment = Image.LEFT or Image.TEXTWRAP
                document.add(image)
            }
            val worker = XMLWorkerHelper.getInstance()
            worker.parseXHtml(pdfWriter, document, fis)

            document.close()
        }

        /**
         * Retrieves the intent of a PDF viewer.
         *
         * @param context The context.
         * @param path The path to the file to be viewed.
         */
        fun getPdfViewer(context: Context, path: String): Intent {
            val path: Uri = FileProvider.getUriForFile(
                context,
                context.applicationContext.packageName + ".provider",
                File(path)
            )
            // Setting the intent for pdf reader
            val pdfIntent = Intent(Intent.ACTION_VIEW)
            pdfIntent.setDataAndType(path, "application/pdf")
            pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            return pdfIntent
        }
    }
}