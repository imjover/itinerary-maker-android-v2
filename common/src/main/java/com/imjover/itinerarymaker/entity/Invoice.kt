/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

import androidx.room.*
import androidx.room.Entity
import java.math.BigDecimal
import java.util.*


/**
 * The invoice of a [Travel].
 *
 * @param id The unique ID and primary key.
 * @param travelId The foreign key of the parent of this object.
 * @param totalAmount The total amount of all expenses.
 * @param expensesIds The IDs of the expenses.
 * @param lastUpdated The most recent date this record was updated.
 * @param lastUpdatedBy The most recent user who updated this record.
 */
@Entity(
    tableName = "tbl_invoice",
    indices = [
        Index("fk_travel_id")
    ],
    foreignKeys = [
        ForeignKey(
            entity = Travel::class,
            parentColumns = ["pk_travel_id"],
            childColumns = ["fk_travel_id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Invoice(
    @PrimaryKey @ColumnInfo(name = "pk_invoice_id") val id: String,
    @ColumnInfo(name = "fk_travel_id") val travelId: String,
    @ColumnInfo(name = "total_amount") val totalAmount: BigDecimal,
    @ColumnInfo(name = "expenses_ids") val expensesIds: String,
    // Audit Trail
    @ColumnInfo(name = "last_updated") val lastUpdated: Date,
    @ColumnInfo(name = "fk_last_updated_by") val lastUpdatedBy: String
) : ChildEntity {

    override fun getUniqueId(): String =
        id

    override fun getForeignKey(): String =
        travelId
}