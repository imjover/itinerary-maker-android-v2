/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.di

import androidx.room.RoomDatabase
import com.imjover.itinerarymaker.source.local.internal.LocalSourceRoom
import com.imjover.itinerarymaker.source.LocalSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the local implementation of [LocalSource] that uses [RoomDatabase].
 */
@Module
abstract class LocalSourceRoomModule {

    @Singleton
    @Binds
    internal abstract fun bindLocalSource(localSource: LocalSourceRoom): LocalSource
}