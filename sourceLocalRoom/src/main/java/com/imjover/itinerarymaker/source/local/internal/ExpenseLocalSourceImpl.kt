/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.internal

import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.source.local.ExpenseLocalSource
import com.imjover.itinerarymaker.source.local.room.DatabaseFactory
import com.imjover.itinerarymaker.source.local.room.dao.ExpenseDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged


internal class ExpenseLocalSourceImpl(
    private val expenseDao: ExpenseDao = DatabaseFactory.getDatabase().expenseDao()
) : AbstractLocalCrudSource<Expense>(expenseDao),
    ExpenseLocalSource {

    @ExperimentalCoroutinesApi
    override fun getStreamChildrenOf(parentId: String): Flow<List<Expense>> =
        expenseDao.getStreamExpensesOf(parentId)
            .distinctUntilChanged()

    override suspend fun getListChildrenOf(parentId: String): List<Expense> =
        expenseDao.getExpensesOf(parentId)

    override suspend fun getListChildrenOf(parentId: String, syncStatus: SyncStatus): List<Expense> =
        expenseDao.getExpensesOf(parentId, syncStatus)

    override suspend fun updateId(toUpdate: Expense, newId: String, syncStatus: SyncStatus) {
        expenseDao.updateId(toUpdate.id, newId, syncStatus)
    }
}