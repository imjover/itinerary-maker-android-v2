/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source.internal

import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.repository.BuildConfig
import com.imjover.itinerarymaker.repository.ItineraryRepository
import com.imjover.itinerarymaker.source.local.ItineraryLocalSource
import com.imjover.itinerarymaker.source.remote.ItineraryRemoteSource
import kotlinx.coroutines.CoroutineDispatcher


internal class ItineraryRepositoryImpl(
    localSource: ItineraryLocalSource,
    remoteSource: ItineraryRemoteSource,
    dispatcher: CoroutineDispatcher
) : AbstractSyncableCrudRepository<Itinerary, String, ItineraryLocalSource, ItineraryRemoteSource>(
    localSource,
    remoteSource,
    dispatcher
), ItineraryRepository {

    override fun toString(): String {
        return if (BuildConfig.DEBUG)
            "ItineraryRepositoryImpl"
        else
            super.toString()
    }
}