/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.helper

import androidx.annotation.StringRes
import com.google.firebase.FirebaseException
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.common.di.AppComponent


/**
 * Returns the throwable's message or a user-friendly error message.
 */
fun Throwable.getMessageForUI(): String =
    getMessageForUI(R.string.error_message__general_error)

/**
 * Returns the throwable's message or a user-friendly error message.
 *
 * @param defaultMsgRes The default message in case the exception's message is invalid.
 */
fun Throwable.getMessageForUI(@StringRes defaultMsgRes: Int): String =
    if (this is FirebaseException && message != null) {
        message!!
    } else {
        AppComponent.appContext.getString(defaultMsgRes)
    }