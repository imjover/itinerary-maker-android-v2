/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

import androidx.room.*
import androidx.room.Entity
import java.util.*


/**
 * The Destination of an [Itinerary].
 *
 * @param id The unique ID and primary key.
 * @param name The label to be used for this record.
 * @param timestamp The date of travel.
 * @param destinationType The type of destination.
 * @param transportationMode The transportation mode to be used for this travel.
 * @param latitude The destination's latitude.
 * @param longitude The destination's longitude.
 * @param isVisited Whether this destination was visited already, else, false.
 * @param lastUpdated The most recent date this record was updated.
 * @param lastUpdatedBy The most recent user who updated this record.
 * @param syncStatus The current synchronization state of this record.
 */
@Entity(
    tableName = "tbl_destination",
)
data class Destination(
    @PrimaryKey @ColumnInfo(name = "pk_destination_id") val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "timestamp") val timestamp: Date,
    @ColumnInfo(name = "destination_type") val destinationType: DestinationType,
    @ColumnInfo(name = "transportation_mode") val transportationMode: TransportationMode,
    @ColumnInfo(name = "latitude") val latitude: Double,
    @ColumnInfo(name = "longitude") val longitude: Double,
    @ColumnInfo(name = "is_visited") val isVisited: Boolean,
    // Audit Trail
    @ColumnInfo(name = "last_updated") val lastUpdated: Date,
    @ColumnInfo(name = "fk_last_updated_by") val lastUpdatedBy: String,
    // Syncable
    @ColumnInfo(name = "sync_status") val syncStatus: SyncStatus
) : SyncableEntity {

    override fun getUniqueId(): String =
        id

    override fun getSyncableStatus(): SyncStatus =
        syncStatus

    override fun cloneAndSetArg(newSyncStatus: SyncStatus): Destination =
        cloneAndSetArg(this.id, newSyncStatus)

    override fun cloneAndSetArg(newId: String, newSyncStatus: SyncStatus): Destination =
        Destination(
            newId,
            this.name,
            this.timestamp,
            this.destinationType,
            this.transportationMode,
            this.latitude,
            this.longitude,
            this.isVisited,
            this.lastUpdated,
            this.lastUpdatedBy,
            newSyncStatus
        )
}