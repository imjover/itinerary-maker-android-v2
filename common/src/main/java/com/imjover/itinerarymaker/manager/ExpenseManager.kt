/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager

import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.entity.ExpenseType
import java.math.BigDecimal


/**
 * Manages the expenses of the application.
 */
interface ExpenseManager : SyncableManager<Expense>, ChildManager<Expense> {


    /***********************************************************************************************
     * CRUD methods.
     **********************************************************************************************/

    /**
     * Retrieves the [Expense].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Expense].
     * @return The [Expense].
     */
    suspend fun getExpense(parentIds: Array<String>, id: String): Expense

    /**
     * Inserts an [Expense].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param description The label to be used for this record.
     * @param expenseType The type of expense.
     * @param capital Cost of the good or services.
     * @param price Amount to be paid by the customer.
     * @param remarks An optional remark or note.
     * @param isDone Whether this expenses is done, then true, else false.
     * @return The ID of the [Destination].
     */
    suspend fun insert(
        parentIds: Array<String>,
        description: String,
        expenseType: ExpenseType,
        capital: BigDecimal,
        price: BigDecimal,
        remarks: String?,
        isDone: Boolean
    ): String

    /**
     * Updates an [Expense].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param oldExpense The old expense record to be updated.
     * @param description The label to be used for this record.
     * @param expenseType The type of expense.
     * @param capital Cost of the good or services.
     * @param price Amount to be paid by the customer.
     * @param remarks An optional remark or note.
     * @param isDone Whether this expenses is done, then true, else false.
     */
    suspend fun update(
        parentIds: Array<String>,
        oldExpense: Expense,
        description: String,
        expenseType: ExpenseType,
        capital: BigDecimal,
        price: BigDecimal,
        remarks: String?,
        isDone: Boolean
    )

    /**
     * Deletes the [Expense]s.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param expenses The [Expense]s to be deleted.
     */
    suspend fun delete(parentIds: Array<String>, vararg expenses: Expense)
}