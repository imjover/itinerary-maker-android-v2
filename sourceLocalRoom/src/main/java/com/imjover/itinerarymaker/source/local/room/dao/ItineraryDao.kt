/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.entity.Travel
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object of the [Itinerary].
 */
@Dao
internal interface ItineraryDao : CrudDao<Itinerary> {

    /**
     * Retrieve a [Itinerary] with the given [id].
     *
     * @param id The ID of this record.
     * @return The found [Itinerary].
     */
    @Query("SELECT * FROM tbl_itinerary WHERE pk_itinerary_id = :id LIMIT 1")
    suspend fun getItinerary(id: String): Itinerary

    /**
     * Retrieve a stream [Itinerary] with the given [id].
     *
     * @param id The ID of this record.
     * @return The found stream [Itinerary].
     */
    @Query("SELECT * FROM tbl_itinerary WHERE pk_itinerary_id = :id LIMIT 1")
    fun getItineraryAsStream(id: String): Flow<Itinerary>

    /**
     * Retrieve all [Itinerary]s of a [Travel] and converts it into a stream.
     *
     * @param travelId The owner of the records.
     * @return A stream of [Itinerary]s.
     */
    @Query("SELECT * FROM tbl_itinerary WHERE fk_travel_id = :travelId")
    fun getStreamItinerariesOf(travelId: String): Flow<List<Itinerary>>

    /**
     * Retrieve all [Itinerary]s of a [Travel].
     *
     * @param travelId The owner of the records.
     * @return A list of [Itinerary]s.
     */
    @Query("SELECT * FROM tbl_itinerary WHERE fk_travel_id = :travelId")
    suspend fun getItinerariesOf(travelId: String): List<Itinerary>

    /**
     * Retrieves all [Itinerary]s that has [syncStatus] and owned by the given [travelId].
     *
     * @param travelId The owner of the records.
     * @param syncStatus The synchronization status.
     * @return A list of [Itinerary]s.
     */
    @Query("SELECT * FROM tbl_itinerary WHERE fk_travel_id = :travelId AND sync_status = :syncStatus")
    suspend fun getItinerariesOf(travelId: String, syncStatus: SyncStatus): List<Itinerary>

    /**
     * Updates the primary key to [newId] and [syncStatus] of the record with [currentId].
     *
     * @param currentId The current ID of this record.
     * @param newId The new ID to be used on this record.
     * @param syncStatus The synchronization status.
     */
    @Query("UPDATE tbl_itinerary SET pk_itinerary_id = :newId, sync_status = :syncStatus WHERE pk_itinerary_id = :currentId")
    suspend fun updateId(currentId: String, newId: String, syncStatus: SyncStatus)
}