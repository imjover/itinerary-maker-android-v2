/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.util

import java.util.*

/**
 * Helper class to generate ID.
 *
 * Ideally, the IDs generated should ONLY be used in mocking and local caching.
 * But actual and final ID should still come from remote source.
 */
class IdGenerator {

    companion object {

        /**
         * Generates a [String] ID.
         *
         * @param prefix A prefix String added in front of the generated ID.
         * @param generatedByUser The user who generated this ID.
         * @return The generated ID.
         */
        fun generate(prefix: String, generatedByUser: String): String =
            "${prefix}-${generatedByUser}-${UUID.randomUUID()}"
    }
}