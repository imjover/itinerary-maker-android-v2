/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.imjover.itinerarymaker.entity.Invoice

/**
 * Data Access Object of the [Invoice].
 */
@Dao
internal interface InvoiceDao : CrudDao<Invoice> {

    /**
     * Retrieve an [Invoice] with the given [id].
     *
     * @param id The ID of this record.
     * @return The found [Invoice].
     */
    @Query("SELECT * FROM tbl_invoice WHERE pk_invoice_id = :id LIMIT 1")
    suspend fun getInvoice(id: String): Invoice
}