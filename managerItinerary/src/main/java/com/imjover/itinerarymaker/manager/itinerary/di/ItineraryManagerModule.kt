/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.itinerary.di

import com.imjover.itinerarymaker.manager.ItineraryManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.manager.itinerary.internal.ItineraryManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the [ItineraryManager].
 */
@Module
abstract class ItineraryManagerModule(userManager: UserManager) {

    @Singleton
    @Binds
    internal abstract fun bindItineraryManager(itineraryManager: ItineraryManagerImpl): ItineraryManager
}