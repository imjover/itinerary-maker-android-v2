/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.internal

import com.imjover.itinerarymaker.entity.Entity
import com.imjover.itinerarymaker.source.local.room.dao.CrudDao
import com.imjover.itinerarymaker.source.local.room.dao.DestinationDao
import com.imjover.itinerarymaker.source.local.room.dao.ExpenseDao
import com.imjover.itinerarymaker.source.local.room.dao.ItineraryDao
import com.imjover.itinerarymaker.source.local.room.dao.NotificationDao
import com.imjover.itinerarymaker.source.local.room.dao.TravelDao
import com.imjover.itinerarymaker.source.type.LocalCrudSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged

/**
 * Defines the local source implementations that has CRUD.
 */
internal abstract class AbstractLocalCrudSource<E : Entity>(
    private val crudDao: CrudDao<E>
) : LocalCrudSource<E> {

    override suspend fun insert(entity: E) {
        crudDao.insert(entity)
    }

    override suspend fun update(entity: E) {
        crudDao.update(entity)
    }

    override suspend fun insertOrUpdate(entity: E) {
        crudDao.insertOrUpdate(entity)
    }

    override suspend fun delete(entity: E) {
        crudDao.delete(entity)
    }

    @Suppress("UNCHECKED_CAST")
    override suspend fun get(id: String): E {
        // Note: If new NEW DAOs are created, please add below.
        //       Unfortunately, Room doesn't support yet the inheritance of select query.
        //       So we are manually casting the DAOs and retrieve the Entity manually.
        return when (crudDao) {
            is TravelDao -> crudDao.getTravel(id)
            is ItineraryDao -> crudDao.getItinerary(id)
            is DestinationDao -> crudDao.getDestination(id)
            is ExpenseDao -> crudDao.getExpense(id)
            is NotificationDao -> throw IllegalStateException()
            else -> throw IllegalStateException()
        } as E
    }

    @Suppress("UNCHECKED_CAST")
    @ExperimentalCoroutinesApi
    override fun getAsStream(id: String): Flow<E> {
        // Note: If new NEW DAOs are created, please add below.
        //       Unfortunately, Room doesn't support yet the inheritance of select query.
        //       So we are manually casting the DAOs and retrieve the Entity manually.
        return when (crudDao) {
            is TravelDao -> crudDao.getTravelAsStream(id)
            is ItineraryDao -> crudDao.getItineraryAsStream(id)
            is DestinationDao -> crudDao.getDestinationAsStream(id)
            is ExpenseDao -> crudDao.getExpenseAsStream(id)
            is NotificationDao -> throw IllegalStateException()
            else -> throw IllegalStateException()
        }.distinctUntilChanged() as Flow<E>
    }
}