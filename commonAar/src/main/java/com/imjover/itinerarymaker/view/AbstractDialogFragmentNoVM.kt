/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment


/**
 * The base class of dialog fragments without [ViewModel].
 */
abstract class AbstractDialogFragmentNoVM<DB : ViewDataBinding> : DialogFragment() {

    protected lateinit var dataBinding: DB
        private set

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        dataBinding = DataBindingUtil.inflate(
            inflater, getLayout(), container, false
        )
        dataBinding.lifecycleOwner = this
        return dataBinding.root
    }

    /**
     * Retrieves the layout of this [Fragment].
     */
    abstract fun getLayout(): Int
}