/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.mock

import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.source.remote.TravelRemoteSource
import com.imjover.itinerarymaker.util.IdGenerator
import kotlinx.coroutines.delay
import java.util.*

internal class TravelRemoteSourceMock(private val notificationRemoteSource: NotificationRemoteSourceMock) :
    AbstractRemoteSyncableChildSourceMock<Travel, String>(),
    TravelRemoteSource {

    companion object {
        val TRAVEL_1_of_TRAVELER_1 = Travel(
            "1",
            UserRemoteSourceMock.TRAVELER_1.id,
            UserRemoteSourceMock.AGENT_1.id,
            "Ilocos Tour",
            Date(),
            Date(),
            null,
            Status.FOR_REVIEW,
            null,
            null,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val TRAVEL_2_of_TRAVELER_1 = Travel(
            "2",
            UserRemoteSourceMock.TRAVELER_1.id,
            UserRemoteSourceMock.AGENT_1.id,
            "Palawan Tour",
            Date(),
            Date(),
            null,
            Status.REVIEWED_OK,
            null,
            null,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val TRAVEL_3_of_TRAVELER_1 = Travel(
            "3",
            UserRemoteSourceMock.TRAVELER_1.id,
            UserRemoteSourceMock.AGENT_1.id,
            "Cebu Tour",
            Date(),
            Date(),
            null,
            Status.REVIEWED_DECLINED,
            null,
            null,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
    }

    override suspend fun update(entity: Travel) {
        super.update(entity)
        if (entity.status == Status.FOR_REVIEW) {
            // Create notification
            notificationRemoteSource.addNotification(
                "",
                entity.lastUpdatedBy,
                entity.owner,
                NotificationType.TRAVEL_REVIEW,
                entity.getUniqueId()
            )
        }
    }


    /***********************************************************************************************
     * Required methods of [AbstractRemoteSyncableChildSourceMock]
     **********************************************************************************************/

    override fun getInitialValueOfEntities(): Map<String, Travel> =
        mapOf(
            TRAVEL_1_of_TRAVELER_1.id to TRAVEL_1_of_TRAVELER_1,
            TRAVEL_2_of_TRAVELER_1.id to TRAVEL_2_of_TRAVELER_1,
            TRAVEL_3_of_TRAVELER_1.id to TRAVEL_3_of_TRAVELER_1
        )

    override fun getEntitiesToUpdateOnFirstRefresh(): Map<String, Travel> {
        val cachedTravel = cachedEntities[TRAVEL_1_of_TRAVELER_1.id]!!
        val updatedTravel = Travel(
            cachedTravel.id,
            cachedTravel.owner,
            cachedTravel.agent,
            "Refreshed Ilocos Tour",
            cachedTravel.startDate,
            cachedTravel.endDate,
            cachedTravel.remarks,
            cachedTravel.status,
            cachedTravel.statusReason,
            cachedTravel.paymentMode,
            cachedTravel.lastUpdated,
            cachedTravel.lastUpdatedBy,
            cachedTravel.syncStatus
        )
        return mapOf(
            updatedTravel.id to updatedTravel
        )
    }

    override fun generateIdFor(entity: Travel): String =
        IdGenerator.generate("TRMock", entity.lastUpdatedBy)

    override suspend fun cancel(travelId: String) {
        delay(MockConfig.DELAY_TIME)

        val cachedTravel = cachedEntities[travelId]!!
        val updatedTravel = Travel(
            cachedTravel.id,
            cachedTravel.owner,
            cachedTravel.agent,
            cachedTravel.name,
            cachedTravel.startDate,
            cachedTravel.endDate,
            cachedTravel.remarks,
            Status.CANCELLED,
            cachedTravel.statusReason,
            cachedTravel.paymentMode,
            cachedTravel.lastUpdated,
            cachedTravel.lastUpdatedBy,
            cachedTravel.syncStatus
        )
        cachedEntities[travelId] = updatedTravel
    }

    override suspend fun updatePaymentMode(travelId: String, paymentMode: PaymentMode) {
        delay(MockConfig.DELAY_TIME)

        val cachedTravel = cachedEntities[travelId]!!
        val updatedTravel = Travel(
                cachedTravel.id,
                cachedTravel.owner,
                cachedTravel.agent,
                cachedTravel.name,
                cachedTravel.startDate,
                cachedTravel.endDate,
                cachedTravel.remarks,
                Status.PAID,
                cachedTravel.statusReason,
                paymentMode,
                cachedTravel.lastUpdated,
                cachedTravel.lastUpdatedBy,
                cachedTravel.syncStatus
        )
        cachedEntities[travelId] = updatedTravel
    }
}
