/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.itinerary

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.BuildConfig
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.itinerary.ItinerariesFragment
import com.imjover.itinerarymaker.exception.RemoteAccessException
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.viewmodel.itinerary.AbstractItinerariesViewModel
import kotlinx.coroutines.launch


/**
 * The view model of [ItinerariesFragment].
 */
class ItinerariesViewModel : AbstractItinerariesViewModel() {

    val isLoading = ObservableBoolean()

    private val _travelIsCancelled: MutableLiveData<Void> = MutableLiveData()
    val travelIsCancelled: LiveData<Void> get() = _travelIsCancelled


    /**
     * Submits the travel to be reviewed.
     */
    fun submitTravel() {
        viewModelScope.launch {
            isLoading.set(true)
            _showMessage.value =
                travelManager.submitForReview(getParentUserId(), getParentTravelId())
                    .map { Consumable(it) }
                    .materialize { Consumable(it.getMessageForUI()) }
            isLoading.set(false)
        }
    }

    /**
     * Cancels the travel.
     */
    fun cancelTravel() {
        viewModelScope.launch {
            isLoading.set(true)
            val travelIsCancelled =
                try {
                    travelManager.cancel(getParentUserId(), getParentTravelId())
                    true
                } catch (e: RemoteAccessException) {
                    false
                }
            if (travelIsCancelled) {
                _travelIsCancelled.value = null
            }
            isLoading.set(false)
        }
    }


    /***********************************************************************************************
     * Required functions of [AbstractItinerariesViewModel].
     **********************************************************************************************/

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }

    override fun toString(): String =
        if (BuildConfig.DEBUG) "ItinerariesViewModel" else super.toString()
}