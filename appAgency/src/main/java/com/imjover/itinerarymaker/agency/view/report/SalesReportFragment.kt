/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.report

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import com.google.android.material.datepicker.MaterialDatePicker
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.FragmentSalesReportBinding
import com.imjover.itinerarymaker.agency.viewmodel.report.ReportsViewModel
import com.imjover.itinerarymaker.util.CsvWriterUtil
import com.imjover.itinerarymaker.util.DateFormat
import com.imjover.itinerarymaker.util.toStringFormat
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.widget.validator.FormValidator
import com.imjover.itinerarymaker.view.widget.validator.NonEmptyValidator
import java.util.*


/**
 * The screen that holds the view of sales report.
 */
class SalesReportFragment : AbstractFragment<ReportsViewModel, FragmentSalesReportBinding>() {

    private lateinit var formValidator: FormValidator
    private var startDate = Date()
    private var endDate = Date()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        dataBinding.tvTravelDate.setOnClickListener {
            val builder = MaterialDatePicker.Builder.dateRangePicker()
            builder.setSelection(Pair(startDate.time, endDate.time))
            val materialDatePicker = builder.build()
            materialDatePicker.addOnNegativeButtonClickListener { materialDatePicker.dismiss() }
            materialDatePicker.addOnPositiveButtonClickListener {
                updateTravelDates(Date(it.first!!), Date(it.second!!))
            }
            materialDatePicker.show(
                requireActivity().supportFragmentManager,
                materialDatePicker.toString()
            )
        }
        dataBinding.btnDownload.setOnClickListener {
            download()
        }

        // Setup validators
        val nonEmptyValidator = NonEmptyValidator()
        formValidator = FormValidator()
        formValidator.addValidator(dataBinding.tvTravelDate, nonEmptyValidator)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Setup observers on view model
        viewModel.showMessage.observe(viewLifecycleOwner, { consumable ->
            consumable.consume {
                showMessage(it)
            }
        })
        viewModel.showCSV.observe(viewLifecycleOwner, { consumable ->
            consumable.consume {
                val pdfViewerIntent = CsvWriterUtil.getCsvViewer(requireContext(), it)
                try {
                    startActivity(pdfViewerIntent)
                } catch (e: ActivityNotFoundException) {
                    showMessage(R.string.sales_report_fragment__view_csv__failed__message)
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.tryToCancelGeneration()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_FOR_WRITE_EXTERNAL_STORAGE -> {
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)
                ) {
                    viewModel.generateSalesReport(startDate, endDate)
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                    showMessage(R.string.error_message__permission_required__write_storage__message)
                }
                return
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Updates and shows the travel dates in the UI.
     *
     * @param startDate The start of date range.
     * @param endDate The end of date range.
     */
    private fun updateTravelDates(startDate: Date, endDate: Date) {
        this.startDate = startDate
        this.endDate = endDate
        dataBinding.tvTravelDate.text =
            getString(
                R.string.travel_form_fragment__travel_date__format,
                this.startDate.toStringFormat(DateFormat.DATE_FORMAT),
                this.endDate.toStringFormat(DateFormat.DATE_FORMAT)
            )
    }

    /**
     * Handles the saving process of the inputs.
     */
    private fun download() {
        if (!formValidator.validateAll()) {
            return
        }
        // Check if user approves permission
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED -> {
                viewModel.generateSalesReport(startDate, endDate)
            }
            shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
                // In an educational UI, explain to the user why your app requires this
                // permission for a specific feature to behave as expected. In this UI,
                // include a "cancel" or "no thanks" button that allows the user to
                // continue using your app without granting the permission.
                showMessage(R.string.error_message__permission_required__write_storage__message)
            }
            else -> {
                // Directly ask for the permission.
                requestPermissions(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_CODE_FOR_WRITE_EXTERNAL_STORAGE
                )
            }
        }
    }

    private fun showMessage(@StringRes message: Int) {
        showMessage(getString(message))
    }

    private fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_sales_report

    override fun getViewModelClass(): Class<ReportsViewModel> =
        ReportsViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }

    companion object {
        private const val REQUEST_CODE_FOR_WRITE_EXTERNAL_STORAGE = 100
    }
}