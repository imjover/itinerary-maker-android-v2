/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.mock

import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.util.IdGenerator
import com.imjover.itinerarymaker.source.remote.DestinationRemoteSource
import java.util.*

internal class DestinationRemoteSourceMock : AbstractRemoteSyncableChildSourceMock<Destination, String>(),
    DestinationRemoteSource {

    companion object {
        val DESTINATION_1_of_ITINERARY_1 = Destination(
            "1",
            ItineraryRemoteSourceMock.ITINERARY_1_of_TRAVEL_1.id,
            "Calle Crisollogo",
            Date(),
            DestinationType.DEFAULT,
            TransportationMode.BUS,
            17.569987,
            120.388340,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val DESTINATION_2_of_ITINERARY_1 = Destination(
            "2",
            ItineraryRemoteSourceMock.ITINERARY_1_of_TRAVEL_1.id,
            "Baluarte",
            Date(),
            DestinationType.DEFAULT,
            TransportationMode.TAXI,
            17.551513,
            120.377262,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val DESTINATION_3_of_ITINERARY_1 = Destination(
            "3",
            ItineraryRemoteSourceMock.ITINERARY_1_of_TRAVEL_1.id,
            "Cafe Leona",
            Date(),
            DestinationType.RESTAURANT,
            TransportationMode.BUS,
            17.573763,
            120.389279,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val DESTINATION_4_of_ITINERARY_1 = Destination(
            "4",
            ItineraryRemoteSourceMock.ITINERARY_1_of_TRAVEL_1.id,
            "Grandpa's Inn & Restaurant",
            Date(),
            DestinationType.HOTEL,
            TransportationMode.WALK,
            17.572789,
            120.390269,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
    }


    /***********************************************************************************************
     * Required methods of [AbstractRemoteSyncableChildSourceMock]
     **********************************************************************************************/

    override fun getInitialValueOfEntities(): Map<String, Destination> =
        mapOf(
            DESTINATION_1_of_ITINERARY_1.id to DESTINATION_1_of_ITINERARY_1,
            DESTINATION_2_of_ITINERARY_1.id to DESTINATION_2_of_ITINERARY_1,
            DESTINATION_3_of_ITINERARY_1.id to DESTINATION_3_of_ITINERARY_1,
            DESTINATION_4_of_ITINERARY_1.id to DESTINATION_4_of_ITINERARY_1
//            "5" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("5"),
//            "6" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("6"),
//            "7" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("7"),
//            "8" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("8"),
//            "9" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("9"),
//            "10" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("10"),
//            "11" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("11"),
//            "12" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("12"),
//            "13" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("13"),
//            "14" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("14"),
//            "15" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("15"),
//            "16" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("16"),
//            "17" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("17"),
//            "18" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("18"),
//            "19" to DESTINATION_4_of_ITINERARY_1.cloneAndSet("19"),
//            "20" to DESTINATION_3_of_ITINERARY_1.cloneAndSet("20"),
//            "21" to DESTINATION_3_of_ITINERARY_1.cloneAndSet("21"),
//            "22" to DESTINATION_3_of_ITINERARY_1.cloneAndSet("22")
        )

    override fun getEntitiesToUpdateOnFirstRefresh(): Map<String, Destination> {
        val cachedEntity = cachedEntities[DESTINATION_1_of_ITINERARY_1.id]!!
        val updatedEntity = Destination(
            cachedEntity.id,
            cachedEntity.itineraryId,
            "Calle Crisollogo (refreshed)",
            cachedEntity.timestamp,
            cachedEntity.destinationType,
            cachedEntity.transportationMode,
            cachedEntity.latitude,
            cachedEntity.longitude,
            cachedEntity.lastUpdated,
            cachedEntity.lastUpdatedBy,
            cachedEntity.syncStatus
        )
        return mapOf(
            updatedEntity.id to updatedEntity
        )
    }

    override fun generateIdFor(entity: Destination): String =
        IdGenerator.generate("DRMock", entity.lastUpdatedBy)
}

private fun Destination.cloneAndSet(newId: String): Destination =
    Destination(
        newId,
        this.itineraryId,
        this.name,
        this.timestamp,
        this.destinationType,
        this.transportationMode,
        this.latitude,
        this.longitude,
        this.lastUpdated,
        this.lastUpdatedBy,
        this.syncStatus
    )