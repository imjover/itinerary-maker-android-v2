/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.viewmodel.destination

import com.imjover.itinerarymaker.traveler.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.traveler.view.destination.DestinationsFragment
import com.imjover.itinerarymaker.viewmodel.destination.AbstractDestinationsViewModel


/**
 * The view model of [DestinationsFragment].
 */
class DestinationsViewModel : AbstractDestinationsViewModel() {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }
}