/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.mock

import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.source.type.RemoteCrudChildSource
import kotlinx.coroutines.delay

/**
 * Handles the mocking of syncable remote source.
 */
abstract class AbstractRemoteSyncableChildSourceMock<SCE : SyncableChildEntity, ParentId> :
    RemoteCrudChildSource<SCE, ParentId> {

    private var isUsedAtLeastOnce = false

    protected lateinit var cachedEntities: MutableMap<String, SCE>

    /**
     * Retrieves the initial values for [cachedEntities].
     */
    abstract fun getInitialValueOfEntities(): Map<String, SCE>

    /**
     * Retrieves the entities to be updated when first refresh is attempted.
     *
     * @return The entities to be updated in format of <id: String, SCE: Entity>
     */
    abstract fun getEntitiesToUpdateOnFirstRefresh(): Map<String, SCE>

    /**
     * Generates a unique ID for the entity.
     */
    abstract fun generateIdFor(entity: SCE): String

    /**
     * Initializes the entities.
     */
    private fun initCachedEntities() {
        if (!this::cachedEntities.isInitialized) {
            cachedEntities = getInitialValueOfEntities().toMutableMap()
        }
    }


    /***********************************************************************************************
     * Overriden methods of [RemoteCrudChildSource]
     **********************************************************************************************/

    override suspend fun getListChildrenOf(parentId: ParentId): List<SCE> {
        delay(MockConfig.DELAY_TIME)
        initCachedEntities()

        // Refresh
        if (isUsedAtLeastOnce) {
            getEntitiesToUpdateOnFirstRefresh().forEach { toUpdateEntity ->
                cachedEntities[toUpdateEntity.key] = toUpdateEntity.value
            }
        }
        isUsedAtLeastOnce = true

        // Retrieve data
        return cachedEntities.filter {
            it.value.getForeignKey() == parentId
        }.values.toList()
    }

    @Suppress("UNCHECKED_CAST")
    override suspend fun insert(entity: SCE): String {
        delay(MockConfig.DELAY_TIME)
        initCachedEntities()

        val newId = generateIdFor(entity)
        val currentEntity = cachedEntities[entity.getUniqueId()]!!
        val newEntity = currentEntity.cloneAndSetArg(newId, SyncStatus.CLEAN) as SCE
        cachedEntities.remove(currentEntity.getUniqueId())
        cachedEntities[newEntity.getUniqueId()] = newEntity
        return newEntity.getUniqueId()
    }

    override suspend fun update(entity: SCE) {
        delay(MockConfig.DELAY_TIME)
        initCachedEntities()

        cachedEntities[entity.getUniqueId()] = entity
    }

    override suspend fun delete(entity: SCE) {
        delay(MockConfig.DELAY_TIME)
        initCachedEntities()

        cachedEntities.remove(entity.getUniqueId())
    }
}