/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.mock

import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.util.IdGenerator
import com.imjover.itinerarymaker.source.remote.ExpenseRemoteSource
import java.math.BigDecimal
import java.util.*

internal class ExpenseRemoteSourceMock : AbstractRemoteSyncableChildSourceMock<Expense, String>(),
    ExpenseRemoteSource {

    companion object {
        val EXPENSE_1_of_DESTINATION_1 = Expense(
            "1",
            DestinationRemoteSourceMock.DESTINATION_1_of_ITINERARY_1.id,
            "Airfare",
            ExpenseType.TRANSPORTATION,
            BigDecimal(8500),
            BigDecimal(10000),
            null,
            false,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val EXPENSE_2_of_DESTINATION_1 = Expense(
            "2",
            DestinationRemoteSourceMock.DESTINATION_1_of_ITINERARY_1.id,
            "Tourist Visa",
            ExpenseType.VISA,
            BigDecimal(5000),
            BigDecimal(5000),
            "Valid only for 30 days. Please contact us (the agency) if you'll need to extend.",
            true,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val EXPENSE_3_of_DESTINATION_1 = Expense(
            "3",
            DestinationRemoteSourceMock.DESTINATION_1_of_ITINERARY_1.id,
            "Limousine",
            ExpenseType.TRANSPORTATION,
            BigDecimal(1500),
            BigDecimal(2000),
            null,
            false,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val EXPENSE_1_of_DESTINATION_3 = Expense(
            "4",
            DestinationRemoteSourceMock.DESTINATION_3_of_ITINERARY_1.id,
            "Gordon Ramsay's Resto",
            ExpenseType.FOOD,
            BigDecimal(2000),
            BigDecimal(3000),
            null,
            true,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val EXPENSE_1_of_DESTINATION_4 = Expense(
            "5",
            DestinationRemoteSourceMock.DESTINATION_4_of_ITINERARY_1.id,
            "Shangri-La Hotel",
            ExpenseType.ACCOMMODATION,
            BigDecimal(4000),
            BigDecimal(7000),
            null,
            false,
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
    }


    /***********************************************************************************************
     * Required methods of [AbstractRemoteSyncableChildSourceMock]
     **********************************************************************************************/

    override fun getInitialValueOfEntities(): Map<String, Expense> =
        mapOf(
            EXPENSE_1_of_DESTINATION_1.id to EXPENSE_1_of_DESTINATION_1,
            EXPENSE_2_of_DESTINATION_1.id to EXPENSE_2_of_DESTINATION_1,
            EXPENSE_3_of_DESTINATION_1.id to EXPENSE_3_of_DESTINATION_1,
            EXPENSE_1_of_DESTINATION_3.id to EXPENSE_1_of_DESTINATION_3,
            EXPENSE_1_of_DESTINATION_4.id to EXPENSE_1_of_DESTINATION_4
        )

    override fun getEntitiesToUpdateOnFirstRefresh(): Map<String, Expense> {
        val cachedDestination = cachedEntities[EXPENSE_1_of_DESTINATION_1.id]!!
        val updatedDestination = Expense(
            cachedDestination.id,
            cachedDestination.destinationId,
            "Airfare (refreshed)",
            cachedDestination.expenseType,
            cachedDestination.capital,
            cachedDestination.price,
            cachedDestination.remarks,
            cachedDestination.isDone,
            cachedDestination.lastUpdated,
            cachedDestination.lastUpdatedBy,
            cachedDestination.syncStatus
        )
        return mapOf(
            updatedDestination.id to updatedDestination
        )
    }

    override fun generateIdFor(entity: Expense): String =
        IdGenerator.generate("DRMock", entity.lastUpdatedBy)
}
