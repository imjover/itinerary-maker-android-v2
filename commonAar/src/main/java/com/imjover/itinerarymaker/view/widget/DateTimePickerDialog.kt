/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.DialogDateTimePickerBinding
import com.imjover.itinerarymaker.util.setNavigationResult
import com.imjover.itinerarymaker.view.AbstractDialogFragmentNoVM
import com.imjover.itinerarymaker.view.AbstractFragment
import java.util.*

/**
 * The screen that allow the user to pick date & time.
 */
class DateTimePickerDialog : AbstractDialogFragmentNoVM<DialogDateTimePickerBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        dataBinding.btnSet.setOnClickListener {
            val calendar: Calendar = GregorianCalendar(
                dataBinding.datePicker.year,
                dataBinding.datePicker.month,
                dataBinding.datePicker.dayOfMonth,
                dataBinding.timePicker.currentHour,
                dataBinding.timePicker.currentMinute
            )
            val timestamp = Date(calendar.timeInMillis)
            setNavigationResult<Date>(timestamp)
            dismiss()
        }

        return view
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.dialog_date_time_picker
}