/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.di

import com.imjover.itinerarymaker.repository.Repository
import com.imjover.itinerarymaker.repository.firebase.RepositoryImpl

/**
 * Holds the static instance of the [Repository] for the manager's usage.
 */
class RepositoryFactory private constructor() {

    companion object {

        private lateinit var repository: Repository

        fun singleton(): Repository {
            if (!this::repository.isInitialized) {
                repository = RepositoryImpl()
            }
            return repository
        }
    }
}