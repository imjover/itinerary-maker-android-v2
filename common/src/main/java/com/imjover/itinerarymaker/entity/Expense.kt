/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigDecimal
import java.util.*


/**
 * The Expense of a [Destination].
 *
 * @param id The unique ID and primary key.
 * @param name The label to be used for this record.
 * @param expenseType The type of expense.
 * @param capital Cost of the good or services.
 * @param price Amount to be paid by the customer.
 * @param remarks An optional remark or note.
 * @param isDone Whether this expenses is done, then true, else false.
 * @param lastUpdated The most recent date this record was updated.
 * @param lastUpdatedBy The most recent user who updated this record.
 * @param syncStatus The current synchronization state of this record.
 */
@Entity(
    tableName = "tbl_expense",
)
data class Expense(
    @PrimaryKey @ColumnInfo(name = "pk_expense_id") val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "expense_type") val expenseType: ExpenseType,
    @ColumnInfo(name = "capital") val capital: BigDecimal,
    @ColumnInfo(name = "price") val price: BigDecimal,
    @ColumnInfo(name = "remarks") val remarks: String?,
    @ColumnInfo(name = "is_done") val isDone: Boolean,
    // Audit Trail
    @ColumnInfo(name = "last_updated") val lastUpdated: Date,
    @ColumnInfo(name = "fk_last_updated_by") val lastUpdatedBy: String,
    // Syncable
    @ColumnInfo(name = "sync_status") val syncStatus: SyncStatus
) : SyncableEntity {

    override fun getUniqueId(): String =
        id

    override fun getSyncableStatus(): SyncStatus =
        syncStatus

    override fun cloneAndSetArg(newSyncStatus: SyncStatus): Expense =
        cloneAndSetArg(this.id, newSyncStatus)

    override fun cloneAndSetArg(newId: String, newSyncStatus: SyncStatus): Expense =
        Expense(
            newId,
            this.name,
            this.expenseType,
            this.capital,
            this.price,
            this.remarks,
            this.isDone,
            this.lastUpdated,
            this.lastUpdatedBy,
            newSyncStatus
        )
}