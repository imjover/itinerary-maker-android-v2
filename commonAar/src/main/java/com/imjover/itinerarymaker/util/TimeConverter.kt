/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.util

import androidx.annotation.StringRes
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.common.di.AppComponent
import java.text.SimpleDateFormat
import java.util.*


/**
 * Formats the object [Date] to a more user-friendly String.
 *
 * @return The formatted String.
 */
fun Date.toStringFormat(format: DateFormat): String {
    return SimpleDateFormat(AppComponent.appContext.getString(format.format), Locale.US).format(this)
}

/**
 * The format to be used in formatting the object [Date].
 */
enum class DateFormat(@StringRes val format: Int) {
    DATE_FORMAT(R.string.app__date_format),
    TIME_FORMAT(R.string.app__time_format),
    TIME_DATE_FORMAT(R.string.app__time_date_format)
}