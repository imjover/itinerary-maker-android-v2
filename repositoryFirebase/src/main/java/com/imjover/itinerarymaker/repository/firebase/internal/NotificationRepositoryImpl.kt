/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase.internal

import android.util.Log
import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.repository.NotificationRepository
import com.imjover.itinerarymaker.repository.firebase.BuildConfig
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.withContext


private const val NOTIFICATIONS_COLLECTION = "notifications"

internal class NotificationRepositoryImpl(
    private val dispatcher: CoroutineDispatcher,
    private val db: FirebaseFirestore
) : NotificationRepository {

    @ExperimentalCoroutinesApi
    override fun getNewNotificationCountOf(recipient: String): Flow<Int> =
        callbackFlow {
            val eventsCollection = try {
                getEventsCollection(recipient)
            } catch (e: Throwable) {
                // If Firebase cannot be initialized, close the stream of data
                // flow consumers will stop collecting and the coroutine will resume
                close(e)
                return@callbackFlow
            }

            // This implementation is not really a flow. This fetches data only once.
            // Notice here that we use #get followed by #addOnSuccessListener,
            // instead of #addSnapshotListener.
            eventsCollection
                .get()
                .addOnSuccessListener {
                    val unreadNotificationSize = it.filter { notifObj ->
                        !(notifObj["is_seen"] as Boolean)
                    }.size
                    Log.d(toString(), "Notification count $unreadNotificationSize")
                    offer(unreadNotificationSize)
                }
                .addOnFailureListener {
                    it.logException(toString())
                }

            // The callback inside awaitClose will be executed when the flow is
            // either closed or cancelled.
            // In this case, remove the callback from Firestore
            awaitClose {
                // Do nothing
            }
        }

    @ExperimentalCoroutinesApi
    override fun getNotificationsOf(recipient: String): Flow<Notification> =
        callbackFlow {
            val eventsCollection = try {
                getEventsCollection(recipient)
            } catch (e: Throwable) {
                // If Firebase cannot be initialized, close the stream of data
                // flow consumers will stop collecting and the coroutine will resume
                close(e)
                return@callbackFlow
            }

            // Registers callback to firestore, which will be called on new events
            val registration = eventsCollection
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .addSnapshotListener { querySnapshot, e ->
                    if (e != null) {
                        e.logException(toString())
                        return@addSnapshotListener
                    }
                    if (querySnapshot == null) {
                        return@addSnapshotListener
                    }
                    for (change in querySnapshot.documentChanges) {
                        offer(getEntityFrom(change, querySnapshot))
                    }
                }

            // The callback inside awaitClose will be executed when the flow is
            // either closed or cancelled.
            // In this case, remove the callback from Firestore
            awaitClose {
                registration.remove()
            }
        }

    override suspend fun syncNotificationsOf(recipient: String) {
        TODO("Not yet implemented")
    }

    override suspend fun setAsSeen(userId: String, id: String) {
        withContext(dispatcher) {
            getEventsCollection(userId)
                .document(id)
                .update("is_seen", true)
                .addOnSuccessListener {
                    Log.i(toString(), "Seen notification $id")
                }
                .addOnFailureListener {
                    it.logException(toString())
                    throw it
                }
        }
    }

    /***********************************************************************************************
     * Other functions.
     **********************************************************************************************/

    private fun getEventsCollection(userId: String): CollectionReference {
        return db.collection("users")
            .document(userId)
            .collection(NOTIFICATIONS_COLLECTION)
    }

    private fun getEntityFrom(change: DocumentChange, querySnapshot: QuerySnapshot): Notification {
        when (change.type) {
            DocumentChange.Type.REMOVED, // FIXME: This should remove the entity, instead of returning again
            DocumentChange.Type.ADDED,
            DocumentChange.Type.MODIFIED -> {
                val syncStatus =
                    if (!querySnapshot.metadata.isFromCache) {
                        SyncStatus.CLEAN
                    } else {
                        when (change.type) {
                            DocumentChange.Type.ADDED -> SyncStatus.TO_INSERT
                            DocumentChange.Type.MODIFIED -> SyncStatus.TO_UPDATE
                            DocumentChange.Type.REMOVED -> SyncStatus.TO_DELETE
                        }
                    }
                return change.document.data.toEntity(
                    change.document.id,
                    syncStatus
                )
            }
        }
    }

    private fun MutableMap<String, Any?>.toEntity(
        id: String,
        syncStatus: SyncStatus
    ): Notification =
        Notification(
            id,
            this["message"] as String,
            this["fk_submitted_by"] as String,
            NotificationType.fromId(this["notification_type"] as String),
            this["transaction_id"] as String,
            (this["timestamp"] as Timestamp).toDate(),
            this["is_seen"] as Boolean,
            syncStatus
        )

    override fun toString(): String =
        if (BuildConfig.DEBUG) "NotificationRepositoryImpl" else super.toString()
}