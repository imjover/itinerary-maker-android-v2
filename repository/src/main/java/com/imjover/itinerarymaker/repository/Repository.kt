/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository

/**
 * Represents the repository which has access to both local and remote data sources.
 */
interface Repository {

    fun getUserRepository(): UserRepository

    fun getTravelRepository(): TravelRepository

    fun getItineraryRepository(): ItineraryRepository

    fun getDestinationRepository(): DestinationRepository

    fun getExpenseRepository(): ExpenseRepository

    fun getNotificationRepository(): NotificationRepository

    fun getReportRepository(): ReportRepository
}