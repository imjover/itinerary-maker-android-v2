/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase.internal

import com.google.firebase.Timestamp
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.entity.ExpenseType
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.repository.ExpenseRepository
import kotlinx.coroutines.CoroutineDispatcher
import java.util.*


private const val EXPENSES_COLLECTION = "expenses"

internal class ExpenseRepositoryImpl(
    dispatcher: CoroutineDispatcher,
    private val db: FirebaseFirestore
) : AbstractSyncableCrudRepository<Expense>(dispatcher), ExpenseRepository {


    /***********************************************************************************************
     * Abstract functions from AbstractSyncableCrudRepository.
     **********************************************************************************************/

    override fun getEventsCollection(parentIds: Array<String>): CollectionReference {
        if (parentIds.size != 4) {
            throw IllegalArgumentException()
        }
        return db.collection(USERS_COLLECTION)
            .document(parentIds[0])
            .collection(TRAVELS_COLLECTION)
            .document(parentIds[1])
            .collection(ITINERARIES_COLLECTION)
            .document(parentIds[2])
            .collection(DESTINATIONS_COLLECTION)
            .document(parentIds[3])
            .collection(EXPENSES_COLLECTION)
    }

    override fun getOrderBy(): Pair<String, Query.Direction> =
        Pair("name", Query.Direction.ASCENDING)

    override fun MutableMap<String, Any?>.toEntity(id: String, syncStatus: SyncStatus): Expense =
        Expense(
            id,
            this["name"] as String,
            ExpenseType.fromId(this["expense_type"].toString()),
            (this["capital"] as Double).toBigDecimal(),
            (this["price"] as Double).toBigDecimal(),
            this["remarks"] as String?,
            this["is_done"] as Boolean,
            (this["last_updated"] as Timestamp).toDate(),
            this["fk_last_updated_by"] as String,
            syncStatus
        )

    override fun Expense.toHashMap(): HashMap<String, Any?> {
        val map = mutableMapOf(
            "name" to name,
            "expense_type" to expenseType.id,
            "capital" to capital.toDouble(),
            "price" to price.toDouble(),
            "is_done" to isDone,
            "last_updated" to lastUpdated,
            "fk_last_updated_by" to lastUpdatedBy
        )
        remarks?.let { map.put("remarks", it) }
        return HashMap(map)
    }
}