/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.RowNotificationBinding
import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.util.DateFormat
import com.imjover.itinerarymaker.util.toStringFormat
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter
import com.imjover.itinerarymaker.viewmodel.notification.AbstractNotificationsViewModel

/**
 * List adapter of [Notification] item.
 */
class NotificationAdapter(private val notificationViewModel: AbstractNotificationsViewModel) :
    UpdatableRecyclerViewAdapter<Notification, RowNotificationBinding>() {

    private lateinit var context: Context


    /***********************************************************************************************
     * Overriden functions.
     **********************************************************************************************/

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterViewHolder<RowNotificationBinding> {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = RowNotificationBinding.inflate(layoutInflater, parent, false)
        return AdapterViewHolder(itemBinding)
    }

    override fun onBindViewHolder(
        holder: AdapterViewHolder<RowNotificationBinding>,
        position: Int
    ) {
        val notification = getData(position)
        val showDivider = position + 1 < itemCount
        // Bind instances of [Notification] to the row.
        holder.binding.notification = notification
        holder.binding.container.setOnClickListener {
            notificationViewModel.setAsSeen(notification)
        }
        holder.binding.tvTimestamp.text =
            context.getString(
                R.string.notification_fragment__timestamp__format,
                notification.timestamp.toStringFormat(DateFormat.DATE_FORMAT),
                notification.timestamp.toStringFormat(DateFormat.TIME_FORMAT)
            )
        holder.binding.divider.visibility = if (showDivider) View.VISIBLE else View.GONE

        // Update styles
        val style = if (notification.isSeen) R.style.TextStyle_Body1_Secondary else R.style.TextStyle_Body1
        holder.binding.tvMessage.setTextAppearance(context, style)
        holder.binding.tvTimestamp.setTextAppearance(context, style)

        holder.binding.executePendingBindings()
    }

    override fun getOnClickListener(id: String): View.OnClickListener? = null
}