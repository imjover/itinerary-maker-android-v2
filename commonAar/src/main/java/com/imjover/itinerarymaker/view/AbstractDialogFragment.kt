/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel


/**
 * The base class of dialog fragments.
 */
abstract class AbstractDialogFragment<VM : AbstractViewModel, DB : ViewDataBinding> :
    DialogFragment(), DataBindable<VM> {

    protected lateinit var viewModel: VM
        private set
    protected lateinit var dataBinding: DB
        private set

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel = ViewModelProvider(this).get(getViewModelClass())
        dataBinding = DataBindingUtil.inflate(
            inflater, getLayout(), container, false
        )
        dataBinding.lifecycleOwner = this
        bindLayoutAndViewModel()
        return dataBinding.root
    }
}