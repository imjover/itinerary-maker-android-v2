/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget.validator

import android.view.View
import android.widget.TextView
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.common.di.AppComponent

/**
 * Validates a [View] and ensures that its input's length is more than or equal the [min] length.
 *
 * @param min The minimum required length.
 */
class MinimumLengthValidator(private val min: Int) : Validator {

    override fun isValid(view: View): Boolean {
        when (view) {
            is TextView -> {
                val length = view.text.toString().trim().length
                return length >= min
            }
        }
        throw ViewNotSupportedByValidator(view, this)
    }

    override fun getErrorMsg(): String =
        AppComponent.appContext.getString(R.string.form_validation__minimum_length_message, min)
}