/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler

import com.imjover.itinerarymaker.AbstractBaseApplication
import com.imjover.itinerarymaker.common.di.ApplicationType

/**
 * The application object.
 */
class TravelerApplication : AbstractBaseApplication(ApplicationType.AppTraveler, BuildConfig.VERSION_NAME)