/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.source.local.room.dao.*
import com.imjover.itinerarymaker.source.local.room.dao.DestinationDao
import com.imjover.itinerarymaker.source.local.room.dao.ExpenseDao
import com.imjover.itinerarymaker.source.local.room.dao.ItineraryDao
import com.imjover.itinerarymaker.source.local.room.dao.TravelDao
import com.imjover.itinerarymaker.source.local.room.dao.UserDao

/**
 * The database implementation.
 *
 * If your app runs in a single process, you should follow the singleton design pattern when
 * instantiating an AppDatabase object. Each RoomDatabase instance is fairly expensive, and you
 * rarely need access to multiple instances within a single process.
 *
 * If your app runs in multiple processes, include enableMultiInstanceInvalidation() in your
 * database builder invocation. That way, when you have an instance of AppDatabase in each process,
 * you can invalidate the shared database file in one process, and this invalidation automatically
 * propagates to the instances of AppDatabase within other processes.
 *
 * @see [Save data in a local database using Room](https://developer.android.com/training/data-storage/room#kotlin)
 */
@Database(
    entities = [
        User::class,
        Travel::class,
        Itinerary::class,
        Destination::class,
        Expense::class,
        Invoice::class,
        Notification::class
    ],
    version = 1
)
@TypeConverters(Converters::class)
internal abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun travelDao(): TravelDao

    abstract fun itineraryDao(): ItineraryDao

    abstract fun destinationDao(): DestinationDao

    abstract fun expenseDao(): ExpenseDao

    abstract fun invoiceDao(): InvoiceDao

    abstract fun notificationDao(): NotificationDao
}