/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.DialogRegistrationFormBinding
import com.imjover.itinerarymaker.agency.viewmodel.login.RegistrationFormViewModel
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.helper.toImageRes
import com.imjover.itinerarymaker.helper.toStringRes
import com.imjover.itinerarymaker.view.AbstractDialogFragment
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.widget.IconTextSpinnerAdapter


private val USER_TYPES = arrayOf(UserType.AGENT, UserType.ACCOUNTANT).toSpinnerItems()

/**
 * The screen that holds the form to register the user.
 */
class RegistrationFormDialog :
    AbstractDialogFragment<RegistrationFormViewModel, DialogRegistrationFormBinding>() {

    private lateinit var loginActivity: LoginActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (activity !is LoginActivity) {
            throw IllegalStateException()
        } else {
            loginActivity = activity as LoginActivity
        }

        // Setup the UI
        isCancelable = false
        dataBinding.spUserType.adapter = IconTextSpinnerAdapter(this.requireContext(), USER_TYPES)
        dataBinding.btnRegister.setOnClickListener {
            register()
        }
        dataBinding.btnCancel.setOnClickListener {
            showMessage(getString(R.string.login_activity__registration_cancelled__message))
            dismiss()
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup observers on view model
        viewModel.message.observe(this, this::showMessage)
        viewModel.dismiss.observe(this, { dismiss() })
    }

    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Shows the message in the LoginActivity.
     */
    private fun showMessage(message: String) {
        loginActivity.showMessage(message)
    }

    /**
     * Handles the registration process.
     */
    private fun register() {
        val userType = UserType.fromId(dataBinding.spUserType.selectedItem.toString().toInt())
        viewModel.register(userType)
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.dialog_registration_form

    override fun getViewModelClass(): Class<RegistrationFormViewModel> =
        RegistrationFormViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}


/**
 * Converts an array of [UserType] to list of [IconTextSpinnerAdapter.SpinnerItem] to be used
 * for as adapter for a [Spinner].
 *
 * @return [Spinner] adapter items.
 */
private fun Array<UserType>.toSpinnerItems(): List<IconTextSpinnerAdapter.SpinnerItem> {
    val items = mutableListOf<IconTextSpinnerAdapter.SpinnerItem>()
    this.forEach {
        items.add(
            IconTextSpinnerAdapter.SpinnerItem(
                it.id.toString(),
                AppComponent.appContext.getString(it.toStringRes()),
                it.toImageRes()
            )
        )
    }
    return items
}