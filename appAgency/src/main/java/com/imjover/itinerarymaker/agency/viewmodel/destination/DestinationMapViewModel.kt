/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.destination

import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.destination.DestinationMapFragment
import com.imjover.itinerarymaker.viewmodel.destination.AbstractDestinationMapViewModel

/**
 * The view model of [DestinationMapFragment].
 */
class DestinationMapViewModel : AbstractDestinationMapViewModel() {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }
}