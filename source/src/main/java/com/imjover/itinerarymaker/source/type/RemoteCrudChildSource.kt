/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.type

import com.imjover.itinerarymaker.entity.Entity

/**
 * Defines remote sources that has CRUD, and is a child.
 */
interface RemoteCrudChildSource<E : Entity, ParentId> :
    RemoteCrudSource<E>,
    ChildSource<E, ParentId>