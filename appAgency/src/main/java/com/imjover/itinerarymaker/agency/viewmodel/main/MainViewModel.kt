/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.main

import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.main.MainActivity
import com.imjover.itinerarymaker.viewmodel.main.AbstractMainViewModel


/**
 * The view model of [MainActivity].
 */
class MainViewModel : AbstractMainViewModel() {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }
}