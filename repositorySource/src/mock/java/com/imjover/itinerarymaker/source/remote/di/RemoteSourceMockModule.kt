/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.di

import com.imjover.itinerarymaker.source.RemoteSource
import com.imjover.itinerarymaker.source.remote.mock.RemoteSourceMock
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the remote implementation of [RemoteSource] that uses [Mock].
 */
@Module
abstract class RemoteSourceMockModule {

    @Singleton
    @Binds
    internal abstract fun bindRemoteSource(remoteSource: RemoteSourceMock): RemoteSource
}