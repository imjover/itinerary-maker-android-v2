/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.entity.SyncableEntity
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.manager.SyncableManager
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

/**
 * Defines the view model that can be synced in both remote and local sources.
 */
abstract class AbstractSyncableViewModel<SE : SyncableEntity> : AbstractViewModel() {

    private val _isRefreshing: MutableLiveData<Boolean> = MutableLiveData()
    val isRefreshing: LiveData<Boolean>
        get() = _isRefreshing

    abstract fun getParentIds(): Array<String>

    abstract fun getSyncableManager(): SyncableManager<SE>

    /**
     * Retrieves the [Travel]s of a given [User].
     *
     * @return Stream of [Travel]s.
     */
    fun getSyncableItems(): Flow<SE> =
        getSyncableManager().getStreamChildrenOf(getParentIds())

    /**
     * Refresh the stream of travels.
     */
    @Deprecated("Syncing is automatically handled by Firebase")
    fun refresh() {
        viewModelScope.launch {
            if (isRefreshing.value != null && isRefreshing.value!!) {
                // Refreshing is ongoing, skip the rest of the steps.
                return@launch
            }
            _isRefreshing.value = true
            getSyncableManager().syncChildrenOf(getParentIds())
            _isRefreshing.value = false
        }
    }
}