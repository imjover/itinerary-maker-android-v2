/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.common.di

import android.app.Application
import android.content.Context

/**
 * Holds the component objects in application-level.
 */
class AppComponent private constructor() {

    companion object {
        private lateinit var _application: Application
        val appContext: Context
            get() = _application

        private lateinit var _appType: ApplicationType
        val appType: ApplicationType
            get() = _appType

        private lateinit var _appVersion: String
        val appVersion: String
            get() = _appVersion

        /**
         * Initializes the objects of [AppComponent].
         *
         * @param application This [Application].
         * @param appType The type of this application.
         * @param appVersion The version of this application.
         */
        fun init(application: Application, appType: ApplicationType, appVersion: String) {
            _application = application
            _appType = appType
            _appVersion = appVersion
        }
    }
}