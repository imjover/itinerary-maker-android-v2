/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.internal

import androidx.room.RoomDatabase
import com.imjover.itinerarymaker.source.LocalSource
import com.imjover.itinerarymaker.source.local.*
import javax.inject.Inject

/**
 * Local implementation of [LocalSource] that uses [RoomDatabase].
 */
internal class LocalSourceRoom @Inject constructor() : LocalSource {

    override fun getUserLocalSource(): UserLocalSource =
        UserLocalSourceImpl()

    override fun getTravelLocalSource(): TravelLocalSource =
        TravelLocalSourceImpl()

    override fun getItineraryLocalSource(): ItineraryLocalSource =
        ItineraryLocalSourceImpl()

    override fun getDestinationLocalSource(): DestinationLocalSource =
        DestinationLocalSourceImpl()

    override fun getExpenseLocalSource(): ExpenseLocalSource =
        ExpenseLocalSourceImpl()

    override fun getNotificationLocalSource(): NotificationLocalSource =
        NotificationLocalSourceImpl()
}