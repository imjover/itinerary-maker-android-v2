/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * The mode of transportation used to travel.
 *
 * @param id The unique ID.
 */
enum class TransportationMode(val id: String) {

    WALK("walk"),
    CAR("car"),
    BUS("bus"),
    TAXI("taxi"),
    TRAIN("train"),
    AIRPLANE("airplane"),
    BOAT("boat"),
    TRAM("tram");


    companion object {
        /**
         * Retrieves the equivalent [TransportationMode] base on the given ID.
         *
         * @param id The id of the [TransportationMode].
         * @return The retrieved [TransportationMode].
         */
        fun fromId(id: String): TransportationMode {
            values().forEach {
                if (it.id == id) {
                    return it
                }
            }
            throw IllegalStateException()
        }
    }
}