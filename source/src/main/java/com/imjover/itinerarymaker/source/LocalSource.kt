/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source

import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.source.local.*

/**
 * Represents the local data source.
 */
interface LocalSource {

    /**
     * Retrieves the local source of [User].
     */
    fun getUserLocalSource(): UserLocalSource

    /**
     * Retrieves the local source of [Travel].
     */
    fun getTravelLocalSource(): TravelLocalSource

    /**
     * Retrieves the local source of [Itinerary].
     */
    fun getItineraryLocalSource(): ItineraryLocalSource

    /**
     * Retrieves the local source of [Destination].
     */
    fun getDestinationLocalSource(): DestinationLocalSource

    /**
     * Retrieves the local source of [Expense].
     */
    fun getExpenseLocalSource(): ExpenseLocalSource

    /**
     * Retrieves the local source of [Notification].
     */
    fun getNotificationLocalSource(): NotificationLocalSource
}