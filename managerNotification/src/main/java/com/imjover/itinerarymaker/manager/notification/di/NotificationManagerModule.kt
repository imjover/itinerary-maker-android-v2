/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.notification.di

import com.imjover.itinerarymaker.manager.NotificationManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.manager.notification.internal.NotificationManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the [NotificationManager].
 */
@Module
abstract class NotificationManagerModule(userManager: UserManager) {

    @Singleton
    @Binds
    internal abstract fun bindNotificationManager(notificationManager: NotificationManagerImpl): NotificationManager
}