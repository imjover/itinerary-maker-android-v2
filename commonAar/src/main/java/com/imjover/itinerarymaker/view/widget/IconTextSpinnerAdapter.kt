/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import com.imjover.itinerarymaker.R
import kotlinx.android.synthetic.main.dropdown_item_icon_text.view.*
import kotlinx.android.synthetic.main.dropdown_row_icon_text.view.*
import java.lang.IllegalArgumentException


/**
 * Custom [SpinnerAdapter] to support row items with icon and text.
 *
 * @param mContext The view context.
 * @param spinnerItems The row items supplied to the Spinner.
 */
class IconTextSpinnerAdapter(
    private val mContext: Context,
    private val spinnerItems: List<SpinnerItem>
) : ArrayAdapter<String>(mContext, R.layout.dropdown_view_icon_text, R.id.tv_label),
    SpinnerAdapter {

    override fun getItem(position: Int): String =
        spinnerItems[position].id

    override fun getItemId(position: Int): Long =
        position.toLong()

    override fun getCount(): Int =
        spinnerItems.size

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View =
        getCustomView(position, parent, R.layout.dropdown_view_icon_text)

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View =
        getCustomView(position, parent, R.layout.dropdown_row_icon_text)

    private fun getCustomView(position: Int, parent: ViewGroup, @LayoutRes layout: Int): View {
        val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val row: View = inflater.inflate(layout, parent, false)
        val spinnerItem = spinnerItems[position]
        row.dropdown_container.iv_icon.setImageResource(spinnerItem.icon)
        row.dropdown_container.tv_label.text = spinnerItem.label
        return row
    }

    /**
     * Holds the label and icons of the spinner's row item.
     */
    data class SpinnerItem(
        val id: String,
        val label: String,
        @DrawableRes val icon: Int
    )
}

/**
 * Retrieve the position of an ID from the list of [IconTextSpinnerAdapter.SpinnerItem].
 *
 * @return The position.
 * @throws IllegalArgumentException When position is invalid and not found.
 */
fun List<IconTextSpinnerAdapter.SpinnerItem>.getPositionOf(id: String): Int {
    for (i in 0 until size) {
        if (id == this[i].id) {
            return i
        }
    }
    throw IllegalArgumentException()
}