/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local

import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.source.type.LocalSyncableCrudChildSource
import kotlinx.coroutines.flow.Flow

/**
 * Represents the local source of [Notification].
 */
interface NotificationLocalSource :
    LocalSyncableCrudChildSource<Notification, String> {

    /**
     * Retrieves the total count of new notifications.
     * New notifications are those that [Notification.isSeen] is false.
     *
     * @param recipient The [User] who receives the notification.
     * @return Stream of total count.
     */
    fun getNewNotificationCountOf(recipient: String): Flow<Int>

    /**
     * Update notifications as seen.
     *
     * @param id The unique ID of the notification.
     * @param syncStatus The synchronization status.
     */
    suspend fun setAsSeen(id: String, syncStatus: SyncStatus)
}