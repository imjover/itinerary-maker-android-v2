/*
 * Copyright (c) 2021. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager

import com.imjover.itinerarymaker.entity.ExpenseType
import com.imjover.itinerarymaker.util.Result
import java.math.BigDecimal
import java.util.*


/**
 * Manages the reports of the application.
 */
interface ReportManager {

    /**
     * Retrieves the sales report.
     *
     * @param startDate Start of date range.
     * @param endDate End of date range.
     */
    suspend fun getSalesReport(
        startDate: Date,
        endDate: Date
    ): Result<Map<ExpenseType, ArrayList<BigDecimal>>>
}