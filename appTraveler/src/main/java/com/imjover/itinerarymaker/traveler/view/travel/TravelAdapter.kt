/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.travel

import android.view.View
import androidx.navigation.findNavController
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.view.travel.AbstractTravelAdapter


/**
 * List adapter of [Travel] item.
 */
class TravelAdapter(private val parentIds: Array<String>) : AbstractTravelAdapter() {

    override fun isUserAllowedToEdit(): Boolean =
        false // Editing is only allowed in Agency's app

    override fun getOnClickListener(id: String, isEditable: Boolean): View.OnClickListener? =
        // isEditable field here IS not used, because it is only needed in Agency's app
        View.OnClickListener {
            val action = TravelsFragmentDirections.actionTravelsFragmentToItinerariesFragment(
                parentIds.plus(id)
            )
            it.findNavController().navigate(action)
        }

    override fun getOnClickListener(id: String): View.OnClickListener? =
        throw IllegalStateException()
}