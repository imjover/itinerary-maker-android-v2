/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.notification

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.FragmentNotificationsBinding
import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.view.AbstractRefreshableFragment
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter
import com.imjover.itinerarymaker.viewmodel.notification.AbstractNotificationsViewModel


/**
 * The screen that holds all notifications.
 */
abstract class AbstractNotificationsFragment :
    AbstractRefreshableFragment<Notification, AbstractNotificationsViewModel, FragmentNotificationsBinding>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.openNextScreen.observe(viewLifecycleOwner, Observer {
            if (it == null) {
                return@Observer
            }
            it.consume { notification -> handleNotification(notification) }
            viewModel.clearLiveData()
        })
    }

    /**
     * Handles the actions depending on the notification received.
     */
    abstract fun handleNotification(notification: Notification)


    /***********************************************************************************************
     * Required functions of [AbstractRefreshableFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_notifications

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }

    override fun getRecyclerView(): RecyclerView =
        dataBinding.rvNotificationList

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout =
        dataBinding.swipeContainer

    override fun getAdapter(): UpdatableRecyclerViewAdapter<Notification, *> =
        NotificationAdapter(viewModel)
}