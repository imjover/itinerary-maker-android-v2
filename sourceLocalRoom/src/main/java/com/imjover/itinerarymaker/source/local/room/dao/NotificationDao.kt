/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.SyncStatus
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object of the [Notification].
 */
@Dao
internal interface NotificationDao : CrudDao<Notification> {

    /**
     * Retrieve all [Notification]s of [submittedTo].
     *
     * @param submittedTo The recipient of the notification.
     * @return A stream [Notification]s.
     */
    // FIXME: Return Flow<Notification> instead of Flow<List<Notification>>
    @Query("SELECT * FROM tbl_notification WHERE fk_submitted_to = :submittedTo")
    fun getStreamOfNotifications(submittedTo: String): Flow<List<Notification>>

    /**
     * Retrieve all [Notification]s of [submittedTo].
     *
     * @param submittedTo The recipient of the notification.
     * @return A list of [Notification]s.
     */
    @Query("SELECT * FROM tbl_notification WHERE fk_submitted_to = :submittedTo")
    suspend fun getNotificationsOf(submittedTo: String): List<Notification>

    /**
     * Retrieves all [Notification]s that has [syncStatus] and owned by [submittedTo].
     *
     * @param submittedTo The recipient of the notification.
     * @param syncStatus The synchronization status.
     * @return A list of [Notification]s.
     */
    @Query("SELECT * FROM tbl_notification WHERE fk_submitted_to = :submittedTo AND sync_status = :syncStatus")
    suspend fun getNotificationsOf(submittedTo: String, syncStatus: SyncStatus): List<Notification>

    /**
     * Retrieve a [Notification] from the given ID.
     *
     * @param submittedTo The recipient of the notification.
     */
    @Query("SELECT COUNT(*) FROM tbl_notification WHERE is_seen = 0 AND fk_submitted_to = :submittedTo")
    fun getNewNotificationCountOf(submittedTo: String): Flow<Int>

    /**
     * Updates the [isSeen] of the record with [notificationId].
     *
     * @param notificationId The ID of this record.
     * @param syncStatus The synchronization status.
     */
    @Query("UPDATE tbl_notification SET is_seen = 1, sync_status = :syncStatus  WHERE pk_notification_id = :notificationId")
    suspend fun setAsSeen(notificationId: String, syncStatus: SyncStatus)
}