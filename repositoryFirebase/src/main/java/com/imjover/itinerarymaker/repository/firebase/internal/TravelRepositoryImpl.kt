/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase.internal

import android.util.Log
import com.google.firebase.Timestamp
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.Source
import com.google.firebase.functions.FirebaseFunctions
import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.entity.Status
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.repository.TravelRepository
import com.imjover.itinerarymaker.repository.firebase.BuildConfig
import com.imjover.itinerarymaker.util.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


internal const val TRAVELS_COLLECTION = "travels"

internal class TravelRepositoryImpl(
    private val dispatcher: CoroutineDispatcher,
    private val functions: FirebaseFunctions,
    private val db: FirebaseFirestore
) : AbstractSyncableCrudRepository<Travel>(dispatcher), TravelRepository {

    override suspend fun getCachedStatus(parentIds: Array<String>, travelId: String): Status =
        withContext(dispatcher) {
            suspendCoroutine { cont ->
                getEventsCollection(parentIds)
                    .document(travelId)
                    .get(Source.CACHE)
                    .addOnSuccessListener {
                        cont.resume(Status.fromId(it.data?.get("status").toString().toInt()))
                    }
                    .addOnFailureListener {
                        it.logException(toString())
                        cont.resumeWithException(it)
                    }
            }
        }

    override suspend fun cancel(parentIds: Array<String>, travelId: String) {
        withContext(dispatcher) {
            getEventsCollection(parentIds)
                .document(travelId)
                .update("status", Status.CANCELLED.id)
                .addOnSuccessListener {
                    Log.i(toString(), "Cancelled $travelId")
                }
                .addOnFailureListener {
                    it.logException(toString())
                    throw it
                }
        }
    }

    override suspend fun markAsPaid(
        parentIds: Array<String>,
        travelId: String,
        paymentMode: PaymentMode,
        lastUpdatedBy: String,
        lastUpdated: Date
    ) {
        withContext(dispatcher) {
            val fields = mapOf(
                "status" to Status.PAID.id,
                "payment_mode" to paymentMode.id,
                "payment_date" to Date(),
                "fk_last_updated_by" to lastUpdatedBy,
                "last_updated" to lastUpdated
            )
            getEventsCollection(parentIds)
                .document(travelId)
                .update(fields)
                .addOnSuccessListener {
                    Log.i(toString(), "Updated payment mode of $travelId")
                }
                .addOnFailureListener {
                    it.logException(toString())
                    throw it
                }
        }
    }

    override suspend fun submitForReview(
        parentIds: Array<String>,
        travelId: String
    ): Result<String> {
        val data: MutableMap<String, Any> = HashMap()
        data["ownerId"] = parentIds.first()
        data["travelId"] = travelId

        return suspendCoroutine { cont ->
            functions
                .getHttpsCallable("travels-submitForReview")
                .call(data)
                .continueWith { task ->
                    task.result?.data as String
                }
                .addOnSuccessListener {
                    cont.resume(Result.success(it))
                }
                .addOnFailureListener {
                    it.logException(toString())
                    cont.resume(Result.failure(it))
                }
        }
    }

    override suspend fun submitReview(
        parentIds: Array<String>,
        travelId: String,
        isApprove: Boolean,
        reason: String?
    ): Result<String> {
        val data: MutableMap<String, Any?> = HashMap()
        data["ownerId"] = parentIds.first()
        data["travelId"] = travelId
        data["isApprove"] = isApprove
        data["reason"] = reason

        return suspendCoroutine { cont ->
            functions
                .getHttpsCallable("travels-submitReview")
                .call(data)
                .continueWith { task ->
                    task.result?.data as String
                }
                .addOnSuccessListener {
                    cont.resume(Result.success(it))
                }
                .addOnFailureListener {
                    it.logException(toString())
                    cont.resume(Result.failure(it))
                }
        }
    }


    /***********************************************************************************************
     * Abstract functions from AbstractSyncableCrudRepository.
     **********************************************************************************************/

    override fun getEventsCollection(parentIds: Array<String>): CollectionReference {
        if (parentIds.size != 1) {
            throw IllegalArgumentException()
        }
        return db.collection(USERS_COLLECTION)
            .document(parentIds[0])
            .collection(TRAVELS_COLLECTION)
    }

    override fun getOrderBy(): Pair<String, Query.Direction> =
        Pair("start_date", Query.Direction.DESCENDING)

    override fun MutableMap<String, Any?>.toEntity(id: String, syncStatus: SyncStatus): Travel =
        Travel(
            id,
            this["fk_agent"] as String,
            this["name"] as String,
            (this["start_date"] as Timestamp).toDate(),
            (this["end_date"] as Timestamp).toDate(),
            this["remarks"] as String?,
            Status.fromId(this["status"].toString().toInt()),
            this["status_reason"] as String?,
            if (this["payment_mode"] == null) null else
                PaymentMode.fromId(this["payment_mode"] as String),
            (this["last_updated"] as Timestamp).toDate(),
            this["fk_last_updated_by"] as String,
            syncStatus
        )

    override fun Travel.toHashMap(): HashMap<String, Any?> {
        val map = mutableMapOf(
            "fk_agent" to agent,
            "name" to name,
            "start_date" to startDate,
            "end_date" to endDate,
            "status" to status.id,
            "last_updated" to lastUpdated,
            "fk_last_updated_by" to lastUpdatedBy
        )
        paymentMode?.let { map.put("payment_mode", it.id) }
        statusReason?.let { map.put("status_reason", it) }
        remarks?.let { map.put("remarks", it) }
        return HashMap(map)
    }


    /***********************************************************************************************
     * Other functions.
     **********************************************************************************************/

    override fun toString(): String =
        if (BuildConfig.DEBUG) "TravelRepositoryImpl" else super.toString()
}