/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.viewmodel.itinerary

import com.imjover.itinerarymaker.traveler.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.traveler.view.itinerary.ItinerariesFragment
import com.imjover.itinerarymaker.viewmodel.itinerary.AbstractItinerariesViewModel


/**
 * The view model of [ItinerariesFragment].
 */
class ItinerariesViewModel : AbstractItinerariesViewModel() {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }
}