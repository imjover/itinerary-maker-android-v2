/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * Defines that an object is an entity.
 */
interface Entity {

    /**
     * Retrieves the unique ID of this entity.
     *
     * Note: This is intentionally named `getUniqueId` (instead of `getId`) because it
     * clashes with the implementing object's getter of [id] (named also `getSyncStatus`).
     *
     * @return The unique ID.
     */
    fun getUniqueId(): String
}