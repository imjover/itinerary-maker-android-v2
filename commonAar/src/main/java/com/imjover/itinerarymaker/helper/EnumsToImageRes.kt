/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.helper

import androidx.annotation.DrawableRes
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.entity.*

/**
 * Retrieves the equivalent image resource of the [DestinationType].
 */
@DrawableRes
fun DestinationType.toImageRes(): Int =
    when (this) {
        DestinationType.DEFAULT -> R.drawable.ic_place_24
        DestinationType.HOTEL -> R.drawable.ic_hotel_24
        DestinationType.AIRPORT -> R.drawable.ic_airplane_24
        DestinationType.RESTAURANT -> R.drawable.ic_restaurant_24
        DestinationType.CAFE -> R.drawable.ic_cafe_24
        DestinationType.BAR -> R.drawable.ic_bar_24
        DestinationType.MALL -> R.drawable.ic_mall_24
        DestinationType.MUSEUM -> R.drawable.ic_museum_24
        else -> throw IllegalStateException()
    }

/**
 * Retrieves the equivalent image resource of the [TransportationMode].
 */
@DrawableRes
fun TransportationMode.toImageRes(): Int =
    when (this) {
        TransportationMode.WALK -> R.drawable.ic_walk_24
        TransportationMode.CAR -> R.drawable.ic_car_24
        TransportationMode.BUS -> R.drawable.ic_bus_24
        TransportationMode.TAXI -> R.drawable.ic_taxi_24
        TransportationMode.TRAIN -> R.drawable.ic_train_24
        TransportationMode.AIRPLANE -> R.drawable.ic_airplane_24
        TransportationMode.BOAT -> R.drawable.ic_boat_24
        TransportationMode.TRAM -> R.drawable.ic_tram_24
        else -> throw IllegalStateException()
    }

/**
 * Retrieves the equivalent image resource of the [ExpenseType].
 */
@DrawableRes
fun ExpenseType.toImageRes(): Int =
    when (this) {
        ExpenseType.ACCOMMODATION -> R.drawable.ic_hotel_24
        ExpenseType.TRANSPORTATION -> R.drawable.ic_car_24
        ExpenseType.FOOD -> R.drawable.ic_restaurant_24
        ExpenseType.VISA -> R.drawable.ic_airplane_24
        ExpenseType.TOUR -> R.drawable.ic_tour_24
        ExpenseType.OTHERS -> R.drawable.ic_receipt_24
        else -> throw IllegalStateException()
    }

/**
 * Retrieves the equivalent image resource of the [PaymentMode].
 */
@DrawableRes
fun PaymentMode.toImageRes(): Int =
    when (this) {
        PaymentMode.CASH -> R.drawable.ic_money_24
        PaymentMode.BANK_TRANSFER -> R.drawable.ic_bank_24
        PaymentMode.CREDIT_CARD -> R.drawable.ic_credit_card_24
        else -> throw IllegalStateException()
    }

/**
 * Retrieves the equivalent image resource of the [UserType].
 */
@DrawableRes
fun UserType.toImageRes(): Int =
    when (this) {
        UserType.ADMIN -> R.drawable.ic_user_24
        UserType.TRAVELER -> R.drawable.ic_user_24
        UserType.AGENT -> R.drawable.ic_user_24
        UserType.ACCOUNTANT -> R.drawable.ic_user_24
        else -> throw IllegalStateException()
    }