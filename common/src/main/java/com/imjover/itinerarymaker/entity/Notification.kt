/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

import androidx.room.*
import androidx.room.Entity
import java.util.*


/**
 * The notifications for this app.
 *
 * @param id The unique ID and primary key.
 * @param message The message of this notification.
 * @param submittedBy The sender of this notification.
 * @param notificationType The type of this notification.
 * @param transactionId The unique ID of the transaction that this notification holds.
 * @param timestamp The timestamp of this notification.
 * @param isSeen Whether the user has seen this record, then true, else false.
 */
@Entity(
    tableName = "tbl_notification",
    indices = [
        Index("fk_submitted_by")
    ]
)
data class Notification(
    @PrimaryKey @ColumnInfo(name = "pk_notification_id") val id: String,
    @ColumnInfo(name = "message") val message: String,
    @ColumnInfo(name = "fk_submitted_by") val submittedBy: String,
    @ColumnInfo(name = "notification_type") val notificationType: NotificationType,
    @ColumnInfo(name = "transaction_id") val transactionId: String,
    @ColumnInfo(name = "timestamp") val timestamp: Date,
    @ColumnInfo(name = "is_seen") val isSeen: Boolean,
    // Syncable
    @ColumnInfo(name = "sync_status") val syncStatus: SyncStatus
) : SyncableChildEntity {

    override fun getUniqueId(): String =
        id

    override fun getForeignKey(): String =
        submittedBy

    override fun getSyncableStatus(): SyncStatus =
        syncStatus

    override fun cloneAndSetArg(newSyncStatus: SyncStatus): Notification =
        Notification(
            this.id,
            this.message,
            this.submittedBy,
            this.notificationType,
            this.transactionId,
            this.timestamp,
            this.isSeen,
            syncStatus
        )

    override fun cloneAndSetArg(newId: String, newSyncStatus: SyncStatus): Notification {
        throw IllegalStateException("Function not supported.")
    }
}