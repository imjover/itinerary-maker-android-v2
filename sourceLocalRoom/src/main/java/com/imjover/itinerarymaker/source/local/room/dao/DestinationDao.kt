/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.entity.Destination
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object of the [Destination].
 */
@Dao
internal interface DestinationDao : CrudDao<Destination> {

    /**
     * Retrieve a [Destination] with the given [id].
     *
     * @param id The ID of this record.
     * @return The found [Destination].
     */
    @Query("SELECT * FROM tbl_destination WHERE pk_destination_id = :id LIMIT 1")
    suspend fun getDestination(id: String): Destination

    /**
     * Retrieve a stream [Destination] with the given [id].
     *
     * @param id The ID of this record.
     * @return The found stream [Destination].
     */
    @Query("SELECT * FROM tbl_destination WHERE pk_destination_id = :id LIMIT 1")
    fun getDestinationAsStream(id: String): Flow<Destination>

    /**
     * Retrieve all [Destination]s of a [Itinerary] and converts it into a stream.
     *
     * @param itineraryId The owner of the records.
     * @return A stream of [Destination]s.
     */
    @Query("SELECT * FROM tbl_destination WHERE fk_itinerary_id = :itineraryId")
    fun getStreamDestinationsOf(itineraryId: String): Flow<List<Destination>>

    /**
     * Retrieve all [Destination]s of a [Itinerary].
     *
     * @param itineraryId The owner of the records.
     * @return A list of [Destination]s.
     */
    @Query("SELECT * FROM tbl_destination WHERE fk_itinerary_id = :itineraryId")
    suspend fun getDestinationsOf(itineraryId: String): List<Destination>

    /**
     * Retrieves all [Destination]s that has [syncStatus] and owned by the given [itineraryId].
     *
     * @param itineraryId The owner of the records.
     * @param syncStatus The synchronization status.
     * @return A list of [Destination]s.
     */
    @Query("SELECT * FROM tbl_destination WHERE fk_itinerary_id = :itineraryId AND sync_status = :syncStatus")
    suspend fun getDestinationsOf(itineraryId: String, syncStatus: SyncStatus): List<Destination>

    /**
     * Updates the primary key to [newId] and [syncStatus] of the record with [currentId].
     *
     * @param currentId The current ID of this record.
     * @param newId The new ID to be used on this record.
     * @param syncStatus The synchronization status.
     */
    @Query("UPDATE tbl_destination SET pk_destination_id = :newId, sync_status = :syncStatus WHERE pk_destination_id = :currentId")
    suspend fun updateId(currentId: String, newId: String, syncStatus: SyncStatus)
}