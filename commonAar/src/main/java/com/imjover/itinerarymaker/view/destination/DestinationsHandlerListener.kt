/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.destination

import com.imjover.itinerarymaker.entity.Destination

/**
 * Listener of the parent fragment that handles the destinations.
 */
interface DestinationsHandlerListener {

    /**
     * Shows the bottom sheet
     *
     * @param destinationId The ID of the [Destination] to be displayed.
     */
    fun showBottomSheetOf(destinationId: String)
}