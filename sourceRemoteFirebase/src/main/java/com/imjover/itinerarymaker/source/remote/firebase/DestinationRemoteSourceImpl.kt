/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.firebase

import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.source.remote.DestinationRemoteSource


class DestinationRemoteSourceImpl : DestinationRemoteSource {

    override suspend fun insert(entity: Destination): String {
        TODO("Not yet implemented")
    }

    override suspend fun update(entity: Destination) {
        TODO("Not yet implemented")
    }

    override suspend fun delete(entity: Destination) {
        TODO("Not yet implemented")
    }

    override suspend fun getListChildrenOf(parentId: String): List<Destination> {
        TODO("Not yet implemented")
    }
}