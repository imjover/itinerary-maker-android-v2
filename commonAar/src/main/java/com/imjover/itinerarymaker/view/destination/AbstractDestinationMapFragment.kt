/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.destination

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.observe
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.GoogleMap.OnPoiClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.imjover.itinerarymaker.BuildConfig
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.viewmodel.destination.AbstractDestinationMapViewModel


private const val MAPS_ZOOM_PADDING = 100

/**
 * The screen that holds the MAP representation of all destinations.
 */
abstract class AbstractDestinationMapFragment<VM: AbstractDestinationMapViewModel, DB : ViewDataBinding> :
    AbstractFragment<VM, DB>(),
    OnMapReadyCallback, OnMarkerClickListener, OnPoiClickListener {

    private lateinit var map: GoogleMap
    private var markers = mutableListOf<Marker>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        // Get the arguments passed from previous screen
        viewModel.init(
            requireArguments().getStringArray(ARG_PARENT_IDS) ?: throw IllegalStateException()
        )

        // Setup the map
        getMapFragment().getMapAsync(this)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.markerOptions.observe(viewLifecycleOwner, this::handleMarkers)
        viewModel.markerToUpdate.observe(viewLifecycleOwner, {
            it.consume { consumableMarker ->
                markers.forEach { marker ->
                    if (marker.position == consumableMarker.position) {
                        marker.setIcon(consumableMarker.icon) // for now, update the icon only
                        return@forEach
                    }
                }
            }
        })
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setOnMarkerClickListener(this)
        map.setOnPoiClickListener(this)
        map.setMapStyle(
            MapStyleOptions.loadRawResourceStyle(
                requireContext(),
                R.raw.map_style_basic_json
            )
        )
        val loadedMarkers = viewModel.markerOptions.value
        if (!loadedMarkers.isNullOrEmpty()) {
            handleMarkers(loadedMarkers)
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        val destinationId = viewModel.getDestinationIdOf(marker)
        (parentFragment as DestinationsHandlerListener).showBottomSheetOf(destinationId)
        return true
    }

    override fun onPoiClick(poi: PointOfInterest) {
        // FIXME
        Toast.makeText(
            requireContext(), "Clicked: " +
                    poi.name + "\nPlace ID:" + poi.placeId +
                    "\nLatitude:" + poi.latLng.latitude +
                    " Longitude:" + poi.latLng.longitude,
            Toast.LENGTH_SHORT
        ).show()
    }


    /***********************************************************************************************
     * Public functions.
     **********************************************************************************************/

    /**
     * Adds pin on the map.
     *
     * @param latitude The latitude of the place.
     * @param longitude The longitude of the place.
     * @param name Name or description of the place.
     */
    fun addPin(latitude: Double, longitude: Double, name: String) {
        val marker = MarkerOptions()
            .position(LatLng(latitude, longitude))
            .title(name)
        map.addMarker(marker)
    }

    fun getMap(): GoogleMap? =
        if (this::map.isInitialized) map else null


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Handles updates on list of markers.
     */
    private fun handleMarkers(markerOptions: List<MarkerOptions>) {
        if (!this::map.isInitialized) {
            Log.w(toString(), "handleMarkers: Map is not yet initialized.")
            // TODO: Inform user to fix his google maps
            return
        }
        // TODO: Should we clear the map? then add all markers again?
        map.clear()
        if (markerOptions.isNotEmpty()) {
            val builder = LatLngBounds.Builder()
            val polylineOptions = PolylineOptions()
            markerOptions.forEach {
                val marker = map.addMarker(it)
                markers.add(marker)
                builder.include(it.position)
                polylineOptions.add(it.position)
            }
            map.addPolyline(polylineOptions)
            // Add marker's position to builder.
            // This is to compute the initial zoom of map
            // and show all markers inside the map.
            val bounds = builder.build()
            val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, MAPS_ZOOM_PADDING)
            map.moveCamera(cameraUpdate)
        }
    }

    /**
     * Retrieves the map from layout.
     */
    abstract fun getMapFragment(): SupportMapFragment


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }

    override fun toString(): String =
        if (BuildConfig.DEBUG) "AbstractDestinationMapFragment" else super.toString()
}