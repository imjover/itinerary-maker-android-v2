/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.imjover.itinerarymaker.entity.Entity


/**
 * An update-able [RecyclerView] [Adapter].
 * Automatically handles the call to [notifyDataSetChanged] whenever the data is updated.
 */
abstract class UpdatableRecyclerViewAdapter<E : Entity, B : ViewDataBinding> :
    Adapter<UpdatableRecyclerViewAdapter.AdapterViewHolder<B>>() {

    var list = mutableListOf<E>() // TODO Make this private

    /**
     * Called when an item in the Adapter is clicked.
     *
     * @param id The unique ID of the clicked item.
     */
    abstract fun getOnClickListener(id: String): View.OnClickListener?

    /**
     * Updates the data content of this adapter.
     *
     * @param data The list of [Entity]s to be displayed.
     */
    fun updateData(data: E) {
        var updated = false
        list.forEachIndexed { index, e ->
            if (e.getUniqueId() == data.getUniqueId()) {
                list[index] = data
                updated = true
            }
        }
        if (!updated) {
            list.add(data)
        }
        notifyDataSetChanged()
    }

    /**
     * Retrieve the data from the given position
     */
    fun getData(position: Int) =
        list[position]

    override fun getItemCount(): Int =
        list.size

    class AdapterViewHolder<B : ViewDataBinding>(val binding: B) :
        ViewHolder(binding.root)
}