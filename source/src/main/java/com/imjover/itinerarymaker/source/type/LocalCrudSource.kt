/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.type

import com.imjover.itinerarymaker.entity.Entity
import kotlinx.coroutines.flow.Flow

/**
 * Defines local sources that can do CRUD (create, read, update, delete).
 */
interface LocalCrudSource<E: Entity> {

    /**
     * Retrieves an [Entity] with the given [id].
     *
     * @param id The ID of [Entity].
     * @return The found [Entity].
     */
    suspend fun get(id: String): E

    /**
     * Retrieves a stream of [Entity] with the given [id].
     *
     * @param id The ID of [Entity].
     * @return The found stream [Entity].
     */
    fun getAsStream(id: String): Flow<E>

    /**
     * Inserts a new cache of [Entity].
     *
     * @param entity The [Entity] to be inserted.
     */
    suspend fun insert(entity: E)

    /**
     * Updates the cache of [Entity]s.
     *
     * @param entity The [Entity] to be updated.
     */
    suspend fun update(entity: E)

    /**
     * Inserts or updates the cache of [Entity].
     *
     * @param entity The [Entity] to be inserted or updated.
     */
    suspend fun insertOrUpdate(entity: E)

    /**
     * Deletes the cache of [Entity].
     *
     * @param entity The [Entity] to be deleted.
     */
    suspend fun delete(entity: E)
}