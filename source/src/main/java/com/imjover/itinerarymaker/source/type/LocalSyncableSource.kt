/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.type

import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.entity.SyncableEntity
import kotlinx.coroutines.flow.Flow

/**
 * Defines local sources that can be synced in both remote and local sources.
 */
interface LocalSyncableSource<SE: SyncableEntity, ParentId> {

    /**
     * Retrieves a stream of children of a given [ParentId].
     *
     * @param parentId The ID of the parent.
     * @return Stream of children.
     */
    fun getStreamChildrenOf(parentId: ParentId): Flow<List<SE>>

    /**
     * Retrieves all children of a given [ParentId] and with the given [syncStatus].
     *
     * @param parentId The ID of the parent.
     * @param syncStatus The synchronization status.
     * @return List of children.
     */
    suspend fun getListChildrenOf(parentId: ParentId, syncStatus: SyncStatus): List<SE>

    /**
     * Updates the primary key of [SE] to [newId], together with new [syncStatus].
     *
     * @param toUpdate The record to update.
     * @param newId The new ID to be used on this record.
     * @param syncStatus The synchronization status.
     */
    suspend fun updateId(toUpdate: SE, newId: String, syncStatus: SyncStatus)
}