/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.expense.di

import com.imjover.itinerarymaker.manager.ExpenseManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.manager.expense.internal.ExpenseManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the [ExpenseManager].
 */
@Module
abstract class ExpenseManagerModule(userManager: UserManager) {

    @Singleton
    @Binds
    internal abstract fun bindExpenseManager(expenseManager: ExpenseManagerImpl): ExpenseManager
}