/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.mock

import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.NotificationType
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.util.IdGenerator
import com.imjover.itinerarymaker.source.remote.NotificationRemoteSource
import com.imjover.itinerarymaker.source.remote.mock.TravelRemoteSourceMock.Companion.TRAVEL_1_of_TRAVELER_1
import com.imjover.itinerarymaker.source.remote.mock.TravelRemoteSourceMock.Companion.TRAVEL_2_of_TRAVELER_1
import com.imjover.itinerarymaker.source.remote.mock.UserRemoteSourceMock.Companion.AGENT_1
import com.imjover.itinerarymaker.source.remote.mock.UserRemoteSourceMock.Companion.TRAVELER_1
import com.imjover.itinerarymaker.source.type.ChildSource
import kotlinx.coroutines.delay
import java.util.*


class NotificationRemoteSourceMock : NotificationRemoteSource {

    companion object {
        val NOTIF_1_of_TRAVELER_1 = Notification(
            "1",
            "Review your travel itinerary to \"${TRAVEL_1_of_TRAVELER_1.name}\" sent by agent ${AGENT_1.lastName}, ${AGENT_1.firstName}.",
            AGENT_1.id,
            TRAVELER_1.id,
            NotificationType.TRAVEL_REVIEW,
            TRAVEL_1_of_TRAVELER_1.id,
            Date(),
            false,
            SyncStatus.CLEAN
        )
        val NOTIF_2_of_TRAVELER_1 = Notification(
            "2",
            "Review your travel itinerary to \"${TRAVEL_2_of_TRAVELER_1.name}\" sent by agent ${AGENT_1.lastName}, ${AGENT_1.firstName}.",
            AGENT_1.id,
            TRAVELER_1.id,
            NotificationType.TRAVEL_REVIEW,
            TRAVEL_2_of_TRAVELER_1.id,
            Date(),
            false,
            SyncStatus.CLEAN
        )
        val NOTIF_1_of_AGENT_1 = Notification(
            "3",
            "Traveler \"${TRAVEL_2_of_TRAVELER_1.name}\" approved the travel you created.",
            TRAVELER_1.id,
            AGENT_1.id,
            NotificationType.TRAVEL_REVIEW,
            TRAVEL_2_of_TRAVELER_1.id,
            Date(),
            false,
            SyncStatus.CLEAN
        )
    }

    private val cachedNotifications = mutableMapOf<String, Notification>(
        NOTIF_1_of_TRAVELER_1.id to NOTIF_1_of_TRAVELER_1,
        NOTIF_2_of_TRAVELER_1.id to NOTIF_2_of_TRAVELER_1,
        NOTIF_1_of_AGENT_1.id to NOTIF_1_of_AGENT_1
    )

    /**
     * Adds new notification.
     */
    @Deprecated("It seems this is useless, lols.")
    internal fun addNotification(
        message: String,
        submittedBy: String,
        submittedTo: String,
        notificationType: NotificationType,
        transactionId: String
    ) {
        val id = IdGenerator.generate("NRM", submittedBy)
        cachedNotifications[id] = Notification(
            id,
            message,
            submittedBy,
            submittedTo,
            notificationType,
            transactionId,
            Date(),
            false,
            SyncStatus.CLEAN
        )
    }

    /***********************************************************************************************
     * Overriden functions of [NotificationRemoteSource]
     **********************************************************************************************/

    override suspend fun setAsSeen(ids: List<String>) {
        delay(MockConfig.DELAY_TIME)

        ids.forEach {
            val temp = cachedNotifications[it]!!
            val updated = temp.updateIsSeen(true)
            cachedNotifications[it] = updated
        }
    }


    /***********************************************************************************************
     * Overriden functions of [ChildSource]
     **********************************************************************************************/

    override suspend fun getListChildrenOf(parentId: String): List<Notification> {
        delay(MockConfig.DELAY_TIME)

        return cachedNotifications.filter {
            it.value.getForeignKey() == parentId
        }.values.toList()
    }
}

fun Notification.updateIsSeen(isSeen: Boolean): Notification =
    Notification(
        this.id,
        this.message,
        this.submittedBy,
        this.submittedTo,
        this.notificationType,
        this.transactionId,
        this.timestamp,
        isSeen,
        this.syncStatus
    )