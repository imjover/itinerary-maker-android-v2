/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.invoice

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.DialogPaymentFormBinding
import com.imjover.itinerarymaker.agency.viewmodel.invoice.PaymentFormViewModel
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.helper.toImageRes
import com.imjover.itinerarymaker.helper.toStringRes
import com.imjover.itinerarymaker.view.AbstractDialogFragment
import com.imjover.itinerarymaker.view.widget.IconTextSpinnerAdapter


private val PAYMENT_MODES = PaymentMode.values().toSpinnerItems()

/**
 * The screen that holds the form to process payment.
 */
class PaymentFormDialog :
    AbstractDialogFragment<PaymentFormViewModel, DialogPaymentFormBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get the arguments passed from previous screen
        val args = PaymentFormDialogArgs.fromBundle(requireArguments())
        viewModel.parentIds = args.rootParentId
        viewModel.parentTravelId = args.parentTravelId

        // Setup the UI
        isCancelable = false
        dataBinding.spPaymentMode.adapter =
            IconTextSpinnerAdapter(
                this.requireContext(),
                PAYMENT_MODES
            )
        dataBinding.btnSave.setOnClickListener {
            save()
        }
        dataBinding.btnCancel.setOnClickListener {
            dismiss()
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.dismiss.observe(viewLifecycleOwner, {
            dismiss()
        })
    }

    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Handles the saving process of the inputs.
     */
    private fun save() {
        val paymentMode =
            PaymentMode.fromId(dataBinding.spPaymentMode.selectedItem as String)
        viewModel.update(paymentMode)
    }

    /***********************************************************************************************
     * Required functions of [AbstractDialogFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.dialog_payment_form

    override fun getViewModelClass(): Class<PaymentFormViewModel> =
        PaymentFormViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}


/**
 * Converts an array of [PaymentMode] to list of [IconTextSpinnerAdapter.SpinnerItem] to be used
 * for as adapter for a [Spinner].
 *
 * @return [Spinner] adapter items.
 */
private fun Array<PaymentMode>.toSpinnerItems(): List<IconTextSpinnerAdapter.SpinnerItem> {
    val items = mutableListOf<IconTextSpinnerAdapter.SpinnerItem>()
    this.forEach {
        items.add(
            IconTextSpinnerAdapter.SpinnerItem(
                it.id,
                AppComponent.appContext.getString(it.toStringRes()),
                it.toImageRes()
            )
        )
    }
    return items
}