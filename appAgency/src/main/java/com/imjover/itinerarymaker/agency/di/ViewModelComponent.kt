/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.di

import com.imjover.itinerarymaker.AbstractUpdateDeviceTokenWorker
import com.imjover.itinerarymaker.agency.viewmodel.destination.DestinationFormViewModel
import com.imjover.itinerarymaker.agency.viewmodel.destination.DestinationListViewModel
import com.imjover.itinerarymaker.agency.viewmodel.destination.DestinationMapViewModel
import com.imjover.itinerarymaker.agency.viewmodel.destination.DestinationsViewModel
import com.imjover.itinerarymaker.agency.viewmodel.expense.ExpenseFormViewModel
import com.imjover.itinerarymaker.agency.viewmodel.expense.ExpensesViewModel
import com.imjover.itinerarymaker.agency.viewmodel.invoice.PaymentFormViewModel
import com.imjover.itinerarymaker.agency.viewmodel.itinerary.ItinerariesViewModel
import com.imjover.itinerarymaker.agency.viewmodel.itinerary.ItineraryFormViewModel
import com.imjover.itinerarymaker.agency.viewmodel.login.LoginViewModel
import com.imjover.itinerarymaker.agency.viewmodel.login.RegistrationFormViewModel
import com.imjover.itinerarymaker.agency.viewmodel.main.MainViewModel
import com.imjover.itinerarymaker.agency.viewmodel.notification.NotificationsViewModel
import com.imjover.itinerarymaker.agency.viewmodel.report.ReportsViewModel
import com.imjover.itinerarymaker.agency.viewmodel.travel.TravelFormViewModel
import com.imjover.itinerarymaker.agency.viewmodel.travel.TravelsViewModel
import com.imjover.itinerarymaker.agency.viewmodel.traveler.TravelersViewModel
import com.imjover.itinerarymaker.agency.viewmodel.user.UsersViewModel
import com.imjover.itinerarymaker.manager.destination.di.DestinationManagerModule
import com.imjover.itinerarymaker.manager.expense.di.ExpenseManagerModule
import com.imjover.itinerarymaker.manager.itinerary.di.ItineraryManagerModule
import com.imjover.itinerarymaker.manager.notification.di.NotificationManagerModule
import com.imjover.itinerarymaker.manager.report.di.ReportManagerModule
import com.imjover.itinerarymaker.manager.travel.di.TravelManagerModule
import com.imjover.itinerarymaker.manager.user.di.UsersManagerModule
import dagger.Component
import javax.inject.Singleton

/**
 * Injects the required modules of the view models.
 */
@Singleton
@Component(
    modules = [
        UsersManagerModule::class,
        TravelManagerModule::class,
        ItineraryManagerModule::class,
        DestinationManagerModule::class,
        ExpenseManagerModule::class,
        NotificationManagerModule::class,
        ReportManagerModule::class
    ]
)
interface ViewModelComponent {

    fun inject(worker: AbstractUpdateDeviceTokenWorker)
    fun inject(viewModel: MainViewModel)
    fun inject(viewModel: LoginViewModel)
    fun inject(viewModel: RegistrationFormViewModel)
    fun inject(viewModel: TravelersViewModel)
    fun inject(viewModel: NotificationsViewModel)
    fun inject(viewModel: TravelsViewModel)
    fun inject(viewModel: TravelFormViewModel)
    fun inject(viewModel: ItinerariesViewModel)
    fun inject(viewModel: ItineraryFormViewModel)
    fun inject(viewModel: DestinationsViewModel)
    fun inject(viewModel: DestinationListViewModel)
    fun inject(viewModel: DestinationMapViewModel)
    fun inject(viewModel: DestinationFormViewModel)
    fun inject(viewModel: ExpensesViewModel)
    fun inject(viewModel: ExpenseFormViewModel)
    fun inject(viewModel: PaymentFormViewModel)
    fun inject(viewModel: UsersViewModel)
    fun inject(viewModel: ReportsViewModel)
}