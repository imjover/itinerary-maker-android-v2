/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.BuildConfig
import com.imjover.itinerarymaker.exception.RemoteAccessException
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.view.login.AbstractLoginActivity
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [AbstractLoginActivity].
 */
abstract class AbstractLoginViewModel : BaseLoginViewModel() {

    @Inject lateinit var userManager: UserManager

    private val _loggedOut = MutableLiveData<Consumable<Unit>>()
    val loggedOut: LiveData<Consumable<Unit>> get() = _loggedOut

    init {
        injectDependencies()
    }

    /**
     * Do fake login, this is to fetch the logged in user in Firebase
     */
    abstract fun doFakeLogin()


    /**
     * Logs out the current logged-in user.
     */
    fun logout() {
        viewModelScope.launch {
            userManager.logout().fold({
                _loggedOut.value = Consumable(Unit)
            }, {
                Log.w(toString(), it)
                _errorMessage.value = it.getMessageForUI()
            })
        }
    }

    override fun toString(): String =
        if (BuildConfig.DEBUG) "AbstractLoginViewModel" else super.toString()
}