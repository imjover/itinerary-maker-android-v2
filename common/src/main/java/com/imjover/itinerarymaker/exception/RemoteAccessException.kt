/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.exception

/**
 * Exception thrown when an attempt to call the remote source fails.
 *
 * @param message The error message.
 */
@Deprecated("Return Result#failed instead of throwing exceptions.")
class RemoteAccessException(message: String) : Exception(message)
