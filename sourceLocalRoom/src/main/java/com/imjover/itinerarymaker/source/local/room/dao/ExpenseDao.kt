/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.entity.SyncStatus
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object of the [Expense].
 */
@Dao
internal interface ExpenseDao : CrudDao<Expense> {

    /**
     * Retrieve an [Expense] with the given [id].
     *
     * @param id The ID of this record.
     * @return The found [Expense].
     */
    @Query("SELECT * FROM tbl_expense WHERE pk_expense_id = :id LIMIT 1")
    suspend fun getExpense(id: String): Expense

    /**
     * Retrieve a stream [Expense] with the given [id].
     *
     * @param id The ID of this record.
     * @return The found stream [Expense].
     */
    @Query("SELECT * FROM tbl_expense WHERE pk_expense_id = :id LIMIT 1")
    fun getExpenseAsStream(id: String): Flow<Expense>

    /**
     * Retrieve all [Expense]s of a [Destination] and converts it into a stream.
     *
     * @param destinationId The owner of the records.
     * @return A stream of [Expense]s.
     */
    @Query("SELECT * FROM tbl_expense WHERE fk_destination_id = :destinationId")
    fun getStreamExpensesOf(destinationId: String): Flow<List<Expense>>

    /**
     * Retrieve all [Expense]s of a [Destination].
     *
     * @param destinationId The owner of the records.
     * @return A list of [Expense]s.
     */
    @Query("SELECT * FROM tbl_expense WHERE fk_destination_id = :destinationId")
    suspend fun getExpensesOf(destinationId: String): List<Expense>

    /**
     * Retrieves all [Expense]s that has [syncStatus] and owned by the given [destinationId].
     *
     * @param destinationId The owner of the records.
     * @param syncStatus The synchronization status.
     * @return A list of [Expense]s.
     */
    @Query("SELECT * FROM tbl_expense WHERE fk_destination_id = :destinationId AND sync_status = :syncStatus")
    suspend fun getExpensesOf(destinationId: String, syncStatus: SyncStatus): List<Expense>

    /**
     * Updates the primary key to [newId] and [syncStatus] of the record with [currentId].
     *
     * @param currentId The current ID of this record.
     * @param newId The new ID to be used on this record.
     * @param syncStatus The synchronization status.
     */
    @Query("UPDATE tbl_expense SET pk_expense_id = :newId, sync_status = :syncStatus WHERE pk_expense_id = :currentId")
    suspend fun updateId(currentId: String, newId: String, syncStatus: SyncStatus)
}