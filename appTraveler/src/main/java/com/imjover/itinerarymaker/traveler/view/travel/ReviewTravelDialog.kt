/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.travel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.imjover.itinerarymaker.traveler.R
import com.imjover.itinerarymaker.traveler.databinding.DialogReviewTravelBinding
import com.imjover.itinerarymaker.traveler.viewmodel.travel.ReviewTravelViewModel
import com.imjover.itinerarymaker.view.AbstractDialogFragment
import com.imjover.itinerarymaker.view.AbstractFragment


/**
 * The screen that holds the form to review travels.
 */
class ReviewTravelDialog :
    AbstractDialogFragment<ReviewTravelViewModel, DialogReviewTravelBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get the arguments passed from previous screen
        val args = ReviewTravelDialogArgs.fromBundle(requireArguments())
        viewModel.parentIds = args.parentIds
        viewModel.travelId = args.travelId

        dataBinding.btnApprove.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())
            builder.setMessage(R.string.review_travel_fragment__approve__question)
                .setPositiveButton(android.R.string.yes) { _, _ ->
                    viewModel.submitReview(true, dataBinding.etComment.text.toString())
                }
                .setNegativeButton(android.R.string.no) { _, _ ->
                    // Do nothing
                }.show()
        }
        dataBinding.btnDecline.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())
            builder.setMessage(R.string.review_travel_fragment__decline__question)
                .setPositiveButton(android.R.string.yes) { _, _ ->
                    viewModel.submitReview(false, dataBinding.etComment.text.toString())
                }
                .setNegativeButton(android.R.string.no) { _, _ ->
                    // Do nothing
                }.show()
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.showMessage.observe(viewLifecycleOwner, { consumable ->
            consumable.consume {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            }
        })
        viewModel.submitted.observe(viewLifecycleOwner, {
            dismiss()
        })
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.dialog_review_travel

    override fun getViewModelClass(): Class<ReviewTravelViewModel> =
        ReviewTravelViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}