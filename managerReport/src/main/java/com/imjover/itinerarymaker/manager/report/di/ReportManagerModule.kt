/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.report.di

import com.imjover.itinerarymaker.manager.ReportManager
import com.imjover.itinerarymaker.manager.report.internal.ReportManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the [ReportManager].
 */
@Module
abstract class ReportManagerModule {

    @Singleton
    @Binds
    internal abstract fun bindUsersManager(reportManager: ReportManagerImpl): ReportManager
}