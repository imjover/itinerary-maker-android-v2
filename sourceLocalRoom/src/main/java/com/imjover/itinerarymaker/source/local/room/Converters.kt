/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room

import android.net.Uri
import androidx.room.TypeConverter
import com.imjover.itinerarymaker.entity.*
import java.math.BigDecimal
import java.util.*

/**
 * Converters for room entities.
 */
internal class Converters {

    /***********************************************************************************************
     * [Date] x [Long] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromTimestampToDate(value: Long): Date =
        Date(value)

    @TypeConverter
    fun dateToTimestamp(date: Date): Long =
        date.time


    /***********************************************************************************************
     * [Status] x [Int] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromIdToStatus(value: Int): Status =
        Status.fromId(value)

    @TypeConverter
    fun statusToId(status: Status): Int =
        status.id


    /***********************************************************************************************
     * [SyncStatus] x [Int] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromIdToSyncStatus(value: Int): SyncStatus =
        SyncStatus.fromId(value)

    @TypeConverter
    fun syncStatusToId(syncStatus: SyncStatus): Int =
        syncStatus.id


    /***********************************************************************************************
     * [UserType] x [Int] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromIdToUserType(value: Int): UserType =
        UserType.fromId(value)

    @TypeConverter
    fun userTypeToId(userType: UserType): Int =
        userType.id


    /***********************************************************************************************
     * [DestinationType] x [String] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromIdToDestinationType(value: String): DestinationType =
        DestinationType.fromId(value)

    @TypeConverter
    fun destinationTypeToId(destinationType: DestinationType): String =
        destinationType.id


    /***********************************************************************************************
     * [UserType] x [String] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromIdToTransportationMode(value: String): TransportationMode =
        TransportationMode.fromId(value)

    @TypeConverter
    fun transportationModeToId(transportationMode: TransportationMode): String =
        transportationMode.id


    /***********************************************************************************************
     * [ExpenseType] x [String] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromIdToExpenseType(value: String): ExpenseType =
        ExpenseType.fromId(value)

    @TypeConverter
    fun expenseTypeToId(expenseType: ExpenseType): String =
        expenseType.id


    /***********************************************************************************************
     * [BigDecimal] x [Long] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromLongToBigDecimal(value: Long): BigDecimal =
        BigDecimal(value).divide(BigDecimal(100))

    @TypeConverter
    fun bigDecimalToLong(bigDecimal: BigDecimal): Long =
        bigDecimal.multiply(BigDecimal(100)).toLong()


    /***********************************************************************************************
     * [ExpenseType] x [String] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromIdToNotificationType(value: String): NotificationType =
        NotificationType.fromId(value)

    @TypeConverter
    fun notificationTypeToId(notificationType: NotificationType): String =
        notificationType.id


    /***********************************************************************************************
     * [PaymentMode] x [String] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromIdToPaymentMode(value: String?): PaymentMode? =
        value?.let { PaymentMode.fromId(it) }

    @TypeConverter
    fun paymentModeToId(paymentMode: PaymentMode?): String? =
        paymentMode?.id


    /***********************************************************************************************
     * [Uri] x [String] converters
     **********************************************************************************************/

    @TypeConverter
    fun fromStringUrlToUri(value: String?): Uri? =
        value?.let { Uri.parse(it) }

    @TypeConverter
    fun uriToStringUrl(uri: Uri?): String? =
        uri?.toString()
}