/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.BuildConfig
import com.imjover.itinerarymaker.view.splash.AbstractSplashActivity
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


/**
 * The view model of [AbstractSplashActivity].
 */
class SplashViewModel : AbstractViewModel() {

    private val _splashProgress: MutableLiveData<SplashProgress> = MutableLiveData()
    val splashProgress: LiveData<SplashProgress>
        get() = _splashProgress

    /**
     * Starts the activities of splash.
     */
    fun startBackgroundProcesses() {
        viewModelScope.launch {
            _splashProgress.value = SplashProgress.STARTED
            if (BuildConfig.FLAVOR == "mock") {
                delay(2000)
            }
            _splashProgress.value = SplashProgress.ENDED
        }
    }
}