/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.expense

import android.view.View
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.view.expense.AbstractExpenseAdapter


/**
 * List adapter of [Expense] item.
 */
internal class ExpenseAdapter : AbstractExpenseAdapter() {

    override fun getOnClickListener(id: String): View.OnClickListener? = null
}