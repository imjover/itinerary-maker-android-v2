/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager

import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.util.Result
import kotlinx.coroutines.flow.Flow

/**
 * Manages the users of the application.
 */
interface UserManager {

    /**
     * Validates the [username] and [password] if it matches to a user.
     *
     * @param username The username of the user.
     * @param password The password of the user.
     * @return The found user, or an exception.
     */
    suspend fun findUserByLogin(username: String, password: String): Result<User>

    /**
     * Logs out the current logged-in user.
     */
    suspend fun logout(): Result<Unit>

    /**
     * Registers the current user.
     *
     * @param userType The user type.
     * @return The response message.
     */
    suspend fun register(userType: UserType): Result<String>

    /**
     * Approves a user's registration.
     *
     * @param user The user to be registered.
     * @return The response message.
     */
    suspend fun approveRegistration(user: User): Result<String>

    /**
     * Retrieves the current logged-in user. The [findUserByLogin] must be called first successfully.
     */
    fun getLoggedInUser(): User

    /**
     * Retrieves a [User] with the given [id].
     *
     * @param id The ID of this record.
     * @return Stream of [User]s.
     */
    suspend fun getUser(id: String): Result<User>

    /**
     * Retrieves all [User]s with the given [UserType].
     *
     * @param nameQuery The name to be searched for.
     * @param userTypes The user types.
     * @return Stream of [User]s.
     */
    fun getUsers(nameQuery: String, vararg userTypes: UserType): Flow<User>

    /**
     * Updates the registered device token used in push notification.
     *
     * @param token The registration device token.
     */
    suspend fun updateDeviceToken(token: String): Result<Unit>
}