/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.BuildConfig
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.exception.RemoteAccessException
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.view.main.AbstractMainActivity
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import com.imjover.itinerarymaker.viewmodel.InjectableViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * The view model of [AbstractMainActivity].
 */
abstract class AbstractMainViewModel : AbstractViewModel(), InjectableViewModel {

    @Inject
    lateinit var userManager: UserManager

    private val _progressDialog: MutableLiveData<Int?> = MutableLiveData()
    internal val progressDialog: LiveData<Int?> get() = _progressDialog

    private val _showMessage: MutableLiveData<Consumable<String>> = MutableLiveData()
    val showMessage: LiveData<Consumable<String>> get() = _showMessage

    private val _signOut: MutableLiveData<Unit> = MutableLiveData()
    internal val signOut: LiveData<Unit> get() = _signOut

    init {
        injectDependencies()
    }

    /**
     * Logs out the current logged-in user.
     */
    fun logout() {
        viewModelScope.launch {
            _progressDialog.value = R.string.main_activity__logging_out__message
            userManager.logout().fold({
                _signOut.value = null
            }, {
                Log.w(toString(), it)
                _showMessage.value =
                    Consumable(it.getMessageForUI(R.string.main_activity__logout_failed__message))
            })
            _progressDialog.value = null
        }
    }

    /**
     * Retrieves the currently logged in user.
     *
     * @return The user.
     */
    fun getLoggedInUser(): User =
        userManager.getLoggedInUser()

    override fun toString(): String =
        if (BuildConfig.DEBUG) "AbstractMainViewModel" else super.toString()
}