/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel

/**
 * Defines the view model that can be injected with dependencies.
 */
interface InjectableViewModel {

    /**
     * Do the injection to this view model.
     */
    fun injectDependencies()
}