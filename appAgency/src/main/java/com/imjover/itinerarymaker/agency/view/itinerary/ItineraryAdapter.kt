/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.itinerary

import android.view.View
import androidx.navigation.findNavController
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.view.itinerary.AbstractItineraryAdapter

/**
 * List adapter of [Itinerary] item.
 */
class ItineraryAdapter(
    private val parentIds: Array<String>,
    private val isEditable: () -> Boolean
) : AbstractItineraryAdapter() {

    override fun getOnClickListener(id: String): View.OnClickListener? =
        View.OnClickListener {
            val action =
                ItinerariesFragmentDirections.actionItinerariesFragmentToDestinationsFragment(
                    parentIds.plus(id),
                    isEditable()
                )
            it.findNavController().navigate(action)
        }
}