/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager

import com.imjover.itinerarymaker.entity.SyncableEntity
import kotlinx.coroutines.flow.Flow

/**
 * Defines managers that can be synced in both remote and local sources.
 */
interface SyncableManager<Child : SyncableEntity> {

    /**
     * Retrieves the [Child]ren of the given [parentIds].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @return Stream of [Child]ren.
     */
    fun getStreamChildrenOf(parentIds: Array<String>): Flow<Child>

    /**
     * Synchronize all [Child]ren of the given [parentIds] on both local and remote sources.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     */
    suspend fun syncChildrenOf(parentIds: Array<String>)
}