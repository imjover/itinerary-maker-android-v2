/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.travel.di

import com.imjover.itinerarymaker.manager.TravelManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.manager.travel.internal.TravelManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the [TravelManager].
 */
@Module
abstract class TravelManagerModule(userManager: UserManager) {

    @Singleton
    @Binds
    internal abstract fun bindTravelsManager(travelsManager: TravelManagerImpl): TravelManager
}