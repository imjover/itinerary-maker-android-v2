/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.travel.internal

import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.manager.TravelManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.repository.TravelRepository
import com.imjover.itinerarymaker.repository.di.RepositoryFactory
import com.imjover.itinerarymaker.util.IdGenerator
import com.imjover.itinerarymaker.util.Result
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject


private val AGENT_USER_TYPES = listOf(UserType.AGENT, UserType.ADMIN)

internal class TravelManagerImpl @Inject constructor() : TravelManager {

    @Inject
    lateinit var userManager: UserManager

    private val travelRepository: TravelRepository =
        RepositoryFactory.singleton().getTravelRepository()

    /**
     * Checks whether the user is allowed to use this manager or not.
     *
     * @param isAgentFeature Whether the expected feature is for agent and admin, then true, else false.
     * @throws IllegalStateException When the user is NOT allowed to use this manager.
     */
    private fun checkIfUserIsAllowed(isAgentFeature: Boolean) {
        val userIsAgent = AGENT_USER_TYPES.contains(userManager.getLoggedInUser().userType)
        val userIsAllowed = (isAgentFeature && userIsAgent) || (!isAgentFeature && !userIsAgent)
        if (!userIsAllowed) {
            throw IllegalStateException("The user is NOT allowed to use this feature")
        }
    }


    /***********************************************************************************************
     * Syncable-related methods.
     **********************************************************************************************/

    override fun getStreamChildrenOf(parentIds: Array<String>): Flow<Travel> =
        travelRepository.getStreamChildrenOf(parentIds)

    override suspend fun syncChildrenOf(parentIds: Array<String>) {
        travelRepository.syncChildrenOf(parentIds)
    }


    /***********************************************************************************************
     * CRUD methods.
     **********************************************************************************************/

    override suspend fun getTravel(parentIds: Array<String>, id: String): Travel {
        return travelRepository.getCached(parentIds, id)
    }

    override fun getTravelAsStream(parentIds: Array<String>, id: String): Flow<Travel> =
        travelRepository.getCachedAsStream(parentIds, id)

    override suspend fun getCachedStatus(parentIds: Array<String>, travelId: String): Status =
        travelRepository.getCachedStatus(parentIds, travelId)

    override suspend fun insert(
        parentIds: Array<String>,
        description: String,
        startDate: Date,
        endDate: Date
    ): String {
        checkIfUserIsAllowed(true)
        val loggedInUser = userManager.getLoggedInUser().id
        val temporaryId = IdGenerator.generate("TLM", loggedInUser)
        val travel = Travel(
            temporaryId,
            loggedInUser,
            description,
            startDate,
            endDate,
            null,
            Status.IN_PROGRESS,
            null,
            null,
            Date(),
            loggedInUser,
            SyncStatus.TO_INSERT
        )
        return travelRepository.insert(parentIds, travel)
    }

    override suspend fun update(
        parentIds: Array<String>,
        oldTravel: Travel,
        description: String,
        startDate: Date,
        endDate: Date
    ) {
        checkIfUserIsAllowed(true)
        val loggedInUser = userManager.getLoggedInUser().id
        val travel = Travel(
            oldTravel.id,
            oldTravel.agent,
            description,
            startDate,
            endDate,
            oldTravel.remarks,
            oldTravel.status,
            oldTravel.statusReason,
            oldTravel.paymentMode,
            Date(),
            loggedInUser,
            SyncStatus.TO_UPDATE
        )
        travelRepository.update(parentIds, travel)
    }

    override suspend fun delete(parentIds: Array<String>, vararg travels: Travel) {
        travelRepository.delete(parentIds, *travels)
    }


    /***********************************************************************************************
     * Review-related methods.
     **********************************************************************************************/

    override suspend fun submitForReview(
        parentIds: Array<String>,
        travelId: String
    ): Result<String> {
        checkIfUserIsAllowed(true)
        return travelRepository.submitForReview(parentIds, travelId)
    }

    override suspend fun submitReview(
        parentIds: Array<String>,
        travelId: String,
        isApprove: Boolean,
        reason: String?
    ): Result<String> {
        checkIfUserIsAllowed(false)
        return travelRepository.submitReview(parentIds, travelId, isApprove, reason)
    }


    /***********************************************************************************************
     * Other methods.
     **********************************************************************************************/

    override suspend fun cancel(parentIds: Array<String>, travelId: String) {
        travelRepository.cancel(parentIds, travelId)
    }

    override suspend fun markAsPaid(
        parentIds: Array<String>,
        travelId: String,
        paymentMode: PaymentMode
    ) {
        travelRepository.markAsPaid(
            parentIds,
            travelId,
            paymentMode,
            userManager.getLoggedInUser().id,
            Date()
        )
    }
}