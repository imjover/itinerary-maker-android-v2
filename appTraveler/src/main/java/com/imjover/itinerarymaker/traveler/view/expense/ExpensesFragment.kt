/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.expense

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.traveler.R
import com.imjover.itinerarymaker.traveler.databinding.FragmentExpensesBinding
import com.imjover.itinerarymaker.traveler.viewmodel.expense.ExpensesViewModel
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.AbstractRefreshableFragment
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter


/**
 * The screen that holds all expenses.
 */
class ExpensesFragment :
    AbstractRefreshableFragment<Expense, ExpensesViewModel, FragmentExpensesBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get the arguments passed from previous screen
        val args = ExpensesFragmentArgs.fromBundle(requireArguments())
        viewModel.parentIds = args.parentIds
        requireActivity().title = args.destinationDesc

        return view
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_expenses

    override fun getViewModelClass(): Class<ExpensesViewModel> =
        ExpensesViewModel::class.java

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }

    override fun getRecyclerView(): RecyclerView =
        dataBinding.rvExpenseList

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout =
        dataBinding.swipeContainer

    override fun getAdapter(): UpdatableRecyclerViewAdapter<Expense, *> =
        ExpenseAdapter()
}