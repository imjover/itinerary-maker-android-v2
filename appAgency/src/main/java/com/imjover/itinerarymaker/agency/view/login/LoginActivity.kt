/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.login

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.view.main.MainActivity
import com.imjover.itinerarymaker.agency.viewmodel.login.LoginViewModel
import com.imjover.itinerarymaker.view.AbstractActivity
import com.imjover.itinerarymaker.view.login.AbstractLoginActivity
import com.imjover.itinerarymaker.view.login.BaseLoginActivity


/**
 * The login screen.
 */
class LoginActivity : AbstractLoginActivity<LoginViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.registerUser.observe(this, {
            val builder = AlertDialog.Builder(this)
            builder.setMessage(R.string.login_activity__register_user__question)
                .setPositiveButton(R.string.button__register) { _, _ ->
                    RegistrationFormDialog().show(supportFragmentManager, "tag_register")
                }
                .setNegativeButton(android.R.string.cancel) { _, _ ->
                    // Logout so that the user can use different account
                    viewModel.logout()
                }
                .show()
        })
    }

    /***********************************************************************************************
     * Required functions of [BaseLoginActivity].
     **********************************************************************************************/

    override fun getClassOfNextActivity(): Class<Activity> =
        MainActivity::class.java as Class<Activity>


    /***********************************************************************************************
     * Required functions of [AbstractActivity].
     **********************************************************************************************/

    override fun getViewModelClass(): Class<LoginViewModel> =
        LoginViewModel::class.java
}