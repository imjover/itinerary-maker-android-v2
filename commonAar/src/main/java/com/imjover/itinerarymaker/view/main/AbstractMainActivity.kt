/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.main

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.google.android.material.navigation.NavigationView
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.helper.toStringRes
import com.imjover.itinerarymaker.util.image.CircleTransform
import com.imjover.itinerarymaker.view.AbstractActivity
import com.imjover.itinerarymaker.view.login.BaseLoginActivity
import com.imjover.itinerarymaker.viewmodel.main.AbstractMainViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.nav_header.view.*


/**
 * The main activity of the app.
 */
abstract class AbstractMainActivity<VM : AbstractMainViewModel, DB : ViewDataBinding> :
    AbstractActivity<VM, DB>() {

    private lateinit var dialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loadUserProfile()
        viewModel.signOut.observe(this, Observer {
            showMessage(getString(R.string.main_activity__logout__message))
            val intent = Intent(this, getLoginActivity())
            startActivity(intent)
            finish()
        })
        viewModel.showMessage.observe(this, { it.consume {
                message -> showMessage(message)
        }})
        viewModel.progressDialog.observe(this, Observer {
            if (it != null) {
                if (!this::dialog.isInitialized) {
                    dialog = ProgressDialog(this)
                }
                dialog.setMessage(getString(it))
                dialog.show()
            } else {
                dialog.dismiss()
            }
        })
    }

    /**
     * Loads the user's profile.
     */
    private fun loadUserProfile() {
        val navHeader = getNavigationView().inflateHeaderView(R.layout.nav_header)
        val user = viewModel.getLoggedInUser()
        Picasso.get()
            .load(user.photoUrl)
            .placeholder(R.drawable.ic_user_24)
            .transform(CircleTransform())
            .into(navHeader.iv_profile_pic)
        navHeader.tv_name.text = user.name
        navHeader.tv_user_type.setText(user.userType.toStringRes())
    }

    /***********************************************************************************************
     * Abstract functions.
     **********************************************************************************************/

    abstract fun getLoginActivity(): Class<BaseLoginActivity<*, *>>

    abstract fun getNavigationView(): NavigationView

    abstract fun showMessage(message: String)
}