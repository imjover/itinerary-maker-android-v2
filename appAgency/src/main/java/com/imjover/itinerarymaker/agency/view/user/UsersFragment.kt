/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.user

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.FragmentUsersBinding
import com.imjover.itinerarymaker.agency.viewmodel.user.UsersViewModel
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.view.AbstractFragment
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * The screen that holds all travelers.
 */
class UsersFragment : AbstractFragment<UsersViewModel, FragmentUsersBinding>() {

    private lateinit var users: Flow<User> // There is NO un-collect in Flow, so let's cache the flow to overwrite it

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)

        // Setup RecyclerView
        val layoutManager = LinearLayoutManager(context)
        dataBinding.rvUserList.layoutManager = layoutManager
        dataBinding.rvUserList.adapter = UserAdapter(requireContext(), viewModel)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchTraveler("")
        viewModel.showMessage.observe(viewLifecycleOwner, {
            it.consume { message -> showMessage(message) }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.users_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchViewMenuItem = menu.findItem(R.id.search_menu)
        val searchView = searchViewMenuItem.actionView as SearchView
        searchView.queryHint =
            resources.getString(R.string.traveler_fragment__search_travel__menu_hint)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { searchTraveler(it) }

                // FIXME: SearchView is not closing :(
                //  Found below solution (not working) on https://stackoverflow.com/questions/23072123/actionbar-search-will-not-close-after-search
                searchView.clearFocus()
                searchView.isIconified = true
                searchViewMenuItem.collapseActionView()
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                // Do nothing
                return false
            }
        })
        searchView.setOnCloseListener {
            searchTraveler("")
            false
        }
    }


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Handle searching for traveler
     */
    private fun searchTraveler(name: String) {
        val adapter = dataBinding.rvUserList.adapter as UserAdapter
        adapter.list.clear()
        adapter.notifyDataSetChanged()

        lifecycleScope.launch {
            users = viewModel.getUsers(name)
            users.collect {
                adapter.updateData(it)
            }
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(dataBinding.rvUserList, message, Snackbar.LENGTH_LONG).show()
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_users

    override fun getViewModelClass(): Class<UsersViewModel> =
        UsersViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}