/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object of the [User].
 */
@Dao
internal interface UserDao : CrudDao<User> {

    /**
     * Retrieve a [User] from the given user ID.
     *
     * @param userId Unique identifier of the [User].
     */
    @Query("SELECT * FROM tbl_user WHERE pk_user_id = :userId LIMIT 1")
    suspend fun getUser(userId: String): User

    /**
     * Retrieve and searches a stream [Travel] with the given [userTypes] and possible name.
     *
     * @param nameQuery The name to be searched for.
     * @param userTypes The user types.
     * @return The found stream [Travel].
     */
    @Query("SELECT * FROM tbl_user WHERE user_type IN (:userTypes) AND UPPER(name) LIKE '%'||UPPER(:nameQuery)||'%'")
    fun getUsers(nameQuery: String, vararg userTypes: UserType): Flow<User>
}