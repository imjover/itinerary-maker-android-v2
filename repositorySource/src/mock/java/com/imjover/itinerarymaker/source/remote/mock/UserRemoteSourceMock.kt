/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.mock

import com.imjover.itinerarymaker.common.di.ApplicationType
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.exception.RemoteAccessException
import com.imjover.itinerarymaker.source.remote.UserRemoteSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

private val USER_NOT_FOUND_EXCEPTION = RemoteAccessException("User not found")

internal class UserRemoteSourceMock : UserRemoteSource {

    private var loggedInUser: User? = null

    companion object {
        val USER_ADMIN = User(
            "0", "admin", "Jover", "Magdaleno", UserType.ADMIN
        )
        val TRAVELER_1 = User(
            "1", "user", "John Oliver", "Magdaleno", UserType.TRAVELER
        )
        val AGENT_1 = User(
            "2", "agent", "Kim", "Mendoza", UserType.AGENT
        )
        val ACCOUNTANT_1 = User(
            "3", "money", "Lyndon", "Mendoza", UserType.ACCOUNTANT
        )
        val TRAVELER_2 = User(
            "4", "wilma.d", "Wilma", "Duguran", UserType.TRAVELER
        )
        val TRAVELER_3 = User(
            "5", "yowi.l", "Yowi", "Luwalhati", UserType.TRAVELER
        )
    }

    private val allUsers = mapOf(
        Pair(USER_ADMIN.username, "admin") to USER_ADMIN,
        Pair(TRAVELER_1.username, "user") to TRAVELER_1,
        Pair(AGENT_1.username, "agent") to AGENT_1,
        Pair(ACCOUNTANT_1.username, "money") to ACCOUNTANT_1,
        Pair(TRAVELER_2.username, "password") to TRAVELER_2,
        Pair(TRAVELER_3.username, "password") to TRAVELER_3
    )

    @Throws(RemoteAccessException::class)
    override suspend fun findUserByLogin(
        username: String,
        password: String,
        applicationType: ApplicationType
    ): User {
        if (loggedInUser != null) {
            throw IllegalStateException()
        }
        delay(MockConfig.DELAY_TIME)
        val credential = Pair(username, password)
        if (allUsers.containsKey(credential)) {
            val candidate = allUsers.getValue(credential)
            val userIsValid =
                when (applicationType) {
                    ApplicationType.AppAgency -> candidate.userType in listOf(
                        UserType.ADMIN,
                        UserType.AGENT,
                        UserType.ACCOUNTANT
                    )
                    ApplicationType.AppTraveler -> candidate.userType == UserType.TRAVELER
                }
            if (!userIsValid) {
                // User is NOT allowed to use this app.
                throw USER_NOT_FOUND_EXCEPTION
            }
            loggedInUser = candidate
            return candidate
        } else {
            throw USER_NOT_FOUND_EXCEPTION
        }
    }

    override suspend fun logout() {
        if (loggedInUser == null) {
            throw IllegalStateException()
        }
        loggedInUser = null
    }

    override fun getLoggedInUser(): User =
        loggedInUser!!

    override suspend fun getUsers(nameQuery: String, vararg userTypes: UserType): List<User> =
        withContext(Dispatchers.IO) {
            val query = nameQuery.toUpperCase()
            return@withContext allUsers.filterValues {
                userTypes.contains(it.userType) &&
                        (it.firstName.toUpperCase().contains(query) ||
                                it.lastName.toUpperCase().contains(query))
            }.values.toList()
        }
}