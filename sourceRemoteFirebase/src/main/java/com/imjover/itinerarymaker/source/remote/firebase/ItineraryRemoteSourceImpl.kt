/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.firebase

import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.source.remote.ItineraryRemoteSource


class ItineraryRemoteSourceImpl : ItineraryRemoteSource {

    override suspend fun insert(entity: Itinerary): String {
        TODO("Not yet implemented")
    }

    override suspend fun update(entity: Itinerary) {
        TODO("Not yet implemented")
    }

    override suspend fun delete(entity: Itinerary) {
        TODO("Not yet implemented")
    }

    override suspend fun getListChildrenOf(parentId: String): List<Itinerary> {
        TODO("Not yet implemented")
    }
}