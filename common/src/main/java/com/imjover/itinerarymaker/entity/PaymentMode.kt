/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * The payment.
 *
 * @param id The unique ID.
 */
enum class PaymentMode(val id: String) {

    CASH("cash"),
    BANK_TRANSFER("bank"),
    CREDIT_CARD("creditcard");


    companion object {
        /**
         * Retrieves the equivalent [PaymentMode] base on the given ID.
         *
         * @param id The id of the [PaymentMode].
         * @return The retrieved [PaymentMode].
         */
        fun fromId(id: String): PaymentMode {
            values().forEach {
                if (it.id == id) {
                    return it
                }
            }
            throw IllegalStateException()
        }
    }
}