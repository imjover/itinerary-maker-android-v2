/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.di

import com.imjover.itinerarymaker.source.RemoteSource
import com.imjover.itinerarymaker.source.remote.RemoteSourceFirebase
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the remote implementation of [RemoteSource] that uses [Firebase].
 */
@Module
abstract class RemoteSourceFirebaseModule {

    @Singleton
    @Binds
    internal abstract fun bindRemoteSource(remoteSource: RemoteSourceFirebase): RemoteSource
}