/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view

/**
 * The options when opening a form.
 */
enum class FormOption {
    /**
     * Choose to insert a new record.
     */
    ADD,

    /**
     * Choose to update an existing record.
     */
    EDIT
}