/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.imjover.itinerarymaker.view.AbstractActivity
import com.imjover.itinerarymaker.viewmodel.login.BaseLoginViewModel


/**
 * The login screen.
 */
abstract class BaseLoginActivity<VM : BaseLoginViewModel, DB : ViewDataBinding> :
    AbstractActivity<VM, DB>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Setup ViewModel
        viewModel.isLoggedIn.observe(this, Observer {
            if (it) {
                openNextActivity()
            }
        })
        viewModel.errorMessage.observe(this, Observer {
            showMessage(it)
        })
    }

    private fun openNextActivity() {
        val intent = Intent(this, getClassOfNextActivity())
        startActivity(intent)
        finish()
    }


    /***********************************************************************************************
     * Abstract functions.
     **********************************************************************************************/

    /**
     * Retrieves the intent for the next activity.
     *
     * @return The intent.
     */
    abstract fun getClassOfNextActivity(): Class<Activity>

    /**
     * Shows message in the UI.
     */
    abstract fun showMessage(message: String)
}