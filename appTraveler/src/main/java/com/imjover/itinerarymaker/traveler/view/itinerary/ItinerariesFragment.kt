/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.itinerary

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.entity.Status
import com.imjover.itinerarymaker.traveler.R
import com.imjover.itinerarymaker.traveler.databinding.FragmentItinerariesBinding
import com.imjover.itinerarymaker.traveler.viewmodel.itinerary.ItinerariesViewModel
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.itinerary.AbstractItinerariesFragment
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


/**
 * The screen that holds all itineraries.
 */
class ItinerariesFragment :
    AbstractItinerariesFragment<ItinerariesViewModel, FragmentItinerariesBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get the arguments passed from previous screen
        val args = ItinerariesFragmentArgs.fromBundle(requireArguments())
        viewModel.setParentIds(args.parentIds)
        // Update Activity's title
        jobGetTravel = lifecycleScope.launch {
            viewModel.getTravel().collect {
                // FIXME: Activity's title is NOT updated sometimes (usually starting 2nd attempt).
                // Why not pass the name directly instead of just ID? then manually update the title bar
                requireActivity().title = it.name
                dataBinding.fabSubmitReview.visibility =
                    if (it.status == Status.FOR_REVIEW) View.VISIBLE
                    else View.GONE
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        dataBinding.fabSubmitReview.setOnClickListener {
            val action =
                ItinerariesFragmentDirections.actionItinerariesFragmentToReviewTravelDialog(
                    arrayOf(viewModel.getParentIds()[0]),
                    viewModel.getParentIds()[1],
                )
            findNavController().navigate(action)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.itineraries_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.invoice_menu -> {
                generateInvoice()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_itineraries

    override fun getViewModelClass(): Class<ItinerariesViewModel> =
        ItinerariesViewModel::class.java

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }

    override fun getRecyclerView(): RecyclerView =
        dataBinding.rvItineraryList

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout =
        dataBinding.swipeContainer

    override fun getAdapter(): UpdatableRecyclerViewAdapter<Itinerary, *> =
        ItineraryAdapter(viewModel.getParentIds())
}