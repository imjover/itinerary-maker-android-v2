/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.destination.internal

import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.DestinationType
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.entity.TransportationMode
import com.imjover.itinerarymaker.manager.DestinationManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.repository.DestinationRepository
import com.imjover.itinerarymaker.repository.di.RepositoryFactory
import com.imjover.itinerarymaker.util.IdGenerator
import com.imjover.itinerarymaker.util.Result
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject


internal class DestinationManagerImpl @Inject constructor() : DestinationManager {

    @Inject
    lateinit var userManager: UserManager

    private val destinationRepository: DestinationRepository =
        RepositoryFactory.singleton().getDestinationRepository()


    /***********************************************************************************************
     * Child-related methods.
     **********************************************************************************************/

    override suspend fun getListChildrenOf(parentIds: Array<String>): List<Destination> =
        destinationRepository.getListChildrenOf(parentIds)


    /***********************************************************************************************
     * Syncable-related methods.
     **********************************************************************************************/

    override fun getStreamChildrenOf(parentIds: Array<String>): Flow<Destination> =
        destinationRepository.getStreamChildrenOf(parentIds)

    override suspend fun syncChildrenOf(parentIds: Array<String>) {
        destinationRepository.syncChildrenOf(parentIds)
    }

    /***********************************************************************************************
     * CRUD methods.
     **********************************************************************************************/

    override suspend fun getDestination(parentIds: Array<String>, id: String): Destination =
        destinationRepository.getCached(parentIds, id)

    override suspend fun insert(
        parentIds: Array<String>,
        description: String,
        timestamp: Date,
        destinationType: DestinationType,
        transportationMode: TransportationMode,
        latitude: Double,
        longitude: Double
    ): String {
        val loggedInUser = userManager.getLoggedInUser().id
        val destination = Destination(
            IdGenerator.generate("DLM", loggedInUser),
            description,
            timestamp,
            destinationType,
            transportationMode,
            latitude,
            longitude,
            false,
            Date(),
            loggedInUser,
            SyncStatus.TO_INSERT
        )
        return destinationRepository.insert(parentIds, destination)
    }

    override suspend fun update(
        parentIds: Array<String>,
        oldDestination: Destination,
        description: String,
        timestamp: Date,
        destinationType: DestinationType,
        transportationMode: TransportationMode,
        latitude: Double,
        longitude: Double
    ) {
        val loggedInUser = userManager.getLoggedInUser().id
        val destination = Destination(
            oldDestination.id,
            description,
            timestamp,
            destinationType,
            transportationMode,
            latitude,
            longitude,
            oldDestination.isVisited,
            Date(),
            loggedInUser,
            SyncStatus.TO_UPDATE
        )
        destinationRepository.update(parentIds, destination)
    }

    override suspend fun delete(parentIds: Array<String>, vararg destinations: Destination) {
        destinationRepository.delete(parentIds, *destinations)
    }


    /***********************************************************************************************
     * Other methods.
     **********************************************************************************************/

    override suspend fun updateIsVisited(
        parentIds: Array<String>,
        id: String,
        isVisited: Boolean
    ): Result<Unit> =
        destinationRepository.updateIsVisited(parentIds, id, isVisited)
}