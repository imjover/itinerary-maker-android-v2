/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.destination

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.manager.DestinationManager
import com.imjover.itinerarymaker.view.destination.AbstractDestinationListFragment
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import com.imjover.itinerarymaker.viewmodel.InjectableViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [AbstractDestinationListFragment].
 */
abstract class AbstractDestinationListViewModel : AbstractViewModel(), InjectableViewModel {

    @Inject lateinit var destinationManager: DestinationManager
    internal lateinit var parentIds: Array<String>

    val isRefreshing = ObservableBoolean()

    init {
        injectDependencies()
    }

    /**
     * Retrieves the [Destination]s of a given [User].
     *
     * @return Stream of [Destination]s.
     */
    fun getDestinations(): Flow<Destination> =
        destinationManager.getStreamChildrenOf(parentIds)

    /**
     * Refresh the stream of destinations.
     */
    fun refreshDestinations() {
        viewModelScope.launch {
            isRefreshing.set(true)
            destinationManager.syncChildrenOf(parentIds)
            isRefreshing.set(false)
        }
    }
}