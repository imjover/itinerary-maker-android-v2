/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * Defines that an [Entity] can be synced in both remote and local sources.
 * Also is a child and has a parent (relationship).
 */
interface SyncableChildEntity : SyncableEntity, ChildEntity