/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.splash

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.ActivitySplashBinding
import com.imjover.itinerarymaker.view.AbstractActivity
import com.imjover.itinerarymaker.viewmodel.splash.SplashProgress
import com.imjover.itinerarymaker.viewmodel.splash.SplashViewModel


/**
 * The first screen to launch in the app.
 */
abstract class AbstractSplashActivity : AbstractActivity<SplashViewModel, ActivitySplashBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.splashProgress.observe(
            this,
            Observer { splashProgress ->
                when (splashProgress) {
                    SplashProgress.STARTED -> {
                        dataBinding.tvProgressMessage.text =
                            getString(R.string.splash_activity__progress__finalizing)
                    }
                    SplashProgress.ENDED -> doSignIn()
                    else -> {
                        // do nothing
                    }
                }
            }
        )
        viewModel.startBackgroundProcesses()
    }


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Do final sign-in steps before closing this splash activity.
     */
    private fun doSignIn() {
        val intent = Intent(this, getClassOfNextActivity())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                dataBinding.logo,
                getString(R.string.shared_transition_name__app_logo)
            )
            startActivity(intent, options.toBundle())
        } else {
            startActivity(intent)
        }
        finish()
    }


    /***********************************************************************************************
     * Abstract functions.
     **********************************************************************************************/

    /**
     * Retrieves the intent for the next activity.
     *
     * @return The intent.
     */
    abstract fun getClassOfNextActivity(): Class<Activity>


    /***********************************************************************************************
     * Required functions of [AbstractActivity].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.activity_splash

    override fun getViewModelClass(): Class<SplashViewModel> =
        SplashViewModel::class.java

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }
}
