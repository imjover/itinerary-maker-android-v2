/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.itinerary

import android.os.Build
import android.os.Environment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.helper.ReportFormat
import com.imjover.itinerarymaker.manager.*
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.util.PdfWriterUtil
import com.imjover.itinerarymaker.viewmodel.AbstractSyncableViewModel
import com.imjover.itinerarymaker.viewmodel.InjectableViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


private const val DOWNLOAD_PDF_ROOT_FOLDER = "itineraries"

/**
 * The view model of [ItinerariesFragment].
 */
abstract class AbstractItinerariesViewModel : AbstractSyncableViewModel<Itinerary>(),
    InjectableViewModel {

    @Inject
    lateinit var userManager: UserManager
    @Inject
    lateinit var travelManager: TravelManager
    @Inject
    lateinit var itineraryManager: ItineraryManager
    @Inject
    lateinit var destinationManager: DestinationManager
    @Inject
    lateinit var expenseManager: ExpenseManager

    private lateinit var parentIds: Array<String>

    protected val _showMessage: MutableLiveData<Consumable<String>> = MutableLiveData()
    val showMessage: LiveData<Consumable<String>> get() = _showMessage

    private val _dialogMessage: MutableLiveData<Int> = MutableLiveData()
    val dialogMessage: LiveData<Int> get() = _dialogMessage

    private val _dialogDismiss: MutableLiveData<Boolean> = MutableLiveData()
    val dialogDismiss: LiveData<Boolean> get() = _dialogDismiss

    private val _showPDF: MutableLiveData<Consumable<String>> = MutableLiveData()
    val showPDF: LiveData<Consumable<String>> get() = _showPDF

    private lateinit var job: Job

    init {
        injectDependencies()
    }

    /**
     * Sets the [Travel] for caching.
     *
     * @param parentIds The unique ID of the travel.
     */
    fun setParentIds(parentIds: Array<String>) {
        this.parentIds = parentIds
    }

    /**
     * Retrieves the stream of [Travel].
     */
    fun getTravel(): Flow<Travel> {
        return travelManager.getTravelAsStream(
            parentIds.dropLast(1).toTypedArray(),
            parentIds.last()
        )
    }

    /**
     * Retrieves the parent user id.
     */
    fun getParentUserId(): Array<String> = arrayOf(parentIds[0])

    /**
     * Retrieves the parent travel id.
     */
    fun getParentTravelId(): String = parentIds[1]


    /**
     * Generates the invoice
     */
    fun generateInvoice() {
        _dialogDismiss.value = false
        job = viewModelScope.launch {
            try {
                _dialogMessage.value =
                    R.string.itinerary_fragment__generate_invoice__syncing__message
                // 1. Sync and retrieve ALL data to ensure that EXPENSES are updated
                val allTravelData = mutableMapOf<Itinerary, Map<Destination, List<Expense>>>()
                // 1.1 Itinerary: retrieve
                val itineraries = itineraryManager.getListChildrenOf(getParentIds())
                itineraries.forEach root@{ itinerary ->

                    // FIXME: Reimplement
                    if (!isActive) {
                        return@root // Handle cancellation. Though it seems this is NOT working, lols
                    }
                    // 1.2 Destination: sync then retrieve
                    val destinationMap = mutableMapOf<Destination, List<Expense>>()
                    val destinationParents = getParentIds().plus(itinerary.id)
                    destinationManager.syncChildrenOf(destinationParents)
                    if (!isActive) {
                        return@root // Handle cancellation. Though it seems this is NOT working, lols
                    }
                    val destinations = destinationManager.getListChildrenOf(destinationParents)
                    destinations.forEach { destination ->

                        if (!isActive) {
                            return@root // Handle cancellation. Though it seems this is NOT working, lols
                        }
                        val expenseParents = destinationParents.plus(destination.id)
                        // 1.3 Expense: sync then retrieve
                        expenseManager.syncChildrenOf(expenseParents)
                        if (!isActive) {
                            return@root // Handle cancellation. Though it seems this is NOT working, lols
                        }
                        val expenses = expenseManager.getListChildrenOf(expenseParents)
                        destinationMap[destination] = expenses
                    }
                    allTravelData[itinerary] = destinationMap
                }

                // Get travel title
                val travel = travelManager.getTravel(
                    getParentIds().dropLast(1).toTypedArray(),
                    getParentIds().last()
                )

                // Get user info
                val traveler: User? = userManager.getUser(getParentIds().first()).getOrNull()

                // 2. Generate invoice to PDF.
                // This transaction is NOT cancellable anymore at this point.
                // TODO : Disable cancel button
                _dialogMessage.value =
                    R.string.itinerary_fragment__generate_invoice__generating__message
                val formattedInvoice = ReportFormat.invoiceToPDF(traveler, travel, allTravelData)
                writePdf(formattedInvoice.toString())

                // 3. Export PDF
                _showMessage.value =
                    Consumable(
                        AppComponent.appContext.getString(
                            R.string.itinerary_fragment__generate_invoice__done__message
                        )
                    )
            } finally {
                if (!isActive) {
                    // If cancelled, then show inform user
                    _showMessage.value =
                        Consumable(
                            AppComponent.appContext.getString(
                                R.string.itinerary_fragment__generate_invoice__cancelled__message
                            )
                        )
                }
                _dialogDismiss.value = true
            }
        }
    }

    /**
     * Attempts to cancel the generation of invoice.
     */
    fun tryToCancelGeneration() {
        _dialogMessage.value = R.string.itinerary_fragment__generate_invoice__cancelling__message
        job.cancel()
    }


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    private fun writePdf(content: String) {
        val pdfFolder: File =
            if (Build.VERSION.SDK_INT >= 19) {
                File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
                    DOWNLOAD_PDF_ROOT_FOLDER
                )
            } else {
                File(
                    Environment.getExternalStorageDirectory().absolutePath + "/Documents",
                    DOWNLOAD_PDF_ROOT_FOLDER
                );
            }
        if (!pdfFolder.exists()) {
            pdfFolder.mkdirs()
        }
        val timestamp = SimpleDateFormat("yyyyMMddhhmm").format(Date())
        val fileName = "invoice_${getParentIds().last()}_$timestamp.pdf"
        val filePath = File(pdfFolder.absolutePath, fileName)
        PdfWriterUtil.write(
            filePath,
            AppComponent.appContext.resources.getDrawable(R.drawable.logo_rayton),
            content
        )
        _showPDF.value = Consumable(filePath.absolutePath)
    }


    /***********************************************************************************************
     * Required functions of [AbstractSyncableViewModel].
     **********************************************************************************************/

    override fun getParentIds(): Array<String> =
        parentIds

    override fun getSyncableManager(): SyncableManager<Itinerary> =
        itineraryManager
}