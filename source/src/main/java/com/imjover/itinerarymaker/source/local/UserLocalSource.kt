/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local

import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import kotlinx.coroutines.flow.Flow

/**
 * Represents the local source of [User].
 */
interface UserLocalSource {

    /**
     * Inserts or updates the cache of [users].
     */
    suspend fun insertOrUpdate(vararg users: User)

    /**
     * Retrieves all [User]s with the given [UserType].
     *
     * @param nameQuery The name to be searched for.
     * @param userTypes The user types.
     * @return Stream of [User]s.
     */
    fun getUsers(nameQuery: String, vararg userTypes: UserType): Flow<User>
}