/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.imjover.itinerarymaker.view.login.BaseLoginActivity
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import com.imjover.itinerarymaker.viewmodel.InjectableViewModel


/**
 * The view model of [BaseLoginActivity].
 */
abstract class BaseLoginViewModel : AbstractViewModel(), InjectableViewModel {

    protected val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> get() = _errorMessage

    protected val _isLoggedIn = MutableLiveData<Boolean>()
    val isLoggedIn: LiveData<Boolean> get() = _isLoggedIn
}