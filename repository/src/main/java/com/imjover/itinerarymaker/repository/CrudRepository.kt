/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository

import com.imjover.itinerarymaker.entity.Entity
import kotlinx.coroutines.flow.Flow


/**
 * Defines repositories that can do CRUD (create, read, update, delete), but except READING.
 * Still named as CRUD because CUD sounds awful ;)
 */
interface CrudRepository<E : Entity> {

    /**
     * Retrieves a locally cached [Entity] with the given [id] as primary key.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Entity].
     * @return The found [Entity].
     */
    suspend fun getCached(parentIds: Array<String>, id: String): E

    /**
     * Retrieves a locally cached [Entity] (as stream) with the given [id] as primary key.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Entity].
     * @return The found stream [Entity].
     */
    fun getCachedAsStream(parentIds: Array<String>, id: String): Flow<E>

    /**
     * Inserts a new record [Entity].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param entity The [Entity] to be inserted.
     * @return The ID of the [Entity].
     */
    suspend fun insert(parentIds: Array<String>, entity: E): String

    /**
     * Updates the record of [Entity].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param entity The [Entity] to be updated.
     */
    suspend fun update(parentIds: Array<String>, entity: E)

    /**
     * Deletes the records of [Entity]s.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param entities The [Entity]s to be deleted.
     */
    suspend fun delete(parentIds: Array<String>, vararg entities: E)
}