/*
 * Copyright (c) 2021. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.content.FileProvider
import java.io.File
import java.io.FileWriter
import java.io.IOException


/**
 * Utility class to handle writing of CSV files.
 */
class CsvWriterUtil {

    companion object {

        /**
         * Writes a CSV file.
         *
         * @param file The path + filename where the CSV file will be saved (written).
         * @param formattedCSV The content of the formatted CSV.
         */
        fun write(file: File, formattedCSV: String) {
            if (file.exists()) {
                // Delete the file so we can create new one
                file.delete()
            }
            if (!file.createNewFile()) {
                throw IOException("Failed to generate PDF file at: ${file.absolutePath}")
            }

            try {
                FileWriter(file).apply {
                    write(formattedCSV)
                    close()
                }
            } catch (ex: Exception) {
                throw IOException(ex)
            }
        }

        /**
         * Retrieves the intent of a CSV viewer.
         *
         * @param context The context.
         * @param path The path to the file to be viewed.
         */
        fun getCsvViewer(context: Context, path: String): Intent {
            val path: Uri = FileProvider.getUriForFile(
                context,
                context.applicationContext.packageName + ".provider",
                File(path)
            )
            // Setting the intent for csv reader
            val csvIntent = Intent(Intent.ACTION_VIEW)
            csvIntent.setDataAndType(path, "text/csv")
            csvIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            return csvIntent
        }
    }
}