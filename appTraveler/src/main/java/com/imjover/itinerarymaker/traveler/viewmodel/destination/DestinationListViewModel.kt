/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.viewmodel.destination

import com.imjover.itinerarymaker.traveler.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.traveler.view.destination.DestinationListFragment
import com.imjover.itinerarymaker.viewmodel.destination.AbstractDestinationListViewModel

/**
 * The view model of [DestinationListFragment].
 */
class DestinationListViewModel : AbstractDestinationListViewModel() {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }
}