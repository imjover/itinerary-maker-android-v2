/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * The types of destination.
 *
 * @param id The unique ID.
 */
enum class DestinationType(val id: String) {

    DEFAULT(""),
    HOTEL("hotel"),
    AIRPORT("airport"),
    RESTAURANT("restau"),
    CAFE("cafe"),
    BAR("bar"),
    MALL("mall"),
    MUSEUM("museum");


    companion object {
        /**
         * Retrieves the equivalent [DestinationType] base on the given ID.
         *
         * @param id The id of the [DestinationType].
         * @return The retrieved [DestinationType].
         */
        fun fromId(id: String): DestinationType {
            values().forEach {
                if (it.id == id) {
                    return it
                }
            }
            throw IllegalStateException()
        }
    }
}