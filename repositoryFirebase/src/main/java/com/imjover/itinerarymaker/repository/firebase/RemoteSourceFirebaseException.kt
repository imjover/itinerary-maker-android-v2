/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase

import com.google.firebase.FirebaseException

/**
 * Custom Firebase remote exception.
 */
class RemoteSourceFirebaseException(msg: String) : FirebaseException(msg)