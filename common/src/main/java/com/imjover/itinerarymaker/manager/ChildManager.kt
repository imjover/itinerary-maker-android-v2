/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager

import com.imjover.itinerarymaker.entity.Entity

/**
 * Defines managers that has parents.
 */
interface ChildManager<E: Entity> {

    /**
     * Retrieves all children of a given [parentIds].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     */
    suspend fun getListChildrenOf(parentIds: Array<String>): List<E>
}