/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


/**
 * The travel of a [User].
 *
 * @param id The unique ID and primary key.
 * @param agent The agent that manages this record.
 * @param name The label to be used for this record.
 * @param startDate The date when the travel should start.
 * @param endDate The date when the travel should have ended.
 * @param remarks Additional remarks.
 * @param status The status of this entity.
 * @param statusReason The reasons for the status change.
 * @param paymentMode The payment mode used by the customer to pay this travel.
 * @param lastUpdated The most recent date this record was updated.
 * @param lastUpdatedBy The most recent user who updated this record.
 * @param syncStatus The current synchronization state of this record.
 */
@Entity(
    tableName = "tbl_travel",
)
data class Travel(
    @PrimaryKey @ColumnInfo(name = "pk_travel_id") val id: String,
    @ColumnInfo(name = "fk_agent") val agent: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "start_date") val startDate: Date,
    @ColumnInfo(name = "end_date") val endDate: Date,
    @ColumnInfo(name = "remarks") val remarks: String?,
    @ColumnInfo(name = "status") val status: Status,
    @ColumnInfo(name = "status_reason") val statusReason: String?,
    @ColumnInfo(name = "payment_mode") val paymentMode: PaymentMode?,
    // Audit Trail
    @ColumnInfo(name = "last_updated") val lastUpdated: Date,
    @ColumnInfo(name = "fk_last_updated_by") val lastUpdatedBy: String,
    // Syncable
    @ColumnInfo(name = "sync_status") val syncStatus: SyncStatus
) : SyncableEntity {

    override fun getUniqueId(): String =
        id

    override fun getSyncableStatus(): SyncStatus =
        syncStatus

    override fun cloneAndSetArg(newSyncStatus: SyncStatus): Travel =
        cloneAndSetArg(this.id, newSyncStatus)

    override fun cloneAndSetArg(newId: String, newSyncStatus: SyncStatus): Travel =
        Travel(
            newId,
            this.agent,
            this.name,
            this.startDate,
            this.endDate,
            this.remarks,
            this.status,
            this.statusReason,
            this.paymentMode,
            this.lastUpdated,
            this.lastUpdatedBy,
            newSyncStatus
        )
}