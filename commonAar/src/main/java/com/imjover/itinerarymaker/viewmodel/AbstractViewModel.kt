/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel

import androidx.lifecycle.ViewModel


/**
 * The base class of view models.
 */
abstract class AbstractViewModel : ViewModel()