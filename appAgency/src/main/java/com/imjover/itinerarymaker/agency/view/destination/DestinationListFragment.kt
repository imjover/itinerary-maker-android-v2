/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.destination

import android.os.Bundle
import com.imjover.itinerarymaker.agency.viewmodel.destination.DestinationListViewModel
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.destination.ARG_PARENT_IDS
import com.imjover.itinerarymaker.view.destination.AbstractDestinationListFragment
import com.imjover.itinerarymaker.viewmodel.destination.AbstractDestinationListViewModel


/**
 * The screen that holds the LIST representation of all destinations.
 */
class DestinationListFragment : AbstractDestinationListFragment() {

    companion object {
        /**
         * Creates an instance of this fragment.
         *
         * @param parentIds The parent [Itinerary] of the destination to be displayed.
         */
        fun newInstance(parentIds: Array<String>) =
            DestinationListFragment().apply {
                arguments = Bundle().apply {
                    putStringArray(ARG_PARENT_IDS, parentIds)
                }
            }
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getViewModelClass(): Class<AbstractDestinationListViewModel> =
        DestinationListViewModel::class.java as Class<AbstractDestinationListViewModel>
}