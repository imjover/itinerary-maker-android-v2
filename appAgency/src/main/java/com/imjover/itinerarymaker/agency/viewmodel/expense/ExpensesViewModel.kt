/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.expense

import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.expense.ExpensesFragment
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.manager.ExpenseManager
import com.imjover.itinerarymaker.manager.SyncableManager
import com.imjover.itinerarymaker.viewmodel.AbstractSyncableViewModel
import javax.inject.Inject


/**
 * The view model of [ExpensesFragment].
 */
class ExpensesViewModel : AbstractSyncableViewModel<Expense>() {

    @Inject lateinit var expenseManager: ExpenseManager
    internal lateinit var parentIds: Array<String>

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    override fun getParentIds(): Array<String> =
        parentIds

    override fun getSyncableManager(): SyncableManager<Expense> =
        expenseManager
}