/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository

import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.entity.Status
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.util.Result
import java.util.*


/**
 * Represents the repository data source of [Travel].
 */
interface TravelRepository : CrudRepository<Travel>, SyncableRepository<Travel> {


    /**
     * Retrieve the status of a [Travel], stored locally as cache.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The ID of this record.
     * @return The status of the [Travel].
     */
    suspend fun getCachedStatus(parentIds: Array<String>, travelId: String): Status

    /**
     * Cancels the travel.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The ID of the travel.
     */
    suspend fun cancel(parentIds: Array<String>, travelId: String)

    /**
     * Updates the [paymentMode] of the travel.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The ID of this record.
     * @param paymentMode The payment mode.
     * @param lastUpdatedBy The user who updated this record.
     * @param lastUpdated The date this record was updated.
     */
    suspend fun markAsPaid(
        parentIds: Array<String>,
        travelId: String,
        paymentMode: PaymentMode,
        lastUpdatedBy: String,
        lastUpdated: Date
    )


    /***********************************************************************************************
     * Review-related methods.
     **********************************************************************************************/

    /**
     * Submits the travel to be reviewed.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The ID of the travel.
     * @return The response message.
     */
    suspend fun submitForReview(parentIds: Array<String>, travelId: String): Result<String>

    /**
     * Submits the review on the travel.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The unique ID of the [Travel].
     * @param isApprove Whether the review is approve, then true, else false.
     * @param reason The reasons for the status change.
     * @return The response message.
     */
    suspend fun submitReview(
        parentIds: Array<String>,
        travelId: String,
        isApprove: Boolean,
        reason: String?
    ): Result<String>
}