/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.travel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.FragmentTravelsBinding
import com.imjover.itinerarymaker.agency.viewmodel.travel.TravelsViewModel
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.view.AbstractRefreshableFragment
import com.imjover.itinerarymaker.view.FormOption
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter


/**
 * The screen that holds all travels.
 */
class TravelsFragment :
    AbstractRefreshableFragment<Travel, TravelsViewModel, FragmentTravelsBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)

        // Get the arguments passed from previous screen
        val args = TravelsFragmentArgs.fromBundle(requireArguments())
        viewModel.userId = args.userId
        // Update Activity's title
        requireActivity().title = args.name

        // Setup UI
        if (isUserAllowedToEdit()) {
            dataBinding.btnAddTravel.setOnClickListener {
                val action =
                    TravelsFragmentDirections.actionTravelsFragmentToTravelFormFragment(
                        FormOption.ADD,
                        null,
                        arrayOf(viewModel.userId)
                    )
                findNavController().navigate(action)
            }
        } else {
            dataBinding.btnAddTravel.visibility = View.GONE
        }

        return view
    }

    /**
     * Checks whether the current logged-in user is allowed to edit
     */
    private fun isUserAllowedToEdit() =
        listOf(UserType.ADMIN, UserType.AGENT).contains(viewModel.getLoggedInUser().userType)


    /***********************************************************************************************
     * Required functions of [AbstractRefreshableFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_travels

    override fun getViewModelClass(): Class<TravelsViewModel> =
        TravelsViewModel::class.java

    override fun bindLayoutAndViewModel() {
        // do nothing
    }

    override fun getRecyclerView(): RecyclerView =
        dataBinding.rvTravelList

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout =
        dataBinding.swipeContainer

    override fun getAdapter(): UpdatableRecyclerViewAdapter<Travel, *> =
        TravelAdapter(viewModel.getParentIds(), isUserAllowedToEdit())
}