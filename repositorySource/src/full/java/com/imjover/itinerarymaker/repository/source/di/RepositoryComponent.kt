/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source.di

import com.imjover.itinerarymaker.repository.source.RepositoryImpl
import com.imjover.itinerarymaker.source.local.di.LocalSourceRoomModule
import com.imjover.itinerarymaker.source.remote.di.RemoteSourceFirebaseModule
import dagger.Component
import javax.inject.Singleton

/**
 * Injects the required modules of the [DataSourceLocal].
 */
@Singleton
@Component(modules = [LocalSourceRoomModule::class, RemoteSourceFirebaseModule::class])
internal interface RepositoryComponent {

    fun inject(repository: RepositoryImpl)
}