/*
 * Copyright (c) 2021. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase.internal

import com.google.firebase.functions.FirebaseFunctions
import com.imjover.itinerarymaker.entity.ExpenseType
import com.imjover.itinerarymaker.repository.ReportRepository
import com.imjover.itinerarymaker.util.Result
import java.math.BigDecimal
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class ReportRepositoryImpl(
    private val functions: FirebaseFunctions
) : ReportRepository {

    override suspend fun getSalesReport(
        startDate: Date,
        endDate: Date
    ): Result<Map<ExpenseType, ArrayList<BigDecimal>>> {
        val data: MutableMap<String, Any> = HashMap()
        data["startDate"] = startDate.time
        data["endDate"] = endDate.time

        return suspendCoroutine { cont ->
            functions
                .getHttpsCallable("reports-getSalesReport")
                .call(data)
                .continueWith { task ->
                    task.result?.data as HashMap<String, ArrayList<Double>>
                }
                .addOnSuccessListener {
                    val reportData = mutableMapOf<ExpenseType, ArrayList<BigDecimal>>()
                    it.forEach { rawData ->
                        val bigDecimals = arrayListOf<BigDecimal>()
                        rawData.value.forEach { double ->
                            bigDecimals.add(BigDecimal(double))
                        }
                        reportData[ExpenseType.fromId(rawData.key)] = bigDecimals
                    }
                    cont.resume(Result.success(reportData))
                }
                .addOnFailureListener {
                    it.logException(toString())
                    cont.resume(Result.failure(it))
                }
        }
    }
}