/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase.internal

import com.google.firebase.Timestamp
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.repository.ItineraryRepository
import kotlinx.coroutines.CoroutineDispatcher
import java.util.*


internal const val ITINERARIES_COLLECTION = "itineraries"

internal class ItineraryRepositoryImpl(
    dispatcher: CoroutineDispatcher,
    private val db: FirebaseFirestore
) : AbstractSyncableCrudRepository<Itinerary>(dispatcher), ItineraryRepository {


    /***********************************************************************************************
     * Abstract functions from AbstractSyncableCrudRepository.
     **********************************************************************************************/

    override fun getEventsCollection(parentIds: Array<String>): CollectionReference {
        if (parentIds.size != 2) {
            throw IllegalArgumentException()
        }
        return db.collection(USERS_COLLECTION)
            .document(parentIds[0])
            .collection(TRAVELS_COLLECTION)
            .document(parentIds[1])
            .collection(ITINERARIES_COLLECTION)
    }

    override fun getOrderBy(): Pair<String, Query.Direction> =
        Pair("name", Query.Direction.ASCENDING)

    override fun MutableMap<String, Any?>.toEntity(id: String, syncStatus: SyncStatus): Itinerary =
        Itinerary(
            id,
            this["name"] as String,
            (this["last_updated"] as Timestamp).toDate(),
            this["fk_last_updated_by"] as String,
            syncStatus
        )

    override fun Itinerary.toHashMap(): HashMap<String, Any?> =
        hashMapOf(
            "name" to name,
            "last_updated" to lastUpdated,
            "fk_last_updated_by" to lastUpdatedBy
        )
}