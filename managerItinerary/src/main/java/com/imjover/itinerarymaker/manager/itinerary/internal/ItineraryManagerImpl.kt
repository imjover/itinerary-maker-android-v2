/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.itinerary.internal

import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.manager.ItineraryManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.repository.ItineraryRepository
import com.imjover.itinerarymaker.repository.di.RepositoryFactory
import com.imjover.itinerarymaker.util.IdGenerator
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject


internal class ItineraryManagerImpl @Inject constructor() : ItineraryManager {

    @Inject
    lateinit var userManager: UserManager

    private val itineraryRepository: ItineraryRepository =
        RepositoryFactory.singleton().getItineraryRepository()


    /***********************************************************************************************
     * Child-related methods.
     **********************************************************************************************/

    override suspend fun getListChildrenOf(parentIds: Array<String>): List<Itinerary> =
        itineraryRepository.getListChildrenOf(parentIds)


    /***********************************************************************************************
     * Syncable-related methods.
     **********************************************************************************************/

    override fun getStreamChildrenOf(parentIds: Array<String>): Flow<Itinerary> =
        itineraryRepository.getStreamChildrenOf(parentIds)

    override suspend fun syncChildrenOf(parentIds: Array<String>) {
        itineraryRepository.syncChildrenOf(parentIds)
    }


    /***********************************************************************************************
     * CRUD methods.
     **********************************************************************************************/

    override suspend fun getItinerary(parentIds: Array<String>, id: String): Itinerary =
        itineraryRepository.getCached(parentIds, id)

    override fun getItineraryAsStream(parentIds: Array<String>, id: String): Flow<Itinerary> =
        itineraryRepository.getCachedAsStream(parentIds, id)

    override suspend fun insert(
        parentIds: Array<String>,
        description: String
    ): String {
        val loggedInUser = userManager.getLoggedInUser().id
        val temporaryId = IdGenerator.generate("ILM", loggedInUser)
        val itinerary = Itinerary(
            temporaryId,
            description,
            Date(),
            loggedInUser,
            SyncStatus.TO_INSERT
        )
        return itineraryRepository.insert(parentIds, itinerary)
    }

    override suspend fun update(
        parentIds: Array<String>,
        oldItinerary: Itinerary,
        description: String
    ) {
        val loggedInUser = userManager.getLoggedInUser().id
        val itinerary = Itinerary(
            oldItinerary.id,
            description,
            Date(),
            loggedInUser,
            SyncStatus.TO_UPDATE
        )
        itineraryRepository.update(parentIds, itinerary)
    }

    override suspend fun delete(parentIds: Array<String>, vararg itineraries: Itinerary) {
        itineraryRepository.delete(parentIds, *itineraries)
    }
}