/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.traveler

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.RowTravelerBinding
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.util.image.CircleTransform
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter
import com.squareup.picasso.Picasso


/**
 * List adapter of [Travel] item.
 */
class TravelerAdapter : UpdatableRecyclerViewAdapter<User, RowTravelerBinding>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterViewHolder<RowTravelerBinding> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = RowTravelerBinding.inflate(layoutInflater, parent, false)
        return AdapterViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: AdapterViewHolder<RowTravelerBinding>, position: Int) {
        val user = getData(position)
        val showDivider = position + 1 < itemCount
        // Bind instances of [Travel] to the row.
        holder.binding.user = user
        holder.binding.divider.visibility = if (showDivider) View.VISIBLE else View.GONE
        Picasso.get()
            .load(user.photoUrl)
            .placeholder(R.drawable.ic_user_24)
            .transform(CircleTransform())
            .into(holder.binding.ivProfilePic)
        holder.binding.container.setOnClickListener {
            val action =
                TravelersFragmentDirections.actionTravelersFragmentToTravelsFragment(
                    user.id,
                    user.name
                )
            it.findNavController().navigate(action)
        }
        holder.binding.executePendingBindings()
    }

    override fun getOnClickListener(id: String): View.OnClickListener? =
        null // This function is NOT used here so return null
}