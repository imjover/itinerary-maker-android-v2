/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * Defines that an [Entity] is a child and has a parent (relationship).
 */
@Deprecated("Firebase does not support foreign keys")
interface ChildEntity : Entity {

    /**
     * Retrieves the (main) foreign key that connects to a parent [Entity].
     *
     * Please note that any entity can have multiple relationship, but this focuses on retrieving
     * the main relationship it uses for this app.
     *
     * @return The relationship ID.
     */
    fun getForeignKey(): String
}