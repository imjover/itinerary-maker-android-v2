/*
 * Copyright (c) 2021. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.util

import android.content.pm.PackageManager


/**
 * Utility class for access to runtime permissions.
 *
 * Reference: https://github.com/googlemaps/android-samples/blob/master/ApiDemos/kotlin/app/src/gms/java/com/example/kotlindemos/PermissionUtils.kt
 */
class PermissionUtil {

    companion object {

        /**
         * Checks if the result contains a [PackageManager.PERMISSION_GRANTED] result for a
         * permission from a runtime permissions request.
         *
         * @see androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback
         */
        @JvmStatic
        fun isPermissionGranted(
            grantPermissions: Array<String>, grantResults: IntArray,
            permission: String
        ): Boolean {
            for (i in grantPermissions.indices) {
                if (permission == grantPermissions[i]) {
                    return grantResults[i] == PackageManager.PERMISSION_GRANTED
                }
            }
            return false
        }
    }
}