/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager

import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.entity.Status
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.util.Result
import kotlinx.coroutines.flow.Flow
import java.util.*


/**
 * Manages the travels of the application.
 */
interface TravelManager : SyncableManager<Travel> {


    /***********************************************************************************************
     * CRUD methods.
     **********************************************************************************************/

    /**
     * Retrieves the [Travel].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Travel].
     * @return The [Travel].
     */
    suspend fun getTravel(parentIds: Array<String>, id: String): Travel

    /**
     * Retrieves the [Travel] as a stream.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Travel].
     * @return The stream [Travel].
     */
    fun getTravelAsStream(parentIds: Array<String>, id: String): Flow<Travel>

    /**
     * Retrieve the status of a [Travel], stored locally as cache.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The ID of this record.
     * @return The status of the [Travel].
     */
    suspend fun getCachedStatus(parentIds: Array<String>, travelId: String): Status

    /**
     * Inserts a [Travel].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param description The label to be used for this record.
     * @param startDate The date when the travel should start.
     * @param endDate The date when the travel should have ended.
     * @return The ID of the [Travel].
     */
    suspend fun insert(
        parentIds: Array<String>,
        description: String,
        startDate: Date,
        endDate: Date
    ): String

    /**
     * Updates a [Travel].
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param oldTravel The old travel record to be updated.
     * @param description The label to be used for this record.
     * @param startDate The date when the travel should start.
     * @param endDate The date when the travel should have ended.
     */
    suspend fun update(
        parentIds: Array<String>,
        oldTravel: Travel,
        description: String,
        startDate: Date,
        endDate: Date
    )

    /**
     * Deletes the [Travel]s.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travels The [Travel]s to be deleted.
     */
    suspend fun delete(parentIds: Array<String>, vararg travels: Travel)


    /***********************************************************************************************
     * Review-related methods.
     **********************************************************************************************/

    /**
     * Submits the travel to be reviewed.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The ID of the travel.
     * @return The response message.
     */
    suspend fun submitForReview(parentIds: Array<String>, travelId: String): Result<String>

    /**
     * Submits the review on the travel.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The unique ID of the [Travel].
     * @param isApprove Whether the review is approve, then true, else false.
     * @param reason The reasons for the status change.
     * @return The response message.
     */
    suspend fun submitReview(
        parentIds: Array<String>,
        travelId: String,
        isApprove: Boolean,
        reason: String?
    ): Result<String>


    /***********************************************************************************************
     * Other methods.
     **********************************************************************************************/

    /**
     * Cancels the travel.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The ID of the travel.
     */
    suspend fun cancel(parentIds: Array<String>, travelId: String)

    /**
     * Updates the [paymentMode] of the travel.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param travelId The ID of this record.
     * @param paymentMode The payment mode.
     */
    suspend fun markAsPaid(
        parentIds: Array<String>,
        travelId: String,
        paymentMode: PaymentMode
    )
}