/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.main

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.firebase.messaging.FirebaseMessaging
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.ActivityMainBinding
import com.imjover.itinerarymaker.agency.view.login.LoginActivity
import com.imjover.itinerarymaker.agency.viewmodel.main.MainViewModel
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.view.AbstractActivity
import com.imjover.itinerarymaker.view.login.BaseLoginActivity
import com.imjover.itinerarymaker.view.main.AbstractMainActivity


/**
 * The main activity of the app.
 */
class MainActivity : AbstractMainActivity<MainViewModel, ActivityMainBinding>(),
    NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(dataBinding.toolbar)

        // Setup NavController and NavView
        val navController = getNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph, dataBinding.drawerLayout)
        dataBinding.toolbar.setupWithNavController(navController, appBarConfiguration)
        dataBinding.navView.setNavigationItemSelectedListener(this)
        if (viewModel.getLoggedInUser().userType == UserType.ACCOUNTANT) {
            dataBinding.navView.menu.findItem(R.id.usersFragment).isVisible = false
        }

        // Initializes push notification
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            Log.d(toString(), "FCM registration token: $it")
        }.addOnFailureListener {
            Log.w(toString(), "Fetching FCM registration token failed", it)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.log_out_menu -> {
                viewModel.logout()
                return true
            }
        }
        val navController = getNavController()
        val isOnNavDestinationSelected = item.onNavDestinationSelected(navController)
        if (isOnNavDestinationSelected) {
            dataBinding.drawerLayout.closeDrawer(GravityCompat.START)
        }
        return isOnNavDestinationSelected
    }


    /***********************************************************************************************
     * Private methods.
     **********************************************************************************************/

    private fun getNavController(): NavController =
        findNavController(R.id.nav_host_fragment_container)


    /***********************************************************************************************
     * Required functions of [AbstractActivity].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.activity_main

    override fun getViewModelClass(): Class<MainViewModel> =
        MainViewModel::class.java

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }

    override fun getLoginActivity(): Class<BaseLoginActivity<*, *>> =
        LoginActivity::class.java as Class<BaseLoginActivity<*, *>>

    override fun getNavigationView(): NavigationView =
        dataBinding.navView

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}