/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.user.di

import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.manager.user.internal.UserManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Binds the [UserManager].
 */
@Module
abstract class UsersManagerModule {

    @Singleton
    @Binds
    internal abstract fun bindUsersManager(usersManager: UserManagerImpl): UserManager
}