/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local

import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.source.type.LocalSyncableCrudChildSource

/**
 * Represents the local source of [Expense].
 */
interface ExpenseLocalSource :
    LocalSyncableCrudChildSource<Expense, String>