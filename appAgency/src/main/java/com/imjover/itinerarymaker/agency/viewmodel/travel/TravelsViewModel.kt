/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.travel

import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.travel.TravelsFragment
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.manager.SyncableManager
import com.imjover.itinerarymaker.manager.TravelManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.viewmodel.AbstractSyncableViewModel
import javax.inject.Inject


/**
 * The view model of [TravelsFragment].
 */
class TravelsViewModel : AbstractSyncableViewModel<Travel>() {

    @Inject
    lateinit var userManager: UserManager
    @Inject
    lateinit var travelManager: TravelManager
    internal lateinit var userId: String

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    override fun getParentIds(): Array<String> =
        arrayOf(userId)

    override fun getSyncableManager(): SyncableManager<Travel> =
        travelManager

    /**
     * Retrieves the currently logged in user.
     *
     * @return The user.
     */
    fun getLoggedInUser(): User =
        userManager.getLoggedInUser()
}