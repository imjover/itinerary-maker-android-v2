/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.destination

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.model.LatLng
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.FragmentDestinationFormBinding
import com.imjover.itinerarymaker.agency.viewmodel.destination.DestinationFormViewModel
import com.imjover.itinerarymaker.entity.DestinationType
import com.imjover.itinerarymaker.entity.TransportationMode
import com.imjover.itinerarymaker.util.DateFormat
import com.imjover.itinerarymaker.util.getNavigationResult
import com.imjover.itinerarymaker.helper.toImageRes
import com.imjover.itinerarymaker.util.toStringFormat
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.FormOption
import com.imjover.itinerarymaker.view.widget.IconTextSpinnerAdapter
import com.imjover.itinerarymaker.view.widget.getPositionOf
import com.imjover.itinerarymaker.view.widget.validator.FormValidator
import com.imjover.itinerarymaker.view.widget.validator.NonEmptyValidator
import java.util.*


private val TIMESTAMP_FORMAT = DateFormat.TIME_DATE_FORMAT
private val DESTINATION_TYPES = DestinationType.values().toSpinnerItems()
private val TRANSPORTATION_MODES = TransportationMode.values().toSpinnerItems()

/**
 * The screen that holds the form to add or edit destinations.
 */
class DestinationFormFragment :
    AbstractFragment<DestinationFormViewModel, FragmentDestinationFormBinding>() {

    private lateinit var formValidator: FormValidator
    private lateinit var formOption: FormOption
    private lateinit var latLngForAdding: LatLng
    private lateinit var timestamp: Date

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get the arguments needed
        val args = DestinationFormFragmentArgs.fromBundle(requireArguments())
        formOption = args.formOption
        if (args.parentIds.isNullOrEmpty()) {
            throw IllegalStateException()
        }
        viewModel.parentIds = args.parentIds
        when (formOption) {
            FormOption.ADD -> {
                if (args.place == null || !args.destinationId.isNullOrEmpty()) {
                    throw IllegalStateException()
                }
                latLngForAdding = args.place.latLng!!
                dataBinding.etName.setText(args.place.name)
                timestamp = Date()
            }
            FormOption.EDIT -> {
                if (args.place != null || args.destinationId.isNullOrEmpty()) {
                    throw IllegalStateException()
                }
                viewModel.getDestination(args.destinationId)
            }
        }

        // Setup the UI
        dataBinding.tvTimestamp.setOnClickListener {
            // Observe to fragment result
            val liveData = getNavigationResult<Date>()!!
            liveData.observe(viewLifecycleOwner, object : Observer<Date> {
                override fun onChanged(timestamp: Date) {
                    dataBinding.tvTimestamp.text = timestamp.toStringFormat(TIMESTAMP_FORMAT)
                    liveData.removeObserver(this)
                }
            })
            // Open fragment
            val action =
                DestinationFormFragmentDirections.actionDestinationFormFragmentToDateTimePickerDialog()
            findNavController().navigate(action)
        }
        dataBinding.spDestinationType.adapter =
            IconTextSpinnerAdapter(
                this.requireContext(),
                DESTINATION_TYPES
            )
        dataBinding.spTransportationMode.adapter =
            IconTextSpinnerAdapter(
                this.requireContext(),
                TRANSPORTATION_MODES
            )
        dataBinding.btnSave.setOnClickListener {
            save()
        }
        dataBinding.btnCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        // Setup validators
        val nonEmptyValidator = NonEmptyValidator()
        formValidator = FormValidator()
        formValidator.addValidator(dataBinding.etName, nonEmptyValidator)
        formValidator.addValidator(dataBinding.tvTimestamp, nonEmptyValidator)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup observers on view model
        viewModel.oldDestination.observe(viewLifecycleOwner, Observer {
            timestamp = it.timestamp
            dataBinding.etName.setText(it.name)
            dataBinding.tvTimestamp.text = timestamp.toStringFormat(TIMESTAMP_FORMAT)
            val posDestinationType = DESTINATION_TYPES.getPositionOf(it.destinationType.id)
            val posTransportationMode = TRANSPORTATION_MODES.getPositionOf(it.transportationMode.id)
            dataBinding.spDestinationType.setSelection(posDestinationType)
            dataBinding.spTransportationMode.setSelection(posTransportationMode)
        })
        viewModel.dismiss.observe(viewLifecycleOwner, Observer {
            findNavController().popBackStack()
        })
    }

    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Handles the saving process of the inputs.
     */
    private fun save() {
        if (!formValidator.validateAll()) {
            return
        }
        val description = dataBinding.etName.text.toString().trim()
        val destinationType =
            DestinationType.fromId(dataBinding.spDestinationType.selectedItem as String)
        val transportationMode =
            TransportationMode.fromId(dataBinding.spTransportationMode.selectedItem as String)
        when (formOption) {
            FormOption.ADD -> viewModel.insert(
                description,
                timestamp,
                destinationType,
                transportationMode,
                latLngForAdding.latitude,
                latLngForAdding.longitude
            )
            FormOption.EDIT -> viewModel.update(
                description,
                timestamp,
                destinationType,
                transportationMode
            )
        }
    }

    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_destination_form

    override fun getViewModelClass(): Class<DestinationFormViewModel> =
        DestinationFormViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}


/**
 * Converts an array of [DestinationType] to list of [IconTextSpinnerAdapter.SpinnerItem] to be used
 * for as adapter for a [Spinner].
 *
 * @return [Spinner] adapter items.
 */
private fun Array<DestinationType>.toSpinnerItems(): List<IconTextSpinnerAdapter.SpinnerItem> {
    val items = mutableListOf<IconTextSpinnerAdapter.SpinnerItem>()
    this.forEach {
        items.add(
            IconTextSpinnerAdapter.SpinnerItem(
                it.id,
                it.name.toLowerCase().capitalize(),
                it.toImageRes()
            )
        )
    }
    return items
}

/**
 * Converts an array of [TransportationMode] to list of [IconTextSpinnerAdapter.SpinnerItem] to be used
 * for as adapter for a [Spinner].
 *
 * @return [Spinner] adapter items.
 */
private fun Array<TransportationMode>.toSpinnerItems(): List<IconTextSpinnerAdapter.SpinnerItem> {
    val items = mutableListOf<IconTextSpinnerAdapter.SpinnerItem>()
    this.forEach {
        items.add(
            IconTextSpinnerAdapter.SpinnerItem(
                it.id,
                it.name.toLowerCase().capitalize(),
                it.toImageRes()
            )
        )
    }
    return items
}