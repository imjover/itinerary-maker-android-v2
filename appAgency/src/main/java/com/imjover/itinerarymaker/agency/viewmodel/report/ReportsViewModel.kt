/*
 * Copyright (c) 2021. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.report

import android.os.Build
import android.os.Environment
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.BuildConfig
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.report.SalesReportFragment
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.helper.ReportFormat
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.manager.ReportManager
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.util.CsvWriterUtil
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


private const val DOWNLOAD_CSV_ROOT_FOLDER = "itineraries"

/**
 * The view model of [SalesReportFragment].
 */
class ReportsViewModel : AbstractViewModel() {

    @Inject
    lateinit var reportManager: ReportManager

    private val _showMessage: MutableLiveData<Consumable<String>> = MutableLiveData()
    val showMessage: LiveData<Consumable<String>> get() = _showMessage

    private val _showCSV: MutableLiveData<Consumable<String>> = MutableLiveData()
    val showCSV: LiveData<Consumable<String>> get() = _showCSV

    val isLoading = ObservableBoolean()

    private lateinit var job: Job

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Generates the invoice
     */
    fun generateSalesReport(startDate: Date, endDate: Date) {
        job = viewModelScope.launch {
            isLoading.set(true)
            try {
                _showMessage.value = Consumable(
                    AppComponent.appContext.getString(
                        R.string.sales_report_fragment__download_sales_report__downloading__message
                    )
                )

                _showMessage.value = reportManager.getSalesReport(startDate, endDate)
                    .map {
                        val csvFolder: File =
                            if (Build.VERSION.SDK_INT >= 19) {
                                File(
                                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
                                    DOWNLOAD_CSV_ROOT_FOLDER
                                )
                            } else {
                                File(
                                    Environment.getExternalStorageDirectory().absolutePath + "/Documents",
                                    DOWNLOAD_CSV_ROOT_FOLDER
                                );
                            }
                        if (!csvFolder.exists()) {
                            csvFolder.mkdirs()
                        }
                        val dateFormat = SimpleDateFormat("yyyyMMdd")
                        val fileName =
                            "sales_report_${dateFormat.format(startDate)}-${
                                dateFormat.format(
                                    endDate
                                )
                            }.csv"
                        val filePath = File(csvFolder.absolutePath, fileName)

                        val csvSalesReport = ReportFormat.salesReportToCSV(it, startDate, endDate)
                        CsvWriterUtil.write(filePath, csvSalesReport.toString())
                        _showCSV.value = Consumable(filePath.absolutePath)

                        Consumable(
                            AppComponent.appContext.getString(
                                R.string.sales_report_fragment__download_sales_report__done__message
                            )
                        )
                    }
                    .materialize {
                        Consumable(it.getMessageForUI())
                    }
            } catch (ex: CancellationException) {
                Log.i(toString(), "Downloading report is cancelled")
            } finally {
                if (!isActive) {
                    // If cancelled, then show inform user
                    _showMessage.value =
                        Consumable(
                            AppComponent.appContext.getString(
                                R.string.itinerary_fragment__generate_invoice__cancelled__message
                            )
                        )
                }
                isLoading.set(false)
            }
        }
    }

    /**
     * Attempts to cancel the generation of invoice.
     */
    fun tryToCancelGeneration() {
        isLoading.set(false)
        if (this::job.isInitialized) {
            job.cancel()
        }
    }

    override fun toString(): String =
        if (BuildConfig.DEBUG) "ReportsViewModel" else super.toString()
}