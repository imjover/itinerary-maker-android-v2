/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.firebase

import android.util.Log
import com.google.firebase.functions.FirebaseFunctionsException


/**
 * Handles the logging of Firebase Exceptions.
 *
 * @param tag The tag of the log.
 */
internal fun Throwable.logException(tag: String) {
    if (this is FirebaseFunctionsException) {
        Log.e(tag, "Firebase Exception [$code]: $details")
    } else {
        Log.e(tag, this.message)
    }
}