/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.viewmodel.destination

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.traveler.BuildConfig
import com.imjover.itinerarymaker.traveler.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.traveler.view.destination.DestinationMapFragment
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.viewmodel.destination.AbstractDestinationMapViewModel
import kotlinx.coroutines.launch

/**
 * The view model of [DestinationMapFragment].
 */
class DestinationMapViewModel : AbstractDestinationMapViewModel() {

    private val _showMessage: MutableLiveData<Consumable<String>> = MutableLiveData()
    val showMessage: LiveData<Consumable<String>> get() = _showMessage

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Retrieve all unvisited destinations.
     *
     * @return All unvisited destinations.
     */
    fun getUnvisitedDestinations(): Array<Destination> =
        destinations.filter { !it.isVisited }.toTypedArray()

    /**
     * Set this destination as visited.
     */
    fun setDestinationAsVisited(id: String) {
        viewModelScope.launch {
             destinationManager.updateIsVisited(parentIds, id, true)
                .materialize{
                    _showMessage.value = Consumable(it.getMessageForUI())
                }
        }
    }

    /***********************************************************************************************
     * Others
     **********************************************************************************************/

    override fun toString(): String =
        if (BuildConfig.DEBUG) "DestinationMapViewModel" else super.toString()
}