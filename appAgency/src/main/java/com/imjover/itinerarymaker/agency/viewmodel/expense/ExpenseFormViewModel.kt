/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.expense

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.entity.ExpenseType
import com.imjover.itinerarymaker.manager.ExpenseManager
import com.imjover.itinerarymaker.agency.view.expense.ExpenseFormDialog
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.launch
import java.math.BigDecimal
import javax.inject.Inject


/**
 * The view model of [ExpenseFormDialog].
 */
class ExpenseFormViewModel : AbstractViewModel() {

    @Inject lateinit var expenseManager: ExpenseManager
    internal lateinit var parentIds: Array<String>
    private val _oldExpense: MutableLiveData<Expense> = MutableLiveData()
    val oldExpense: LiveData<Expense>
        get() = _oldExpense
    private val _dismiss: MutableLiveData<Void> = MutableLiveData()
    val dismiss: LiveData<Void>
        get() = _dismiss
    val isLoading = ObservableBoolean()

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Retrieves the details of the [Expense].
     *
     * @param expenseId The unique ID of the expense.
     */
    fun getExpense(expenseId: String) {
        viewModelScope.launch {
            _oldExpense.value = expenseManager.getExpense(parentIds, expenseId)
        }
    }

    /**
     * Inserts an [Expense].
     *
     * @param description The label to be used for this record.
     * @param expenseType The type of expense.
     * @param capital Cost of the good or services.
     * @param price Amount to be paid by the customer.
     * @param remarks An optional remark or note.
     * @param isDone Whether this expenses is done, then true, else false.
     */
    fun insert(
        description: String,
        expenseType: ExpenseType,
        capital: BigDecimal,
        price: BigDecimal,
        remarks: String?,
        isDone: Boolean
    ) {
        viewModelScope.launch {
            isLoading.set(true)
            expenseManager.insert(
                parentIds,
                description,
                expenseType,
                capital,
                price,
                remarks,
                isDone
            )
            _dismiss.value = null
            isLoading.set(false)
        }
    }

    /**
     * Updates an [Expense].
     *
     * @param description The label to be used for this record.
     * @param expenseType The type of expense.
     * @param capital Cost of the good or services.
     * @param price Amount to be paid by the customer.
     * @param remarks An optional remark or note.
     * @param isDone Whether this expenses is done, then true, else false.
     */
    fun update(
        description: String,
        expenseType: ExpenseType,
        capital: BigDecimal,
        price: BigDecimal,
        remarks: String?,
        isDone: Boolean
    ) {
        viewModelScope.launch {
            isLoading.set(true)
            expenseManager.update(
                parentIds,
                oldExpense.value!!,
                description,
                expenseType,
                capital,
                price,
                remarks,
                isDone
            )
            _dismiss.value = null
            isLoading.set(false)
        }
    }
}