/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.firebase

import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.source.remote.NotificationRemoteSource


class NotificationRemoteSourceImpl : NotificationRemoteSource {

    override suspend fun setAsSeen(ids: List<String>) {
        TODO("Not yet implemented")
    }

    override suspend fun getListChildrenOf(parentId: String): List<Notification> {
        TODO("Not yet implemented")
    }
}