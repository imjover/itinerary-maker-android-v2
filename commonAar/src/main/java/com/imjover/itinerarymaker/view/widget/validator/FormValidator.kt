/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget.validator

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged

/**
 * Manages the validation of added [View]s, and displays error messages if validation fails.
 *
 * TODO: It seem it's better if the validator also returns the value that was validated.
 * This is to ensure that the same exact value will be used for processing.
 * Maybe validate each view one-by-one in the activity level, then return the value.
 */
class FormValidator {

    private val viewValidators = mutableMapOf<View, List<Validator>>()

    /**
     * Checks whether all [View]s have passed its assigned [Validator]s.
     *
     * @return Whether all [View]s have passed its assigned [Validator]s, then true, else false.
     */
    fun validateAll(): Boolean {
        var passed = true
        viewValidators.forEach {
            val view = it.key
            val validators = it.value
            validators.forEach { validator ->
                if (!validator.isValid(view)) {
                    showErrorMsg(view, validator.getErrorMsg())
                    passed = false
                    return@forEach
                } else {
                    removeErrorMsg(view)
                }
            }
        }
        return passed
    }

    /**
     * Checks whether a [View]s have passed its assigned [Validator]s.
     *
     * @param view The [View] to be validated.
     * @return Whether a [View]s have passed its assigned [Validator]s, then true, else false.
     */
    fun validate(view: View): Boolean {
        val validators = viewValidators[view]
        validators?.forEach {  validator ->
            if (!validator.isValid(view)) {
                showErrorMsg(view, validator.getErrorMsg())
                return false
            }
        }
        removeErrorMsg(view)
        return true
    }

    /**
     * Assign (add or replace) [Validator] to a [View].
     *
     * @param view The [View] to be validated.
     * @param validators The [Validator]s to be used for validation.
     */
    fun addValidator(view: View, vararg validators: Validator) {
        viewValidators[view] = validators.toList()

        when (view) {
            is TextView -> {
                view.doAfterTextChanged {
                    validate(view)
                }
            }
            is Spinner -> {
                view.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        // Do nothing
                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        v: View?,
                        position: Int,
                        id: Long
                    ) {
                        validate(view)
                    }
                }
            }
        }
    }

    /**
     * Removes all assigned [Validator]
     *
     * @param view The [View] to be validated.
     */
    fun removeValidatorsOf(view: View) {
        viewValidators.remove(view)
    }


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Shows the error message in the [View].
     *
     * @param view The [View] to be validated.
     * @param errorMsg The error message to be displayed.
     */
    private fun showErrorMsg(view: View, errorMsg: String) {
        if (view is TextView) {
            view.error = errorMsg
        }
    }

    /**
     * Removes the error message in the [View].
     *
     * @param view The [View] to be validated.
     */
    private fun removeErrorMsg(view: View) {
        if (view is TextView) {
            view.error = null
        }
    }
}