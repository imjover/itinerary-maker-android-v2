/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.login

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.login.RegistrationFormDialog
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [RegistrationFormDialog].
 */
class RegistrationFormViewModel : AbstractViewModel() {

    @Inject lateinit var userManager: UserManager

    private val _dismiss: MutableLiveData<Unit> = MutableLiveData()
    val dismiss: LiveData<Unit> get() = _dismiss

    private val _message: MutableLiveData<String> = MutableLiveData()
    val message: LiveData<String> get() = _message

    val isLoading = ObservableBoolean()

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Register the currently logged-in user.
     *
     * @param userType The user's type.
     */
    fun register(userType: UserType) {
        viewModelScope.launch {
            isLoading.set(true)
            val result = userManager.register(userType)
            _message.value = result.materialize { it.getMessageForUI() }
            _dismiss.value = Unit
            isLoading.set(false)
        }
    }
}