/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source.internal

import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.repository.UserRepository
import com.imjover.itinerarymaker.source.local.UserLocalSource
import com.imjover.itinerarymaker.source.remote.UserRemoteSource
import com.imjover.itinerarymaker.util.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext


internal class UserRepositoryImpl(
    private val localSource: UserLocalSource,
    private val remoteSource: UserRemoteSource,
    private val dispatcher: CoroutineDispatcher
) : UserRepository {

    override suspend fun findUserByLogin(username: String, password: String): Result<User> =
        withContext(dispatcher) {
            val user = remoteSource.findUserByLogin(username, password, AppComponent.appType)
            user.getOrNull()?.let {
                // Cache the logged-in user
                localSource.insertOrUpdate(it)
            }
            return@withContext user
        }

    override suspend fun logout(): Result<Unit> =
        withContext(dispatcher) {
            remoteSource.logout()
        }

    override suspend fun register(userType: UserType): Result<String> =
        withContext(dispatcher) {
            remoteSource.register(userType)
        }

    override suspend fun approveRegistration(user: User): Result<String> =
        withContext(dispatcher) {
            val result = remoteSource.approveRegistration(user)
            result.getOrNull()?.let {
                val approvedUser = User(
                    user.id,
                    user.username,
                    user.name,
                    user.photoUrl,
                    user.userType,
                    true
                )
                localSource.insertOrUpdate(approvedUser)
            }
            return@withContext result
        }

    override fun getLoggedInUser(): User =
        remoteSource.getLoggedInUser()

    override fun getUsers(nameQuery: String, vararg userTypes: UserType): Flow<User> {
        // TODO : Cache users from remote to local
        return localSource.getUsers(nameQuery, *userTypes)
            .flatMapMerge {
                // FIXME: merging works for now, but I believe this will cause a bug wherein
                //  this will only return users that are present in both local and remote source
                // Meaning, if the user is NOT yet cached in local source, then it will NOT be
                // emitted in searching. :(
                remoteSource.getUsers(*userTypes).asFlow()
            }
    }
}