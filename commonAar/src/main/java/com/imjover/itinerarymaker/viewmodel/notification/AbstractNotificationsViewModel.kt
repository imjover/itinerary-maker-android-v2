/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.manager.NotificationManager
import com.imjover.itinerarymaker.manager.SyncableManager
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.view.notification.AbstractNotificationsFragment
import com.imjover.itinerarymaker.viewmodel.AbstractSyncableViewModel
import com.imjover.itinerarymaker.viewmodel.InjectableViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [AbstractNotificationsFragment].
 */
abstract class AbstractNotificationsViewModel : AbstractSyncableViewModel<Notification>(),
    InjectableViewModel {

    @Inject
    lateinit var notificationManager: NotificationManager

    @Inject
    lateinit var userManager: UserManager

    private val _openNextScreen: MutableLiveData<Consumable<Notification>> =
        MutableLiveData()
    val openNextScreen: LiveData<Consumable<Notification>> get() = _openNextScreen

    init {
        injectDependencies()
    }

    /**
     * Set the notification as seen.
     *
     * @param notification The notification.
     */
    fun setAsSeen(notification: Notification) {
        viewModelScope.launch {
            if (!notification.isSeen) {
                notificationManager.setAsSeen(
                    userManager.getLoggedInUser().id,
                    notification.getUniqueId()
                )
            }
            _openNextScreen.value = Consumable(notification)
        }
    }

    /**
     * Clears the live data in this view model.
     */
    fun clearLiveData() {
        _openNextScreen.value = null
    }

    /**
     * Retrieves the user id of the currently logged-in user.
     */
    fun getLoggedInUserId(): String =
        userManager.getLoggedInUser().id


    /***********************************************************************************************
     * Required functions of [AbstractSyncableViewModel].
     **********************************************************************************************/

    override fun getParentIds(): Array<String> = emptyArray()

    override fun getSyncableManager(): SyncableManager<Notification> =
        notificationManager
}