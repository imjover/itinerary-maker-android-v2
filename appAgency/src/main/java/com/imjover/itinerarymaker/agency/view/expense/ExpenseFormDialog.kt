/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.expense

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.DialogExpenseFormBinding
import com.imjover.itinerarymaker.agency.viewmodel.expense.ExpenseFormViewModel
import com.imjover.itinerarymaker.entity.ExpenseType
import com.imjover.itinerarymaker.helper.toImageRes
import com.imjover.itinerarymaker.util.toStringMonetary
import com.imjover.itinerarymaker.view.AbstractDialogFragment
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.FormOption
import com.imjover.itinerarymaker.view.widget.IconTextSpinnerAdapter
import com.imjover.itinerarymaker.view.widget.getPositionOf
import com.imjover.itinerarymaker.view.widget.validator.FormValidator
import com.imjover.itinerarymaker.view.widget.validator.NonEmptyValidator
import com.imjover.itinerarymaker.view.widget.validator.RegexValidator
import java.math.BigDecimal


private const val REGEX_AMOUNT = "^\\d{1,3}([ ,]?\\d{3})*([.,]\\d+)?\$"
private val EXPENSE_TYPES = ExpenseType.values().toSpinnerItems()

/**
 * The screen that holds the form to add or edit expenses.
 */
class ExpenseFormDialog : AbstractDialogFragment<ExpenseFormViewModel, DialogExpenseFormBinding>() {

    private lateinit var formValidator: FormValidator
    private lateinit var formOption: FormOption

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get the arguments passed from previous screen
        val args = ExpenseFormDialogArgs.fromBundle(requireArguments())
        formOption = args.formOption
        if (args.parentIds.isEmpty()) {
            throw IllegalStateException()
        }
        viewModel.parentIds = args.parentIds
        when (formOption) {
            FormOption.ADD -> {
                if (!args.expenseId.isNullOrEmpty()) {
                    throw IllegalStateException()
                }
            }
            FormOption.EDIT -> {
                if (args.expenseId.isNullOrEmpty()) {
                    throw IllegalStateException()
                }
                viewModel.getExpense(args.expenseId)
            }
        }

        // Setup UI
        isCancelable = false
        dataBinding.spExpenseType.adapter =
            IconTextSpinnerAdapter(
                this.requireContext(),
                EXPENSE_TYPES
            )
        dataBinding.btnSave.setOnClickListener { save() }
        dataBinding.btnCancel.setOnClickListener { dismiss() }

        // Setup validators
        val nonEmptyValidator = NonEmptyValidator()
        val regexValidator =
            RegexValidator(REGEX_AMOUNT, getString(R.string.form_validation__amount_format_message))
        formValidator = FormValidator()
        formValidator.addValidator(dataBinding.etName, nonEmptyValidator)
        formValidator.addValidator(dataBinding.etCapital, nonEmptyValidator, regexValidator)
        formValidator.addValidator(dataBinding.etPrice, nonEmptyValidator, regexValidator)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup observers on view model
        viewModel.oldExpense.observe(viewLifecycleOwner, {
            val posExpenseType = EXPENSE_TYPES.getPositionOf(it.expenseType.id)
            dataBinding.spExpenseType.setSelection(posExpenseType)
            dataBinding.etCapital.setText(it.capital.toStringMonetary())
            dataBinding.etPrice.setText(it.price.toStringMonetary())
        })
        viewModel.dismiss.observe(this, {
            // Do nothing because the calling Fragment/Activity should collect from stream.
            dismiss()
        })
    }

    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Handles the saving process of the inputs.
     */
    private fun save() {
        if (!formValidator.validateAll()) {
            return
        }
        val description = dataBinding.etName.text.toString().trim()
        val capital = BigDecimal(dataBinding.etCapital.text.toString().trim().removeComma())
        val price = BigDecimal(dataBinding.etPrice.text.toString().trim().removeComma())
        val expenseType = ExpenseType.fromId(dataBinding.spExpenseType.selectedItem as String)
        val remarks = dataBinding.etRemarks.text.toString()
        val isDone = dataBinding.cbIsDone.isChecked
        when (formOption) {
            FormOption.ADD -> viewModel.insert(
                description,
                expenseType,
                capital,
                price,
                remarks,
                isDone
            )
            FormOption.EDIT -> viewModel.update(
                description,
                expenseType,
                capital,
                price,
                remarks,
                isDone
            )
        }
    }

    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.dialog_expense_form

    override fun getViewModelClass(): Class<ExpenseFormViewModel> =
        ExpenseFormViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}


/**
 * Converts an array of [ExpenseType] to list of [IconTextSpinnerAdapter.SpinnerItem] to be used
 * for as adapter for a [Spinner].
 *
 * @return [Spinner] adapter items.
 */
private fun Array<ExpenseType>.toSpinnerItems(): List<IconTextSpinnerAdapter.SpinnerItem> {
    val items = mutableListOf<IconTextSpinnerAdapter.SpinnerItem>()
    this.forEach {
        items.add(
            IconTextSpinnerAdapter.SpinnerItem(
                it.id,
                it.name.toLowerCase().capitalize(),
                it.toImageRes()
            )
        )
    }
    return items
}

private fun String.removeComma(): String =
    this.replace(",", "")