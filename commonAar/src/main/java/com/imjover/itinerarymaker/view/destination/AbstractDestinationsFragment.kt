/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.destination

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.libraries.places.api.Places
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.imjover.itinerarymaker.BuildConfig
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.databinding.BottomSheetDestinationsBinding
import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.helper.toImageRes
import com.imjover.itinerarymaker.util.DateFormat
import com.imjover.itinerarymaker.util.toStringFormat
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.viewmodel.destination.AbstractDestinationsViewModel
import kotlinx.android.synthetic.main.bottom_sheet_destinations.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


const val ARG_PARENT_IDS = "ARG_PARENT_IDS"

/**
 * The screen that holds all destinations.
 */
abstract class AbstractDestinationsFragment<VM : AbstractDestinationsViewModel, DB : ViewDataBinding> :
    AbstractFragment<VM, DB>(),
    DestinationsHandlerListener {

    protected lateinit var selectedDestination: Destination
        private set
    protected lateinit var bottomSheetBehavior: BottomSheetBehavior<ViewGroup>
    protected lateinit var bottomSheet: BottomSheetDestinationsBinding
    protected lateinit var pager: ViewPager2
    protected lateinit var tabLayout: TabLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)

        // Initialize the Places SDK
        Places.initialize(
            this@AbstractDestinationsFragment.requireActivity().application,
            getString(R.string.google_maps_key)
        )
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Update Activity's title
        lifecycleScope.launch {
            viewModel.getItineraryAsStream().collect {
                // FIXME: Activity's title is NOT updated sometimes (usually starting 2nd attempt).
                requireActivity().title = it.name
            }
        }
        // Setup Pager
        pager.isUserInputEnabled = false // Disables ViewPager's swiping
        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        })
        pager.adapter = getPagerAdapter()
        TabLayoutMediator(this.tabLayout, pager) { tab, position ->
            // TODO : Change text to icon
//            tab.icon = resources.getDrawable(
//                when (position) {
//                    0 -> R.drawable.ic_list_48px
//                    1 -> R.drawable.ic_map_48px
//                    else -> throw IllegalStateException()
//                }
//            )
            tab.text = when (position) {
                0 -> "List"
                1 -> "Map"
                else -> throw IllegalStateException()
            }
        }.attach()
        // Setup BottomSheet
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // FIXME: Improvement implementation of adjusting the height of the ViewPager
                //  depending on the BottomSheet's visible height.
                // This section manually adjusts the ViewPager's space (or padding) according to
                // the BottomSheet's slide offset.
                // As per StackOverflow, adjusting the RecyclerView to accommodate the BottomSheet
                // can be done by adding the following attribute in RecyclerView
                // app:layout_behavior="@string/appbar_scrolling_view_behavior"
                // However, I used ViewPager (to hold the fragments for list and map), so it seems
                // the mentioned attribute does not work by adding it in the ViewPager, nor the
                // RecyclerView inside the ViewPager's fragment.
                val bottomPadding: Int =
                    if (slideOffset >= 0) {
                        // Use below code to use bottom sheet's full height
                        (bottomSheetBehavior.peekHeight + ((bottomSheet.height - bottomSheetBehavior.peekHeight) * slideOffset)).toInt()
                    } else { //if (slideOffset < 0)
                        (bottomSheetBehavior.peekHeight * (slideOffset + 1)).toInt()
                    }
                pager.setPadding(0, 0, 0, bottomPadding)
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // Do nothing
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    val action =
                        getNavToExpenses(
                            selectedDestination.id,
                            selectedDestination.name
                        )
                    findNavController().navigate(action)
                }
                onBottomSheetStateChanged(newState)
            }
        })
        // Setup ViewModel's observers
        viewModel.destination.observe(viewLifecycleOwner, this::handleShowingBottomSheetOf)
    }


    /***********************************************************************************************
     * Abstract functions.
     **********************************************************************************************/

    /**
     * Retrieves the navigation to next screen.
     */
    protected abstract fun getNavToExpenses(id: String, name: String): NavDirections

    /**
     * Retrieves the the pager adapter.
     */
    protected abstract fun getPagerAdapter(): FragmentStateAdapter

    /**
     * Called when bottom sheet's state is changed.
     */
    protected abstract fun onBottomSheetStateChanged(newState: Int)


    /***********************************************************************************************
     * Public functions.
     **********************************************************************************************/

    override fun showBottomSheetOf(destinationId: String) {
        viewModel.getDestination(destinationId)
    }


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Shows the BottomSheet with the details of [Destination].
     */
    private fun handleShowingBottomSheetOf(destination: Destination) {
        selectedDestination = destination
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        bottomSheet.bsDestination.tv_name.text = destination.name
        bottomSheet.bsDestination.tv_timestamp.text =
            destination.timestamp.toStringFormat(DateFormat.TIME_DATE_FORMAT)
        bottomSheet.bsDestination.iv_destination_type.setImageResource(destination.destinationType.toImageRes())
        bottomSheet.bsDestination.iv_transportation_mode.setImageResource(destination.transportationMode.toImageRes())
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }


    /***********************************************************************************************
     * Other functions.
     **********************************************************************************************/

    override fun toString(): String {
        return if (BuildConfig.DEBUG) "DestinationsFragment" else super.toString()
    }
}