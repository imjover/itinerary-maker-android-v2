/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.helper

import androidx.annotation.StringRes
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.entity.*


/**
 * Retrieves the equivalent String resource of the [Status].
 */
@StringRes
fun Status.toStringRes(): Int =
    when (this) {
        Status.OK -> R.string.status_enum__ok
        Status.IN_PROGRESS -> R.string.status_enum__in_progress
        Status.FOR_REVIEW -> R.string.status_enum__for_review
        Status.REVIEWED_OK -> R.string.status_enum__reviewed_ok
        Status.REVIEWED_DECLINED -> R.string.status_enum__reviewed_declined
        // End state
        Status.PAID -> R.string.status_enum__paid
        Status.DELETED -> R.string.status_enum__deleted
        Status.CANCELLED -> R.string.status_enum__cancelled
        else -> throw IllegalStateException()
    }

/**
 * Retrieves the equivalent String resource of the [PaymentMode].
 */
@StringRes
fun PaymentMode.toStringRes(): Int =
    when (this) {
        PaymentMode.CASH -> R.string.payment_mode__cash
        PaymentMode.BANK_TRANSFER -> R.string.payment_mode__bank_transfer
        PaymentMode.CREDIT_CARD -> R.string.payment_mode__credit_card
        else -> throw IllegalStateException()
    }

/**
 * Retrieves the equivalent String resource of the [UserType].
 */
@StringRes
fun UserType.toStringRes(): Int =
    when (this) {
        UserType.ADMIN -> R.string.user_type__admin
        UserType.TRAVELER -> R.string.user_type__traveler
        UserType.AGENT -> R.string.user_type__agent
        UserType.ACCOUNTANT -> R.string.user_type__accountant
        else -> throw IllegalStateException()
    }


/**
 * Retrieves the equivalent String resource of the [DestinationType].
 */
@StringRes
fun DestinationType.toStringRes(): Int =
    when (this) {
        DestinationType.DEFAULT -> R.string.destination_type__default
        DestinationType.HOTEL -> R.string.destination_type__hotel
        DestinationType.AIRPORT -> R.string.destination_type__airport
        DestinationType.RESTAURANT -> R.string.destination_type__restaurant
        DestinationType.CAFE -> R.string.destination_type__cafe
        DestinationType.BAR -> R.string.destination_type__bar
        DestinationType.MALL -> R.string.destination_type__mall
        DestinationType.MUSEUM -> R.string.destination_type__museum
        else -> throw IllegalStateException()
    }


/**
 * Retrieves the equivalent String resource of the [TransportationMode].
 */
@StringRes
fun TransportationMode.toStringRes(): Int =
    when (this) {
        TransportationMode.WALK -> R.string.transportation_mode__walk
        TransportationMode.CAR -> R.string.transportation_mode__car
        TransportationMode.BUS -> R.string.transportation_mode__bus
        TransportationMode.TAXI -> R.string.transportation_mode__taxi
        TransportationMode.TRAIN -> R.string.transportation_mode__train
        TransportationMode.AIRPLANE -> R.string.transportation_mode__airplane
        TransportationMode.BOAT -> R.string.transportation_mode__boat
        TransportationMode.TRAM -> R.string.transportation_mode__tram
        else -> throw IllegalStateException()
    }


/**
 * Retrieves the equivalent String resource of the [TransportationMode].
 */
@StringRes
fun ExpenseType.toStringRes(): Int =
    when (this) {
        ExpenseType.ACCOMMODATION -> R.string.expense_type__accommodation
        ExpenseType.TRANSPORTATION -> R.string.expense_type__transportation
        ExpenseType.FOOD -> R.string.expense_type__food
        ExpenseType.VISA -> R.string.expense_type__visa
        ExpenseType.TOUR -> R.string.expense_type__tour
        ExpenseType.OTHERS -> R.string.expense_type__others
        else -> throw IllegalStateException()
    }