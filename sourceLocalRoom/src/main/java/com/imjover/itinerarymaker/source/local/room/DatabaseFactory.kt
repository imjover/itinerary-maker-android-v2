/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.room

import androidx.room.Room
import com.imjover.itinerarymaker.common.di.AppComponent

/**
 * Holds the static instance of the [AppDatabase].
 */
internal class DatabaseFactory {

    companion object {
        private lateinit var appDatabase: AppDatabase

        fun getDatabase(): AppDatabase {
            if (!this::appDatabase.isInitialized) {
                appDatabase = Room.databaseBuilder(
                    AppComponent.appContext,
                    AppDatabase::class.java, "itinerary-maker"
                ).build()
            }
            return appDatabase
        }
    }
}