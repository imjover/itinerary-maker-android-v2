/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.manager.report.internal

import com.imjover.itinerarymaker.entity.ExpenseType
import com.imjover.itinerarymaker.manager.ReportManager
import com.imjover.itinerarymaker.repository.ReportRepository
import com.imjover.itinerarymaker.repository.di.RepositoryFactory
import com.imjover.itinerarymaker.util.Result
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject


internal class ReportManagerImpl @Inject constructor() : ReportManager {

    private val reportRepository: ReportRepository =
        RepositoryFactory.singleton().getReportRepository()

    override suspend fun getSalesReport(
        startDate: Date,
        endDate: Date
    ): Result<Map<ExpenseType, ArrayList<BigDecimal>>> =
        reportRepository.getSalesReport(startDate, endDate)
}