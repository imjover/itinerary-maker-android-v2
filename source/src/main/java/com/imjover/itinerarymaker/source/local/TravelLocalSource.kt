/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local

import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.entity.Status
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.source.type.LocalSyncableCrudChildSource
import java.util.*

/**
 * Represents the local source of [Travel].
 */
interface TravelLocalSource : LocalSyncableCrudChildSource<Travel, String> {

    /**
     * Retrieve the status of a [Travel].
     *
     * @param travelId The ID of this record.
     * @return The status of the [Travel].
     */
    suspend fun getStatus(travelId: String): Status

    /**
     * Cancels the travel.
     *
     * @param travelId The ID of the travel.
     */
    suspend fun cancel(travelId: String)

    /**
     * Updates the [paymentMode] of the travel.
     *
     * @param travelId The ID of this record.
     * @param paymentMode The payment mode.
     * @param lastUpdatedBy The user who updated this record.
     * @param lastUpdated The date this record was updated.
     */
    suspend fun updatePaymentMode(travelId: String, paymentMode: PaymentMode, lastUpdatedBy: String, lastUpdated: Date)
}