/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase.internal

import android.util.Log
import com.google.firebase.firestore.*
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.entity.SyncableEntity
import com.imjover.itinerarymaker.repository.CrudRepository
import com.imjover.itinerarymaker.repository.SyncableRepository
import com.imjover.itinerarymaker.repository.firebase.BuildConfig
import com.imjover.itinerarymaker.repository.firebase.RemoteSourceFirebaseException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


/**
 * Defines the repositories that can be synced, and has CRUD.
 */
abstract class AbstractSyncableCrudRepository<
        SE : SyncableEntity>(
    private val dispatcher: CoroutineDispatcher
) : CrudRepository<SE>,
    SyncableRepository<SE> {


    /***********************************************************************************************
     * Abstract functions.
     **********************************************************************************************/

    /**
     * Retrieves the collection & documents path for running FireStore queries.
     */
    protected abstract fun getEventsCollection(parentIds: Array<String>): CollectionReference

    /**
     * Retrieves the collection & documents path for running FireStore queries.
     */
    protected abstract fun getOrderBy(): Pair<String, Query.Direction>

    /**
     * Converts the map from the server, to its equivalent entity.
     */
    protected abstract fun MutableMap<String, Any?>.toEntity(id: String, syncStatus: SyncStatus): SE

    /**
     * Converts the entity to a server-friendly map.
     */
    protected abstract fun SE.toHashMap(): HashMap<String, Any?>


    /***********************************************************************************************
     * Syncable-related functions.
     **********************************************************************************************/

    override suspend fun getListChildrenOf(parentIds: Array<String>): List<SE> =
        withContext(dispatcher) {
            suspendCoroutine { cont ->
                val orderBy = getOrderBy()

                getEventsCollection(parentIds)
                    .orderBy(orderBy.first, orderBy.second)
                    .get(Source.SERVER)
                    .addOnSuccessListener {
                        val children = mutableListOf<SE>()
                        for (change in it.documentChanges) {
                            children.add(getEntityFrom(change, it))
                        }
                        cont.resume(children)
                    }
                    .addOnFailureListener {
                        it.logException(toString())
                        cont.resumeWithException(it)
                    }
            }
        }

    @ExperimentalCoroutinesApi
    override fun getStreamChildrenOf(parentIds: Array<String>): Flow<SE> =
        callbackFlow {
            val eventsCollection = try {
                getEventsCollection(parentIds)
            } catch (e: Throwable) {
                // If Firebase cannot be initialized, close the stream of data
                // flow consumers will stop collecting and the coroutine will resume
                e.logException(toString())
                close(e)
                return@callbackFlow
            }

            val orderBy = getOrderBy()

            // Registers callback to firestore, which will be called on new events
            val registration = eventsCollection
                .orderBy(orderBy.first, orderBy.second)
                .addSnapshotListener { querySnapshot, e ->
                    if (e != null) {
                        e.logException(toString())
                        return@addSnapshotListener
                    }
                    if (querySnapshot == null) {
                        return@addSnapshotListener
                    }
                    for (change in querySnapshot.documentChanges) {
                        offer(getEntityFrom(change, querySnapshot))
                    }
                }

            // The callback inside awaitClose will be executed when the flow is
            // either closed or cancelled.
            // In this case, remove the callback from Firestore
            awaitClose {
                registration.remove()
            }
        }

    override suspend fun syncChildrenOf(parentIds: Array<String>) {
        // TODO "Not yet implemented"
    }


    /***********************************************************************************************
     * CRUD functions.
     **********************************************************************************************/

    override suspend fun getCached(parentIds: Array<String>, id: String): SE =
        withContext(dispatcher) {
            suspendCoroutine { cont ->
                getEventsCollection(parentIds)
                    .document(id)
                    .get(Source.CACHE)
                    .addOnSuccessListener {
                        if (it.data != null) {
                            val syncStatus =
                                if (!it.metadata.isFromCache) SyncStatus.CLEAN
                                else SyncStatus.TO_UPDATE
                            val entity = it.data?.toEntity(id, syncStatus)
                            cont.resume(entity!!)
                        } else {
                            cont.resumeWithException(RemoteSourceFirebaseException("Received null data"))
                        }
                    }
                    .addOnFailureListener {
                        it.logException(toString())
                        cont.resumeWithException(it)
                    }
            }
        }

    @ExperimentalCoroutinesApi
    override fun getCachedAsStream(parentIds: Array<String>, id: String): Flow<SE> =
        callbackFlow {
            val eventsCollection = try {
                getEventsCollection(parentIds)
            } catch (e: Throwable) {
                // If Firebase cannot be initialized, close the stream of data
                // flow consumers will stop collecting and the coroutine will resume
                e.logException(toString())
                close(e)
                return@callbackFlow
            }

            // Registers callback to firestore, which will be called on new events
            val registration = eventsCollection
                .whereEqualTo(FieldPath.documentId(), id)
                .addSnapshotListener(MetadataChanges.INCLUDE) { querySnapshot, e ->
                    if (e != null) {
                        e.logException(toString())
                        return@addSnapshotListener
                    }
                    if (querySnapshot == null) {
                        return@addSnapshotListener
                    }
                    for (change in querySnapshot.documentChanges) {
                        offer(getEntityFrom(change, querySnapshot))
                    }
                }

            // The callback inside awaitClose will be executed when the flow is
            // either closed or cancelled.
            // In this case, remove the callback from Firestore
            awaitClose {
                registration.remove()
            }
        }

    // TODO: Return Result<String> instead of String
    override suspend fun insert(parentIds: Array<String>, entity: SE): String {
        if (entity.getSyncableStatus() != SyncStatus.TO_INSERT) {
            throw IllegalStateException()
        }
        return withContext(dispatcher) {
            return@withContext suspendCoroutine<String> { cont ->
                // Add a new document with a generated ID
                getEventsCollection(parentIds)
                    .add(entity.toHashMap())
                    .addOnSuccessListener {
                        Log.i(toString(), "Inserted ${entity.getUniqueId()}")
                        cont.resume(it.id)
                    }
                    .addOnFailureListener {
                        it.logException(toString())
                        cont.resumeWithException(it)
                    }
            }
        }
    }

    // TODO: Return Result<UNIT> instead of Unit only
    override suspend fun update(parentIds: Array<String>, entity: SE) {
        if (entity.getSyncableStatus() != SyncStatus.TO_UPDATE) {
            throw IllegalStateException()
        }
        withContext(dispatcher) {
//            suspendCoroutine { cont ->
            // Add a new document with a generated ID
            getEventsCollection(parentIds)
                .document(entity.getUniqueId())
                .set(entity.toHashMap())
                .addOnSuccessListener {
                    Log.i(toString(), "Updated ${entity.getUniqueId()}")
//                        cont.resume(it.id)
                }
                .addOnFailureListener {
                    it.logException(toString())
//                        cont.resumeWithException(it)
                    throw it
                }
//            }
        }
    }

    override suspend fun delete(parentIds: Array<String>, vararg entities: SE) {
        // SyncStatus.TO_DELETE
        TODO("Not yet implemented")
        // As per Google Firestore: Deleting collections from an Android client is not recommended.
        // Reference: https://firebase.google.com/docs/firestore/manage-data/delete-data#kotlin+ktx_2
    }


    /***********************************************************************************************
     * Other functions.
     **********************************************************************************************/

    private fun getEntityFrom(change: DocumentChange, querySnapshot: QuerySnapshot): SE {
        val syncStatus = when (change.type) {
            DocumentChange.Type.REMOVED -> SyncStatus.TO_DELETE
            DocumentChange.Type.ADDED,
            DocumentChange.Type.MODIFIED -> {
                    if (!querySnapshot.metadata.isFromCache) {
                        SyncStatus.CLEAN
                    } else {
                        when (change.type) {
                            DocumentChange.Type.ADDED -> SyncStatus.TO_INSERT
                            DocumentChange.Type.MODIFIED,
                            DocumentChange.Type.REMOVED -> SyncStatus.TO_UPDATE
                        }
                    }

            }
        }
        return change.document.data.toEntity(
            change.document.id,
            syncStatus
        )
    }

    override fun toString(): String =
        if (BuildConfig.DEBUG) "AbstractSyncableCrudRepository" else super.toString()
}