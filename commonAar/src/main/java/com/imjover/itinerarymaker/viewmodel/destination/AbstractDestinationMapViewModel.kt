/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.destination

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.manager.DestinationManager
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.view.destination.AbstractDestinationMapFragment
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import com.imjover.itinerarymaker.viewmodel.InjectableViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [AbstractDestinationMapFragment].
 */
abstract class AbstractDestinationMapViewModel : AbstractViewModel(), InjectableViewModel {

    @Inject lateinit var destinationManager: DestinationManager
    protected lateinit var parentIds: Array<String>

    protected val destinations = mutableListOf<Destination>()

    private val _markerOptions = MutableLiveData<List<MarkerOptions>>()
    internal val markerOptions: LiveData<List<MarkerOptions>> get() = _markerOptions

    private val _markerToUpdate = MutableLiveData<Consumable<MarkerOptions>>()
    internal val markerToUpdate: LiveData<Consumable<MarkerOptions>> get() = _markerToUpdate

    init {
        injectDependencies()
    }

    /**
     * Starts the initialization of this view model as triggered by the view.
     */
    fun init(parentIds: Array<String>) {
        this.parentIds = parentIds
        viewModelScope.launch {
            destinationManager.getStreamChildrenOf(parentIds).collect {
                destinations.forEachIndexed { index, destination ->
                    if (it.id == destination.id) {
                        when (it.syncStatus) {
                            SyncStatus.CLEAN,
                            SyncStatus.TO_UPDATE,
                            SyncStatus.TO_INSERT -> {
                                destinations[index] = it
                                _markerToUpdate.value = Consumable(it.toMarkerOptions())
                            }
                            // TODO : Implement deletion of marker
                            SyncStatus.TO_DELETE -> destinations.removeAt(index)
                        }
                        return@collect
                    }
                }
                // TODO : Implement sorting of destinations
                when (it.syncStatus) {
                    SyncStatus.CLEAN,
                    SyncStatus.TO_UPDATE,
                    SyncStatus.TO_INSERT -> destinations.add(it)
                    SyncStatus.TO_DELETE -> { /* Do nothing, because the item doesn't exist */ }
                }
                // FIXME: Improve speed by returning one value at a time, instead of a list
                //  Thought that means that we also need to have
                _markerOptions.value = destinations.toMarkerOptions()
            }
        }
    }

    /**
     * Retrieves the [Destination.id] of the [marker].
     *
     * @return The [Destination.id].
     */
    fun getDestinationIdOf(marker: Marker): String {
        destinations.forEach {
            val markersPosition = marker.position
            if (it.longitude == markersPosition.longitude && it.latitude == markersPosition.latitude) {
                return it.id
            }
        }
        throw IllegalStateException()
    }
}

/**
 * Converts a list of [Destination] to a list of [MarkerOptions] to be consumed by a [GoogleMap].
 */
private fun List<Destination>.toMarkerOptions(): List<MarkerOptions> {
    val markers = mutableListOf<MarkerOptions>()
    this.forEach { destination -> markers.add(destination.toMarkerOptions()) }
    return markers
}

/**
 * Converts a [Destination] to a [MarkerOptions] to be consumed by a [GoogleMap].
 */
private fun Destination.toMarkerOptions(): MarkerOptions =
    MarkerOptions().apply {
            position(LatLng(latitude, longitude))
            title(name)
            if (isVisited) {
                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            }
        }
