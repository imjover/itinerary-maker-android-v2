/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget.validator

import android.text.InputType
import android.view.View
import android.widget.TextView
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.common.di.AppComponent

/**
 * Validates a [View] and ensures that its input's value is within the required range.
 *
 * @param min The minimum required value.
 * @param max The maximum required value.
 */
class RangeValueValidator(private val min: Int, private val max: Int) : Validator {

    init {
        if (min > max) {
            throw IllegalArgumentException("Minimum value cannot be higher than maximum value.")
        }
    }

    override fun isValid(view: View): Boolean {
        when (view) {
            is TextView -> {
                if (view.inputType != InputType.TYPE_CLASS_NUMBER) {
                    throw RuntimeException("The input type of the view should be number")
                }
                val valueStr = view.text.toString().trim()
                val valueInt =
                    if (valueStr.isEmpty()) {
                        view.text = "0" // Hardcode a value of zero
                        0
                    } else {
                        valueStr.toInt()
                    }
                return valueInt in min..max
            }
        }
        throw ViewNotSupportedByValidator(view, this)
    }

    override fun getErrorMsg(): String =
        AppComponent.appContext.getString(R.string.form_validation__range_value_message, min, max)
}