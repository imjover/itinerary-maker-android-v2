/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.itinerary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.DialogItineraryFormBinding
import com.imjover.itinerarymaker.view.AbstractDialogFragment
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.FormOption
import com.imjover.itinerarymaker.view.widget.validator.FormValidator
import com.imjover.itinerarymaker.view.widget.validator.NonEmptyValidator
import com.imjover.itinerarymaker.agency.viewmodel.itinerary.ItineraryFormViewModel


/**
 * The screen that holds the form to add or edit itineraries.
 */
class ItineraryFormDialog :
    AbstractDialogFragment<ItineraryFormViewModel, DialogItineraryFormBinding>() {

    private lateinit var formValidator: FormValidator
    private lateinit var formOption: FormOption

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get the arguments passed from previous screen
        val args = ItineraryFormDialogArgs.fromBundle(requireArguments())
        formOption = args.formOption
        if (args.parentIds.isEmpty()) {
            throw IllegalStateException()
        }
        viewModel.parentIds = args.parentIds
        when (formOption) {
            FormOption.ADD -> {
                if (!args.itineraryId.isNullOrEmpty()) {
                    throw IllegalStateException()
                }
            }
            FormOption.EDIT -> {
                if (args.itineraryId.isNullOrEmpty()) {
                    throw IllegalStateException()
                }
                viewModel.getItinerary(args.itineraryId)
            }
        }

        // Setup the UI
        isCancelable = false
        dataBinding.btnSave.setOnClickListener {
            save()
        }
        dataBinding.btnCancel.setOnClickListener {
            dismiss()
        }

        // Setup validators
        val nonEmptyValidator = NonEmptyValidator()
        formValidator = FormValidator()
        formValidator.addValidator(dataBinding.etName, nonEmptyValidator)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Setup observers on view model
        viewModel.dismiss.observe(this, { itineraryId ->
            when (formOption) {
                FormOption.ADD -> {
                    val action =
                        ItineraryFormDialogDirections.actionItineraryFormDialogToDestinationsFragment(
                            viewModel.parentIds.plus(itineraryId!!),
                            true // This is a newly added item, so it is editable
                        )
                    findNavController().navigate(action)
                }
                FormOption.EDIT -> {
                    // Do nothing because the calling Fragment/Activity should collect from stream.
                }
            }
            dismiss()
        })
    }

    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Handles the saving process of the inputs.
     */
    private fun save() {
        if (!formValidator.validateAll()) {
            return
        }
        val description = dataBinding.etName.text.toString().trim()
        when (formOption) {
            FormOption.ADD -> viewModel.insert(description)
            FormOption.EDIT -> viewModel.update(description)
        }
    }

    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.dialog_itinerary_form

    override fun getViewModelClass(): Class<ItineraryFormViewModel> =
        ItineraryFormViewModel::class.java

    override fun bindLayoutAndViewModel() {
        dataBinding.viewModel = viewModel
    }
}