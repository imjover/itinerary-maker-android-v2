/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.viewmodel.notification

import com.imjover.itinerarymaker.traveler.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.traveler.view.notification.NotificationsFragment
import com.imjover.itinerarymaker.viewmodel.notification.AbstractNotificationsViewModel


/**
 * The view model of [NotificationsFragment].
 */
class NotificationsViewModel : AbstractNotificationsViewModel() {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }
}