/*
 * Copyright (c) 2021. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.itinerary

import android.Manifest
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.util.PdfWriterUtil
import com.imjover.itinerarymaker.view.AbstractRefreshableFragment
import com.imjover.itinerarymaker.viewmodel.itinerary.AbstractItinerariesViewModel
import kotlinx.coroutines.Job


abstract class AbstractItinerariesFragment<VM : AbstractItinerariesViewModel, DB : ViewDataBinding> :
    AbstractRefreshableFragment<Itinerary, VM, DB>() {

    protected var jobGetTravel: Job? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.showMessage.observe(viewLifecycleOwner, { consumable ->
            consumable.consume {
                showMessage(it)
            }
        })
        viewModel.showPDF.observe(viewLifecycleOwner, { consumable ->
            consumable.consume {
                val pdfViewerIntent = PdfWriterUtil.getPdfViewer(requireContext(), it)
                try {
                    startActivity(pdfViewerIntent)
                } catch (e: ActivityNotFoundException) {
                    showMessage(R.string.itinerary_fragment__generate_invoice__view_pdf__failed__message)
                }
            }
        })
    }

    override fun onPause() {
        super.onPause()
        jobGetTravel?.let {
            it.cancel()
            jobGetTravel = null
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_FOR_WRITE_EXTERNAL_STORAGE -> {
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)
                ) {
                    startGenerationOfInvoice()
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                    showMessage(R.string.error_message__permission_required__write_storage__message)
                }
                return
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    protected fun generateInvoice() {
        // Check if user approves permission
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED -> {
                startGenerationOfInvoice()
            }
            shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
                // In an educational UI, explain to the user why your app requires this
                // permission for a specific feature to behave as expected. In this UI,
                // include a "cancel" or "no thanks" button that allows the user to
                // continue using your app without granting the permission.
                showMessage(R.string.error_message__permission_required__write_storage__message)
            }
            else -> {
                // Directly ask for the permission.
                requestPermissions(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_CODE_FOR_WRITE_EXTERNAL_STORAGE
                )
            }
        }
    }

    private fun startGenerationOfInvoice() {
        // Setup dialog
        val dialog = ProgressDialog(requireContext())
        dialog.setButton(
            DialogInterface.BUTTON_NEGATIVE, getString(android.R.string.cancel)
        ) { _, _ ->
            viewModel.tryToCancelGeneration()
            // Dialog should STILL be visible at this point,
            // and we wait for the ViewModel#dialogDismiss to be emitted
        }
        dialog.show()
        viewModel.generateInvoice()

        // Observe to dialog's observables
        val messageObserver = Observer<Int> {
            dialog.setMessage(getString(it))
        }
        viewModel.dialogMessage.observe(viewLifecycleOwner, messageObserver)
        viewModel.dialogDismiss.observe(viewLifecycleOwner, object : Observer<Boolean> {
            override fun onChanged(dismiss: Boolean) {
                if (dismiss) {
                    // Remove all observer since we will close now the dialog
                    viewModel.dialogMessage.removeObserver(messageObserver)
                    viewModel.dialogDismiss.removeObserver(this)
                    dialog.dismiss()
                }
            }
        })
    }

    protected fun showMessage(@StringRes message: Int) {
        showMessage(getString(message))
    }

    abstract fun showMessage(message: String)

    companion object {
        private const val REQUEST_CODE_FOR_WRITE_EXTERNAL_STORAGE = 100
    }
}