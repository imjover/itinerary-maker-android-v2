/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.mock

import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.util.IdGenerator
import com.imjover.itinerarymaker.source.remote.ItineraryRemoteSource
import java.util.*

internal class ItineraryRemoteSourceMock : AbstractRemoteSyncableChildSourceMock<Itinerary, String>(),
    ItineraryRemoteSource {

    companion object {
        val ITINERARY_1_of_TRAVEL_1 = Itinerary(
            "1",
            TravelRemoteSourceMock.TRAVEL_1_of_TRAVELER_1.id,
            "Day 1: Vigan",
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val ITINERARY_2_of_TRAVEL_1 = Itinerary(
            "2",
            TravelRemoteSourceMock.TRAVEL_1_of_TRAVELER_1.id,
            "Day 2: Pagudpud",
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
        val ITINERARY_3_of_TRAVEL_1 = Itinerary(
            "3",
            TravelRemoteSourceMock.TRAVEL_1_of_TRAVELER_1.id,
            "Day 3: Laoag",
            Date(),
            UserRemoteSourceMock.TRAVELER_1.id,
            SyncStatus.CLEAN
        )
    }


    /***********************************************************************************************
     * Required methods of [AbstractRemoteSyncableChildSourceMock]
     **********************************************************************************************/

    override fun getInitialValueOfEntities(): Map<String, Itinerary> =
        mapOf(
            ITINERARY_1_of_TRAVEL_1.id to ITINERARY_1_of_TRAVEL_1,
            ITINERARY_2_of_TRAVEL_1.id to ITINERARY_2_of_TRAVEL_1,
            ITINERARY_3_of_TRAVEL_1.id to ITINERARY_3_of_TRAVEL_1
        )

    override fun getEntitiesToUpdateOnFirstRefresh(): Map<String, Itinerary> {
        val cachedItinerary = cachedEntities[ITINERARY_1_of_TRAVEL_1.id]!!
        val updatedItinerary = Itinerary(
            cachedItinerary.id,
            cachedItinerary.travelId,
            "Day 1: Vigan (refreshed)",
            cachedItinerary.lastUpdated,
            cachedItinerary.lastUpdatedBy,
            cachedItinerary.syncStatus
        )
        return mapOf(
            updatedItinerary.id to updatedItinerary
        )
    }

    override fun generateIdFor(entity: Itinerary): String =
        IdGenerator.generate("IRMock", entity.lastUpdatedBy)
}
