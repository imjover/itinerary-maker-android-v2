/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.expense

import android.view.View
import androidx.navigation.findNavController
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.view.FormOption
import com.imjover.itinerarymaker.view.expense.AbstractExpenseAdapter


/**
 * List adapter of [Expense] item.
 */
internal class ExpenseAdapter(
    private val parentIds: Array<String>, private val isEditable: () -> Boolean
) : AbstractExpenseAdapter() {

    override fun getOnClickListener(id: String): View.OnClickListener? =
        if (isEditable()) {
            View.OnClickListener {
                val action =
                    ExpensesFragmentDirections.actionExpensesFragmentToExpenseFormDialog(
                        FormOption.EDIT,
                        parentIds,
                        id
                    )
                it.findNavController().navigate(action)
            }
        } else null
}