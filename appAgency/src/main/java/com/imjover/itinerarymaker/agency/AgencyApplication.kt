/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency

import com.imjover.itinerarymaker.AbstractBaseApplication
import com.imjover.itinerarymaker.common.di.ApplicationType

/**
 * The application object.
 */
class AgencyApplication : AbstractBaseApplication(ApplicationType.AppAgency, BuildConfig.VERSION_NAME)