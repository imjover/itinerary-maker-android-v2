/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.destination

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavDirections
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.imjover.itinerarymaker.traveler.R
import com.imjover.itinerarymaker.traveler.databinding.FragmentDestinationsBinding
import com.imjover.itinerarymaker.traveler.viewmodel.destination.DestinationsViewModel
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.destination.AbstractDestinationsFragment


/**
 * The screen that holds all destinations.
 */
class DestinationsFragment :
    AbstractDestinationsFragment<DestinationsViewModel, FragmentDestinationsBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get arguments
        val args = DestinationsFragmentArgs.fromBundle(requireArguments())
        viewModel.parentIds = args.parentIds

        // Instantiate AbstractDestinationsFragment's views
        bottomSheetBehavior =
            BottomSheetBehavior.from(dataBinding.bottomSheetContainer.bsDestination)
        bottomSheet = dataBinding.bottomSheetContainer
        pager = dataBinding.pager
        tabLayout = dataBinding.tabLayout

        return view
    }

    /***********************************************************************************************
     * Required functions of [AbstractDestinationsFragment].
     **********************************************************************************************/

    override fun getNavToExpenses(id: String, name: String): NavDirections =
        DestinationsFragmentDirections.actionDestinationsFragmentToExpensesFragment(
            viewModel.parentIds.plus(id), name
        )

    override fun getPagerAdapter(): FragmentStateAdapter =
        DestinationsTabAdapter(this, viewModel.parentIds)

    override fun onBottomSheetStateChanged(newState: Int) {
        // Do nothing
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getViewModelClass(): Class<DestinationsViewModel> =
        DestinationsViewModel::class.java

    override fun getLayout(): Int =
        R.layout.fragment_destinations
}