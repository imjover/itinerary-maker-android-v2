/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.destination

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imjover.itinerarymaker.databinding.RowDestinationBinding
import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.helper.toImageRes
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter

/**
 * List adapter of [Destination] item.
 *
 * @param destinationsHandlerListener The parent fragment that handles all destinations.
 */
internal class DestinationListAdapter(private val destinationsHandlerListener: DestinationsHandlerListener) :
    UpdatableRecyclerViewAdapter<Destination, RowDestinationBinding>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterViewHolder<RowDestinationBinding> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = RowDestinationBinding.inflate(layoutInflater, parent, false)
        return AdapterViewHolder(itemBinding)
    }

    override fun onBindViewHolder(
        holder: AdapterViewHolder<RowDestinationBinding>,
        position: Int
    ) {
        val destination = getData(position)
        val showDivider = position + 1 < itemCount
        // Bind instances of [Travel] to the row.
        holder.binding.destination = destination
        holder.binding.ivDestinationType.setImageResource(destination.destinationType.toImageRes())
        holder.binding.ivTransportationMode.setImageResource(destination.transportationMode.toImageRes())
        holder.binding.divider.visibility = if (showDivider) View.VISIBLE else View.GONE
        holder.binding.container.setOnClickListener {
            destinationsHandlerListener.showBottomSheetOf(destination.id)
        }
        holder.binding.executePendingBindings()
    }

    override fun getOnClickListener(id: String): View.OnClickListener? {
        TODO("Not yet implemented")
    }
}