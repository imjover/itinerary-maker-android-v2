/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source.di


/**
 * Holds the static instance of [RepositoryComponent].
 */
internal class RepositoryComponentFactory private constructor() {

    companion object {

        private lateinit var repositoryComponent: RepositoryComponent

        fun singleton(): RepositoryComponent {
            if (!this::repositoryComponent.isInitialized) {
                repositoryComponent = DaggerRepositoryComponent.create()
            }
            return repositoryComponent
        }
    }
}