/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.util

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

/**
 * Retrieves the observer that observes the result from a target [Fragment].
 */
fun <T> Fragment.getNavigationResult(key: String = "result") =
    findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<T>(key)

/**
 * Sets the value of the observer from an observing [Fragment].
 */
fun <T> Fragment.setNavigationResult(result: T, key: String = "result") {
    findNavController().previousBackStackEntry?.savedStateHandle?.set(key, result)
}