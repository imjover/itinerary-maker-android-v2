/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel


/**
 * The base class of activities.
 */
abstract class AbstractActivity<VM : AbstractViewModel, DB : ViewDataBinding> :
    AppCompatActivity(), DataBindable<VM> {

    protected lateinit var viewModel: VM
        private set
    protected lateinit var dataBinding: DB
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this).get(getViewModelClass())
        dataBinding = DataBindingUtil.setContentView(this, getLayout())
        dataBinding.lifecycleOwner = this
        bindLayoutAndViewModel()
    }
}