/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.firebase.internal

import android.net.Uri
import android.util.Log
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.FirebaseFunctions
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.repository.UserRepository
import com.imjover.itinerarymaker.repository.firebase.BuildConfig
import com.imjover.itinerarymaker.repository.firebase.RemoteSourceFirebaseException
import com.imjover.itinerarymaker.util.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


internal const val USERS_COLLECTION = "users"

internal class UserRepositoryImpl(
    private val dispatcher: CoroutineDispatcher,
    private val auth: FirebaseAuth,
    private val functions: FirebaseFunctions,
    private val db: FirebaseFirestore
) : UserRepository {

    private var user: User? = null

    override suspend fun findUserByLogin(
        username: String,
        password: String
    ): Result<User> =
        withContext(dispatcher) {
            if (username.isNotEmpty() || password.isNotEmpty()) {
                throw IllegalStateException()
            }
            user?.let {
                return@withContext Result.success(it)
            }
            // Since the login is handled by Firebase-Auth-UI, let's fetch the user instead
            val firebaseUser = auth.currentUser!!
            val claims = suspendCoroutine<Map<String, Any>> { cont ->
                firebaseUser.getIdToken(false)
                    .addOnSuccessListener {
                        cont.resume(it.claims)
                    }
            }
            val userType: UserType
            val isApproved: Boolean
            if (!claims.containsKey("userType") && !claims.containsKey("isApproved")) {
                // By default, user is treated as traveler.
                // But both claims, userType and isApproved, SHOULD not be found.
                userType = UserType.TRAVELER
                isApproved = true
            } else {
                // Else, manually retrieved the claims.
                userType =
                    if (claims.containsKey("userType")) {
                        UserType.fromId(claims["userType"].toString().toInt())
                    } else {
                        val ex = RemoteSourceFirebaseException("Invalid user type.")
                        Log.w(toString(), ex)
                        throw ex
                    }
                isApproved =
                    if (claims.containsKey("isApproved")) {
                        claims["isApproved"].toString().toBoolean()
                    } else {
                        false
                    }
            }

            // Create user
            val uid = firebaseUser.uid
            var username: String = firebaseUser.email ?: uid
            var name: String = firebaseUser.displayName ?: username
            var photoUrl = firebaseUser.photoUrl
            // Try to get user's info from other provider, if available.
            for (profile in firebaseUser.providerData) {
                profile.email?.let { username = it }
                profile.displayName?.let { name = it }
                profile.photoUrl.let { photoUrl = it }
            }
            user = User(uid, username, name, photoUrl, userType, isApproved)
            return@withContext Result.success(user!!)
        }


    override suspend fun logout(): Result<Unit> =
        withContext(dispatcher) {
            suspendCoroutine { cont ->
                AuthUI.getInstance()
                    .signOut(AppComponent.appContext)
                    .addOnSuccessListener {
                        user = null
                        cont.resume(Result.success(Unit))
                    }
                    .addOnFailureListener {
                        it.logException(toString())
                        cont.resume(Result.failure(it))
                    }
            }
        }

    override suspend fun register(userType: UserType): Result<String> =
        withContext(dispatcher) {
            val data: MutableMap<String, Any> = HashMap()
            data["userType"] = userType.id

            return@withContext suspendCoroutine { cont ->
                functions
                    .getHttpsCallable("users-requestRegistration")
                    .call(data)
                    .continueWith { task ->
                        task.result?.data as String
                    }
                    .addOnSuccessListener {
                        cont.resume(Result.success(it))
                    }
                    .addOnFailureListener {
                        it.logException(toString())
                        cont.resume(Result.failure(it))
                    }
            }
        }

    override suspend fun approveRegistration(user: User): Result<String> {
        val data: MutableMap<String, Any> = HashMap()
        data["uid"] = user.id

        return suspendCoroutine { cont ->
            functions
                .getHttpsCallable("users-approveRegistration")
                .call(data)
                .continueWith { task ->
                    task.result?.data as String
                }
                .addOnSuccessListener {
                    cont.resume(Result.success(it))
                }
                .addOnFailureListener {
                    it.logException(toString())
                    cont.resume(Result.failure(it))
                }
        }
    }

    override fun getLoggedInUser(): User =
        user!!

    override suspend fun getUser(id: String): Result<User> =
        withContext(dispatcher) {
            suspendCoroutine { cont ->
                db.collection(USERS_COLLECTION)
                    .document(id)
                    .get()
                    .addOnSuccessListener { document ->
                        if (document.data != null) {
                            cont.resume(Result.success(document.data!!.toUser(document.id)))
                        } else {
                            cont.resume(Result.failure(RemoteSourceFirebaseException("User not found")))
                        }
                    }
                    .addOnFailureListener {
                        it.logException(toString())
                        cont.resume(Result.failure(it))
                    }
            }
        }

    @ExperimentalCoroutinesApi
    override fun getUsers(nameQuery: String, vararg userTypes: UserType): Flow<User> =
        callbackFlow {
            val eventsCollection = try {
                db.collection(USERS_COLLECTION)
            } catch (e: Throwable) {
                // If Firebase cannot be initialized, close the stream of data
                // flow consumers will stop collecting and the coroutine will resume
                close(e)
                return@callbackFlow
            }
            val userTypeIds: List<Int> = userTypes.map { it.id }

            // Registers callback to firestore, which will be called on new events
            val registration = eventsCollection
                .whereIn("user_type", userTypeIds)
                // FIXME: this causes "FAILED_PRECONDITION: The query requires an index."
                // .orderBy("name", Query.Direction.ASCENDING)
                .addSnapshotListener { querySnapshot, e ->
                    if (e != null) {
                        e.logException(toString())
                        return@addSnapshotListener
                    }
                    if (querySnapshot == null) {
                        return@addSnapshotListener
                    }
                    for (change in querySnapshot.documentChanges) {
                        when (change.type) {
                            DocumentChange.Type.ADDED,
                            DocumentChange.Type.MODIFIED -> {
                                change.document.data.let {
                                    offer(it.toUser(change.document.id))
                                }
                            }
                            DocumentChange.Type.REMOVED -> {
                                // TODO: "To be implemented"
                            }
                        }
                    }
                }

            // The callback inside awaitClose will be executed when the flow is
            // either closed or cancelled.
            // In this case, remove the callback from Firestore
            awaitClose {
                registration.remove()
            }
        }

    override suspend fun updateDeviceToken(token: String): Result<Unit> =
        withContext(dispatcher) {
            val fields = mapOf(
                "device_token" to token
            )
            return@withContext suspendCoroutine { cont ->
                db.collection(USERS_COLLECTION).document(getLoggedInUser().id)
                    .update(fields)
                    .addOnSuccessListener {
                        Log.i(toString(), "Updated device token")
                        cont.resume(Result.success(Unit))
                    }
                    .addOnFailureListener {
                        it.logException(toString())
                        cont.resume(Result.failure(it))
                    }
            }
        }


    /***********************************************************************************************
     * Other functions.
     **********************************************************************************************/

    private fun MutableMap<String, Any?>.toUser(id: String): User =
        User(
            id,
            this["username"] as String,
            this["name"] as String,
            this["photo_url"]?.let { photoUrl -> Uri.parse(photoUrl as String) },
            UserType.fromId((this["user_type"] as Long).toInt()),
            this["is_approved"] as Boolean
        )

    override fun toString(): String =
        if (BuildConfig.DEBUG) "UserRepositoryImpl" else super.toString()
}