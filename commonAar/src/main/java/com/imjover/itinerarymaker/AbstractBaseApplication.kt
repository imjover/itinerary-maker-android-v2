/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker

import androidx.multidex.MultiDexApplication
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.common.di.ApplicationType

/**
 * The base application.
 *
 * @param appType The type of this application.
 * @param appVersion The version of this application.
 */
abstract class AbstractBaseApplication(
    private val appType: ApplicationType,
    private val appVersion: String
) : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        initDependencies()
    }

    /**
     * Initializes the application-level components.
     */
    private fun initDependencies() {
        AppComponent.init(this, appType, appVersion)
    }
}