/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.common.di

/**
 * The type of application.
 */
enum class ApplicationType {
    AppAgency,
    AppTraveler
}