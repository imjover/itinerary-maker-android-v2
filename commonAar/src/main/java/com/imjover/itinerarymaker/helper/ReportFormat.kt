/*
 * Copyright (c) 2021. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.helper

import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.util.toStringMonetary
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*


/**
 * Helper class to format reports.
 */
class ReportFormat {

    companion object {
        private val TRAVEL_DATE_FORMAT = SimpleDateFormat("MMM dd, yyyy")
        private val ITINERARY_DATE_FORMAT = SimpleDateFormat("MMM dd, yyyy h:mm a")

        /**
         * Formats the invoice.
         */
        fun invoiceToPDF(
            traveler: User?,
            travel: Travel,
            allTravelData: Map<Itinerary, Map<Destination, List<Expense>>>
        ): StringBuilder {
            val context = AppComponent.appContext
            return StringBuilder().apply {
                append("<html>")
                append("<head>")
                append(
                    "<style>\n" +
                            "table, th, td {\n" +
                            "  border: 1px solid black;\n" +
                            "  border-collapse: collapse;\n" +
                            "  vertical-align:top;\n" +
                            "}\n" +
                            "</style>\n"
                )
                append("</head>")
                append("<body>")


                append("<p>")
                append("<br/><br/>")
                append("Email: <u>rayton.travelandtours@gmail.com</u><br/>")
                append("FB: <u>www.facebook.com/rayton.travelandtours</u><br/>")
                append("(046)4379085 / +639171034888 / +639495934660")
                append("</p>")
                append("<h1>Invoice</h1>")

                append("<p>")
                traveler?.let {
                    append("Passenger: ${it.name}<br/>")
                }
                travel.let {
                    append(
                        "Date of Tour: " +
                                "${TRAVEL_DATE_FORMAT.format(it.startDate)} - " +
                                "${TRAVEL_DATE_FORMAT.format(it.endDate)}<br/>"
                    )
                    append("Status: ${context.getString(it.status.toStringRes())}<br/>")
                    it.paymentMode?.let { paymentMode ->
                        append("Payment: ${context.getString(paymentMode.toStringRes())}<br/>")
                    }
                }
                append("</p>")
                append("<br/>")

                // Format table of invoice
                append("<h2>${travel.name}</h2>")
                append("<table width=\"100%\">")
                append("<tr>")
                append("<th>Date & time</th>")
                append("<th>Itinerary</th>")
                append("<th>Amount</th>")
                append("</tr>")
                var totalAmount = BigDecimal(0)
                for ((itinerary, destinations) in allTravelData) {
                    append("<tr>")
                    append("<td></td>")
                    append("<td><b>${itinerary.name}</b></td>")
                    append("<td></td>")
                    append("</tr>")

                    for ((destination, expenses) in destinations) {
                        append("<tr>")
                        append("<td>${ITINERARY_DATE_FORMAT.format(destination.timestamp)}</td>")

                        append("<td>${destination.name}<br/>")
                        destination.destinationType.let {
                            if (it != DestinationType.DEFAULT) {
                                append("${context.getString(it.toStringRes())}")
                            }
                        }
                        destination.transportationMode.let {
                            if (it != TransportationMode.WALK) {
                                append(" | ${context.getString(it.toStringRes())}")
                            }
                        }
                        append("</td>")
                        append("<td></td>")
                        append("</tr>")

                        expenses.forEach { expense ->
                            append("<tr>")
                            append("<td style='border-top:none'></td>")
                            append("<td>${expense.name}")
                            expense.remarks?.let {
                                append("<br/><i>$it</i>")
                            }
                            append("</td>")
                            append("<td align=\"right\">${expense.price.toStringMonetary()}</td>")
                            totalAmount = totalAmount.add(expense.price)
                            append("</tr>")
                        }
                    }
                }
                append("<tr>")
                append("<th>Total Amount</th>")
                append("<th></th>")
                append("<th align=\"right\">${totalAmount.toStringMonetary()}</th>")
                append("</tr>")
                append("</table>")


                append("</body></html>")
            }
        }

        /**
         * Format the sales report
         */
        fun salesReportToCSV(
            data: Map<ExpenseType, ArrayList<BigDecimal>>,
            startDate: Date,
            endDate: Date
        ): StringBuilder =
            StringBuilder().apply {
                var totalCapital = BigDecimal.ZERO
                var totalPrice = BigDecimal.ZERO
                val dateFormat = SimpleDateFormat("MMM dd, yyyy")

                append("Start date:,\"${dateFormat.format(startDate)}\"\n")
                append("End date:,\"${dateFormat.format(endDate)}\"\n")
                append('\n')
                append("Expense Type,Capital,Price,Profit\n")
                data.forEach {
                    append(AppComponent.appContext.getString(it.key.toStringRes()))
                    append(',')
                    val capital = it.value[0]
                    append("\"${capital.toStringMonetary()}\"")
                    append(',')
                    val price = it.value[1]
                    append("\"${price.toStringMonetary()}\"")
                    append(',')
                    append("\"${price.minus(capital).toStringMonetary()}\"")
                    append('\n')

                    totalCapital = totalCapital.add(capital)
                    totalPrice = totalPrice.add(price)
                }
                append("Total,")
                append("\"${totalCapital.toStringMonetary()}\"")
                append(',')
                append("\"${totalPrice.toStringMonetary()}\"")
                append(',')
                append("\"${totalPrice.minus(totalCapital).toStringMonetary()}\"")
            }
    }
}