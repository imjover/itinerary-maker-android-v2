/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository

import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.Entity
import com.imjover.itinerarymaker.util.Result

/**
 * Represents the repository data source of [Destination].
 */
interface DestinationRepository :
    CrudRepository<Destination>,
    SyncableRepository<Destination> {

    /**
     * Update the is visited column of this destination.
     *
     * @param parentIds The IDs of the parents, starting from the root.
     * @param id The ID of the [Entity].
     * @param isVisited Whether this destination is visited, else, false.
     */
    suspend fun updateIsVisited(
        parentIds: Array<String>,
        id: String,
        isVisited: Boolean
    ): Result<Unit>
}