/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.destination

import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.destination.DestinationsFragment
import com.imjover.itinerarymaker.viewmodel.destination.AbstractDestinationsViewModel


/**
 * The view model of [DestinationsFragment].
 */
class DestinationsViewModel : AbstractDestinationsViewModel() {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }
}