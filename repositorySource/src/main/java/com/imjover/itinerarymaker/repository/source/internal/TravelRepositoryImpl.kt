/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source.internal

import android.util.Log
import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.entity.Status
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.repository.BuildConfig
import com.imjover.itinerarymaker.repository.TravelRepository
import com.imjover.itinerarymaker.source.local.TravelLocalSource
import com.imjover.itinerarymaker.source.remote.TravelRemoteSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.util.*


internal class TravelRepositoryImpl(
    localSource: TravelLocalSource,
    remoteSource: TravelRemoteSource,
    dispatcher: CoroutineDispatcher
) : AbstractSyncableCrudRepository<Travel, String, TravelLocalSource, TravelRemoteSource>(
    localSource,
    remoteSource,
    dispatcher
), TravelRepository {

    override suspend fun getCachedStatus(travelId: String): Status =
        localSource.getStatus(travelId)

    override suspend fun cancel(travelId: String) {
        withContext(dispatcher) {
            try {
                remoteSource.cancel(travelId)
            } catch (exception: Exception) {
                // FIXME: Change exception to remote-caused exception ONLY e.g. no internet
                Log.e(toString(), exception.message, exception)
                // Stop cancellation
                return@withContext
            }
            localSource.cancel(travelId)
        }
    }

    override suspend fun updatePaymentMode(
        travelId: String,
        paymentMode: PaymentMode,
        lastUpdatedBy: String,
        lastUpdated: Date
    ) {
        withContext(dispatcher) {
            try {
                remoteSource.updatePaymentMode(travelId, paymentMode)
            } catch (exception: Exception) {
                // FIXME: Change exception to remote-caused exception ONLY e.g. no internet
                Log.e(toString(), exception.message, exception)
                // Stop cancellation
                return@withContext
            }
            localSource.updatePaymentMode(travelId, paymentMode, lastUpdatedBy, lastUpdated)
        }
    }

    override fun toString(): String {
        return if (BuildConfig.DEBUG)
            "TravelRepositoryImpl"
        else
            super.toString()
    }
}