/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.traveler

import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.FragmentTravelersBinding
import com.imjover.itinerarymaker.agency.viewmodel.traveler.TravelersViewModel
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.view.AbstractFragment
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * The screen that holds all travelers.
 */
class TravelersFragment : AbstractFragment<TravelersViewModel, FragmentTravelersBinding>() {

    private lateinit var travelers: Flow<User> // There is NO un-collect in Flow, so let's cache the flow to overwrite it

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)

        // Setup RecyclerView
        val layoutManager = LinearLayoutManager(context)
        dataBinding.rvTravelerList.layoutManager = layoutManager
        dataBinding.rvTravelerList.adapter = TravelerAdapter()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchTraveler("")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.travelers_menu, menu)

        val menuItem = menu.findItem(R.id.notification_menu)
        val actionView = menuItem.actionView
        val tvBadge: TextView = actionView.findViewById(R.id.tv_badge)
        actionView.setOnClickListener { onOptionsItemSelected(menuItem) }

        lifecycleScope.launch {
            viewModel.getNewNotificationCount().collect {
                tvBadge.text = it.toString()
                tvBadge.visibility = if (it > 0) View.VISIBLE else View.GONE
            }
        }

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchViewMenuItem = menu.findItem(R.id.search_menu)
        val searchView = searchViewMenuItem.actionView as SearchView
        searchView.queryHint =
            resources.getString(R.string.traveler_fragment__search_travel__menu_hint)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { searchTraveler(it) }

                // FIXME: SearchView is not closing :(
                //  Found below solution (not working) on https://stackoverflow.com/questions/23072123/actionbar-search-will-not-close-after-search
                searchView.clearFocus()
                searchView.isIconified = true
                searchViewMenuItem.collapseActionView()
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                // Do nothing
                return false
            }
        })
        searchView.setOnCloseListener {
            searchTraveler("")
            false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.notification_menu -> {
                val action = TravelersFragmentDirections.actionTravelersFragmentToNotificationsFragment()
                findNavController().navigate(action)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    /***********************************************************************************************
     * Private functions.
     **********************************************************************************************/

    /**
     * Handle searching for traveler
     */
    private fun searchTraveler(name: String) {
        val adapter = dataBinding.rvTravelerList.adapter as TravelerAdapter
        adapter.list.clear()
        adapter.notifyDataSetChanged()

        lifecycleScope.launch {
            travelers = viewModel.getTravelers(name)
            travelers.collect {
                adapter.updateData(it)
            }
        }
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_travelers

    override fun getViewModelClass(): Class<TravelersViewModel> =
        TravelersViewModel::class.java

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }
}