/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.viewmodel.about

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.common.di.ApplicationType
import com.imjover.itinerarymaker.view.about.AboutFragment
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel


/**
 * The view model of [AboutFragment]
 */
class AboutViewModel : AbstractViewModel() {

    private val _appVersion = MutableLiveData("")
    val appVersion: LiveData<String> get() = _appVersion

    private val _appType = MutableLiveData(-1)
    val appType: LiveData<Int> get() = _appType

    /**
     * Loads all info about this app.
     */
    fun loadAboutInfo() {
        _appVersion.value = "v${AppComponent.appVersion}"
        _appType.value =
            if (AppComponent.appType == ApplicationType.AppAgency)
                R.string.app_type__agency
            else
                R.string.app_type__traveler
    }
}
