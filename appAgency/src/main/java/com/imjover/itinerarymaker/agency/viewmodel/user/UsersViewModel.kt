/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.user

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.user.UsersFragment
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.manager.UserManager
import com.imjover.itinerarymaker.util.Consumable
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [UsersFragment].
 */
class UsersViewModel : AbstractViewModel() {

    @Inject lateinit var userManager: UserManager

    val isLoading = ObservableBoolean()

    private val _showMessage: MutableLiveData<Consumable<String>> = MutableLiveData()
    val showMessage: LiveData<Consumable<String>> get() = _showMessage

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Retrieves all travelers.
     *
     * @param nameQuery The name to be searched for.
     * @return Stream of [User]s.
     */
    fun getUsers(nameQuery: String): Flow<User> =
        userManager.getUsers(nameQuery, UserType.ADMIN, UserType.AGENT, UserType.ACCOUNTANT)

    /**
     * Approves a user's registration.
     *
     * @param user The user to be registered.
     */
    fun approveRegistration(user: User) {
        viewModelScope.launch {
            isLoading.set(true)
            _showMessage.value = userManager.approveRegistration(user)
                .map { Consumable(it) }
                .materialize{ Consumable(it.getMessageForUI()) }
            isLoading.set(false)
        }
    }
}