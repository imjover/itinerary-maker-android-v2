/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.user

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.RowUserBinding
import com.imjover.itinerarymaker.agency.viewmodel.user.UsersViewModel
import com.imjover.itinerarymaker.entity.User
import com.imjover.itinerarymaker.helper.toStringRes
import com.imjover.itinerarymaker.util.image.CircleTransform
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter
import com.squareup.picasso.Picasso


/**
 * List adapter of [User] item.
 */
class UserAdapter(private val context: Context, private val viewModel: UsersViewModel) :
    UpdatableRecyclerViewAdapter<User, RowUserBinding>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterViewHolder<RowUserBinding> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = RowUserBinding.inflate(layoutInflater, parent, false)
        return AdapterViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: AdapterViewHolder<RowUserBinding>, position: Int) {
        val user = getData(position)
        val showDivider = position + 1 < itemCount
        // Bind instances of [Travel] to the row.
        holder.binding.user = user
        holder.binding.tvUserType.setText(user.userType.toStringRes())
        holder.binding.divider.visibility = if (showDivider) View.VISIBLE else View.GONE
        Picasso.get()
            .load(user.photoUrl)
            .placeholder(R.drawable.ic_user_24)
            .transform(CircleTransform())
            .into(holder.binding.ivProfilePic)
        holder.binding.container.setOnClickListener(if (user.isApproved) null else
            View.OnClickListener {
                val builder = AlertDialog.Builder(context)
                builder.setMessage(R.string.users_fragment__approve_user__question)
                    .setPositiveButton(R.string.button__approve) { _, _ ->
                        viewModel.approveRegistration(user)
                    }
                    .setNegativeButton(android.R.string.cancel) { _, _ ->
                        // Do nothing
                    }
                    .show()
            }
        )
        holder.binding.executePendingBindings()
    }

    override fun getOnClickListener(id: String): View.OnClickListener? =
        null // This function is NOT used here so return null
}