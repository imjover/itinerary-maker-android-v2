/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.invoice

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.agency.view.invoice.PaymentFormDialog
import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.manager.TravelManager
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * The view model of [PaymentFormDialog].
 */
class PaymentFormViewModel : AbstractViewModel() {

    @Inject lateinit var travelManager: TravelManager

    private val _dismiss: MutableLiveData<Void> = MutableLiveData()
    val dismiss: LiveData<Void>
        get() = _dismiss
    val isLoading = ObservableBoolean()

    lateinit var parentIds: Array<String>
    lateinit var parentTravelId: String

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Updates the payment mode of the current travel.
     *
     * @param paymentMode The payment mode.
     */
    fun update(paymentMode: PaymentMode) {
        viewModelScope.launch {
            isLoading.set(true)
            travelManager.markAsPaid(parentIds, parentTravelId, paymentMode)
            _dismiss.value = null
            isLoading.set(false)
        }
    }
}