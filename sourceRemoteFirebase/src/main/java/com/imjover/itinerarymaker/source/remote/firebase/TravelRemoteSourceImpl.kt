/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.firebase

import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.source.remote.TravelRemoteSource

class TravelRemoteSourceImpl : TravelRemoteSource {

    override suspend fun getListChildrenOf(parentId: String): List<Travel> {
        // FIXME
        return listOf()
    }

    override suspend fun insert(entity: Travel): String {
        TODO("Not yet implemented")
    }

    override suspend fun update(entity: Travel) {
        TODO("Not yet implemented")
    }

    override suspend fun delete(entity: Travel) {
        TODO("Not yet implemented")
    }

    override suspend fun cancel(travelId: String) {
        TODO("Not yet implemented")
    }

    override suspend fun updatePaymentMode(travelId: String, paymentMode: PaymentMode) {
        TODO("Not yet implemented")
    }
}