/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.view.expense

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.imjover.itinerarymaker.agency.R
import com.imjover.itinerarymaker.agency.databinding.FragmentExpensesBinding
import com.imjover.itinerarymaker.agency.viewmodel.expense.ExpensesViewModel
import com.imjover.itinerarymaker.entity.Expense
import com.imjover.itinerarymaker.view.AbstractFragment
import com.imjover.itinerarymaker.view.AbstractRefreshableFragment
import com.imjover.itinerarymaker.view.FormOption
import com.imjover.itinerarymaker.view.widget.UpdatableRecyclerViewAdapter


/**
 * The screen that holds all expenses.
 */
class ExpensesFragment :
    AbstractRefreshableFragment<Expense, ExpensesViewModel, FragmentExpensesBinding>() {

    private var isEditable = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Get the arguments passed from previous screen
        val args = ExpensesFragmentArgs.fromBundle(requireArguments())
        viewModel.parentIds = args.parentIds
        requireActivity().title = args.destinationDesc
        isEditable = args.isEditable

        // Setup the UI
        if (isEditable) {
            dataBinding.fabAddExpense.setOnClickListener {
                val action =
                    ExpensesFragmentDirections.actionExpensesFragmentToExpenseFormDialog(
                        FormOption.ADD,
                        viewModel.parentIds,
                        null
                    )
                findNavController().navigate(action)
            }
        } else {
            dataBinding.fabAddExpense.visibility = View.GONE
        }
        return view
    }


    /***********************************************************************************************
     * Required functions of [AbstractFragment].
     **********************************************************************************************/

    override fun getLayout(): Int =
        R.layout.fragment_expenses

    override fun getViewModelClass(): Class<ExpensesViewModel> =
        ExpensesViewModel::class.java

    override fun bindLayoutAndViewModel() {
        // Do nothing
    }

    override fun getRecyclerView(): RecyclerView =
        dataBinding.rvExpenseList

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout =
        dataBinding.swipeContainer

    override fun getAdapter(): UpdatableRecyclerViewAdapter<Expense, *> =
        ExpenseAdapter(viewModel.getParentIds()) { isEditable }
}