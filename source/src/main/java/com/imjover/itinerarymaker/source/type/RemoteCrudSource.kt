/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.type

import com.imjover.itinerarymaker.entity.Entity

/**
 * Defines remote sources that can do CRUD (create, read, update, delete), but except READING.
 */
interface RemoteCrudSource<E: Entity> {

    /**
     * Inserts a new cache of [Entity].
     *
     * @param entity The [Entity] to be inserted.
     * @return The unique ID of the inserted [Entity].
     */
    suspend fun insert(entity: E): String

    /**
     * Updates the cache of [Entity].
     *
     * @param entity The [Entity] to be updated.
     */
    suspend fun update(entity: E)

    /**
     * Deletes the cache of [Entity].
     *
     * @param entity The [Entity] to be deleted.
     */
    suspend fun delete(entity: E)
}