/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.view.widget.validator

import android.view.View
import android.widget.TextView
import com.imjover.itinerarymaker.R
import com.imjover.itinerarymaker.common.di.AppComponent

/**
 * Validates a [View] and ensures that it contains an input.
 */
class NonEmptyValidator : Validator {

    override fun isValid(view: View): Boolean {
        when (view) {
            is TextView -> {
                val input = view.text.toString()
                return input.isNotEmpty()
            }
        }
        throw ViewNotSupportedByValidator(view, this)
    }

    override fun getErrorMsg(): String =
        AppComponent.appContext.getString(R.string.form_validation__non_empty_message)
}