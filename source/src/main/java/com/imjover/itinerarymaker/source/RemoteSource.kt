/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source

import com.imjover.itinerarymaker.entity.*
import com.imjover.itinerarymaker.source.remote.*

/**
 * Represents the remote data source.
 */
interface RemoteSource {

    /**
     * Retrieves the remote source of [User].
     */
    fun getUserRemoteSource(): UserRemoteSource

    /**
     * Retrieves the remote source of [Travel].
     */
    fun getTravelRemoteSource(): TravelRemoteSource

    /**
     * Retrieves the remote source of [Itinerary].
     */
    fun getItineraryRemoteSource(): ItineraryRemoteSource

    /**
     * Retrieves the remote source of [Destination].
     */
    fun getDestinationRemoteSource(): DestinationRemoteSource

    /**
     * Retrieves the remote source of [Expense].
     */
    fun getExpenseRemoteSource(): ExpenseRemoteSource

    /**
     * Retrieves the remote source of [Notification].
     */
    fun getNotificationRemoteSource(): NotificationRemoteSource
}