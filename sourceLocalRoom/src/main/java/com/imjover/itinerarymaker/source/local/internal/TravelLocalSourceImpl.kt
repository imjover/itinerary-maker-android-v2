/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.internal

import com.imjover.itinerarymaker.entity.PaymentMode
import com.imjover.itinerarymaker.entity.Status
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.entity.Travel
import com.imjover.itinerarymaker.source.local.TravelLocalSource
import com.imjover.itinerarymaker.source.local.room.DatabaseFactory
import com.imjover.itinerarymaker.source.local.room.dao.TravelDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import java.util.*

internal class TravelLocalSourceImpl(
    private val travelDao: TravelDao = DatabaseFactory.getDatabase().travelDao()
) : AbstractLocalCrudSource<Travel>(travelDao),
    TravelLocalSource {

    @ExperimentalCoroutinesApi
    override fun getStreamChildrenOf(parentId: String): Flow<List<Travel>> =
        travelDao.getStreamTravelsOf(parentId)
            .distinctUntilChanged()

    override suspend fun getListChildrenOf(parentId: String): List<Travel> =
        travelDao.getTravelsOf(parentId)

    override suspend fun getListChildrenOf(parentId: String, syncStatus: SyncStatus): List<Travel> =
        travelDao.getTravelsOf(parentId, syncStatus)

    override suspend fun updateId(toUpdate: Travel, newId: String, syncStatus: SyncStatus) {
        travelDao.updateId(toUpdate.id, newId, syncStatus)
    }

    override suspend fun getStatus(travelId: String): Status =
        travelDao.getStatus(travelId)

    override suspend fun cancel(travelId: String) {
        travelDao.updateStatus(travelId, Status.CANCELLED)
    }

    override suspend fun updatePaymentMode(
        travelId: String,
        paymentMode: PaymentMode,
        lastUpdatedBy: String,
        lastUpdated: Date
    ) {
        travelDao.updatePaymentMode(travelId, paymentMode, lastUpdatedBy, lastUpdated, Status.PAID)
    }
}