/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.viewmodel.login

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.BuildConfig
import com.imjover.itinerarymaker.common.di.AppComponent
import com.imjover.itinerarymaker.entity.UserType
import com.imjover.itinerarymaker.helper.getMessageForUI
import com.imjover.itinerarymaker.traveler.R
import com.imjover.itinerarymaker.traveler.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.traveler.view.login.LoginActivity
import com.imjover.itinerarymaker.viewmodel.login.AbstractLoginViewModel
import kotlinx.coroutines.launch


/**
 * The view model of [LoginActivity].
 */
class LoginViewModel : AbstractLoginViewModel() {

    override fun injectDependencies() {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Do fake login, this is to fetch the logged in user in Firebase
     */
    override fun doFakeLogin() {
        _errorMessage.value = ""
        viewModelScope.launch {
            userManager.findUserByLogin("", "").fold({
                if (it.userType == UserType.TRAVELER && it.isApproved) {
                    // Valid user
                    _isLoggedIn.value = true
                    return@fold
                }
                // INVALID USER
                _errorMessage.value =
                    AppComponent.appContext.getString(R.string.login_activity__unauthorized_user__message)
                logout()
            }, {
                Log.w(toString(), it)
                _errorMessage.value = it.getMessageForUI()
            })
        }
    }

    override fun toString(): String =
        if (BuildConfig.DEBUG) "LoginViewModel" else super.toString()
}