/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.internal

import com.imjover.itinerarymaker.entity.Itinerary
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.source.local.ItineraryLocalSource
import com.imjover.itinerarymaker.source.local.room.DatabaseFactory
import com.imjover.itinerarymaker.source.local.room.dao.ItineraryDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged

internal class ItineraryLocalSourceImpl(
    private val itineraryDao: ItineraryDao = DatabaseFactory.getDatabase().itineraryDao()
) : AbstractLocalCrudSource<Itinerary>(itineraryDao),
    ItineraryLocalSource {

    @ExperimentalCoroutinesApi
    override fun getStreamChildrenOf(parentId: String): Flow<List<Itinerary>> =
        itineraryDao.getStreamItinerariesOf(parentId)
            .distinctUntilChanged()

    override suspend fun getListChildrenOf(parentId: String): List<Itinerary> =
        itineraryDao.getItinerariesOf(parentId)

    override suspend fun getListChildrenOf(parentId: String, syncStatus: SyncStatus): List<Itinerary> =
        itineraryDao.getItinerariesOf(parentId, syncStatus)

    override suspend fun updateId(toUpdate: Itinerary, newId: String, syncStatus: SyncStatus) {
        itineraryDao.updateId(toUpdate.id, newId, syncStatus)
    }
}