/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.local.internal

import com.imjover.itinerarymaker.entity.Notification
import com.imjover.itinerarymaker.entity.SyncStatus
import com.imjover.itinerarymaker.source.local.NotificationLocalSource
import com.imjover.itinerarymaker.source.local.room.DatabaseFactory
import com.imjover.itinerarymaker.source.local.room.dao.NotificationDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged


internal class NotificationLocalSourceImpl(
    private val notificationDao: NotificationDao = DatabaseFactory.getDatabase().notificationDao()
) : AbstractLocalCrudSource<Notification>(notificationDao),
    NotificationLocalSource {

    override fun getNewNotificationCountOf(recipient: String): Flow<Int> =
        notificationDao.getNewNotificationCountOf(recipient)

    override suspend fun setAsSeen(id: String, syncStatus: SyncStatus) {
        notificationDao.setAsSeen(id, syncStatus)
    }


    /***********************************************************************************************
     * Required functions of [AbstractLocalCrudSource].
     **********************************************************************************************/

    @ExperimentalCoroutinesApi
    override fun getStreamChildrenOf(parentId: String): Flow<List<Notification>> =
        notificationDao.getStreamOfNotifications(parentId)
            .distinctUntilChanged()

    override suspend fun getListChildrenOf(
        parentId: String,
        syncStatus: SyncStatus
    ): List<Notification> =
        notificationDao.getNotificationsOf(parentId, syncStatus)

    override suspend fun getListChildrenOf(parentId: String): List<Notification> =
        notificationDao.getNotificationsOf(parentId)

    override suspend fun updateId(toUpdate: Notification, newId: String, syncStatus: SyncStatus) {
        throw IllegalStateException()
    }
}