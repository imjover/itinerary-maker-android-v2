/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.agency.viewmodel.destination

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.imjover.itinerarymaker.agency.di.ViewModelComponentFactory
import com.imjover.itinerarymaker.entity.Destination
import com.imjover.itinerarymaker.entity.DestinationType
import com.imjover.itinerarymaker.entity.TransportationMode
import com.imjover.itinerarymaker.manager.DestinationManager
import com.imjover.itinerarymaker.agency.view.destination.DestinationFormFragment
import com.imjover.itinerarymaker.viewmodel.AbstractViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject


/**
 * The view model of [DestinationFormFragment].
 */
class DestinationFormViewModel : AbstractViewModel() {

    @Inject lateinit var destinationManager: DestinationManager
    internal lateinit var parentIds: Array<String>
    private val _oldDestination: MutableLiveData<Destination> = MutableLiveData()
    val oldDestination: LiveData<Destination>
        get() = _oldDestination
    private val _dismiss: MutableLiveData<Void> = MutableLiveData()
    val dismiss: LiveData<Void>
        get() = _dismiss
    val isLoading = ObservableBoolean()

    init {
        ViewModelComponentFactory.singleton().inject(this)
    }

    /**
     * Retrieves the details of the [Destination].
     *
     * @param destinationId The unique ID of the destination.
     */
    fun getDestination(destinationId: String) {
        viewModelScope.launch {
            _oldDestination.value = destinationManager.getDestination(parentIds, destinationId)
        }
    }

    /**
     * Inserts a [Destination].
     *
     * @param description The label to be used for this record.
     * @param timestamp The date of travel.
     * @param destinationType The type of destination.
     * @param transportationMode The transportation mode to be used for this travel.
     * @param latitude The destination's latitude.
     * @param longitude The destination's longitude.
     */
    fun insert(
        description: String,
        timestamp: Date,
        destinationType: DestinationType,
        transportationMode: TransportationMode,
        latitude: Double,
        longitude: Double
    ) {
        viewModelScope.launch {
            isLoading.set(true)
            destinationManager.insert(
                parentIds,
                description,
                timestamp,
                destinationType,
                transportationMode,
                latitude,
                longitude
            )
            _dismiss.value = null
            isLoading.set(false)
        }
    }

    /**
     * Updates a [Destination].
     *
     * @param description The label to be used for this record.
     * @param timestamp The date of travel.
     * @param destinationType The type of destination.
     * @param transportationMode The transportation mode to be used for this travel.
     */
    fun update(
        description: String,
        timestamp: Date,
        destinationType: DestinationType,
        transportationMode: TransportationMode
    ) {
        viewModelScope.launch {
            isLoading.set(true)
            val destination = oldDestination.value!!
            destinationManager.update(
                parentIds,
                destination,
                description,
                timestamp,
                destinationType,
                transportationMode,
                destination.latitude,
                destination.longitude
            )
            _dismiss.value = null
            isLoading.set(false)
        }
    }
}