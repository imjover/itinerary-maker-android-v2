/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.traveler.view.login

import android.app.Activity
import com.imjover.itinerarymaker.traveler.view.main.MainActivity
import com.imjover.itinerarymaker.traveler.viewmodel.login.LoginViewModel
import com.imjover.itinerarymaker.view.AbstractActivity
import com.imjover.itinerarymaker.view.login.AbstractLoginActivity
import com.imjover.itinerarymaker.view.login.BaseLoginActivity


/**
 * The login screen.
 */
class LoginActivity : AbstractLoginActivity<LoginViewModel>() {

    /***********************************************************************************************
     * Required functions of [BaseLoginActivity].
     **********************************************************************************************/

    override fun getClassOfNextActivity(): Class<Activity> =
        MainActivity::class.java as Class<Activity>


    /***********************************************************************************************
     * Required functions of [AbstractActivity].
     **********************************************************************************************/

    override fun getViewModelClass(): Class<LoginViewModel> =
        LoginViewModel::class.java
}