/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

/**
 * The type of notification.
 *
 * @param id The unique ID.
 */
enum class NotificationType(val id: String) {

    /**
     * An agent sent to a traveler his new travel for review.
     */
    TRAVEL_REVIEW("travel_review");


    companion object {
        /**
         * Retrieves the equivalent [NotificationType] base on the given ID.
         *
         * @param id The id of the [NotificationType].
         * @return The retrieved [NotificationType].
         */
        fun fromId(id: String): NotificationType {
            values().forEach {
                if (it.id == id) {
                    return it
                }
            }
            throw IllegalStateException()
        }
    }
}