/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.entity

import android.net.Uri
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


/**
 * The user of the application.
 *
 * @param id The unique ID and primary key.
 * @param username The username.
 * @param name The name of the user.
 * @param userType The user type.
 * @param isApproved Whether the user has been approved to be used, then true, else false.
 */
@Entity(tableName = "tbl_user")
data class User(
    @PrimaryKey @ColumnInfo(name = "pk_user_id") val id: String,
    @ColumnInfo(name = "username") val username: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "photo_url") val photoUrl: Uri?,
    @ColumnInfo(name = "user_type") val userType: UserType,
    @ColumnInfo(name = "is_approved") val isApproved: Boolean
) : com.imjover.itinerarymaker.entity.Entity {

    override fun getUniqueId(): String =
        id
}