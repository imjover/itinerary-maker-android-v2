/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.source.remote.mock

/**
 * Holds the configurations on mocking.
 */
internal class MockConfig {

    companion object {
        const val DELAY_TIME = 1000L // in milliseconds
    }
}