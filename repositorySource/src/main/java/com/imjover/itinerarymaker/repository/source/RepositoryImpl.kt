/*
 * Copyright (c) 2020. John Oliver Magdaleno, All rights reserved.
 */

package com.imjover.itinerarymaker.repository.source

import com.imjover.itinerarymaker.repository.*
import com.imjover.itinerarymaker.repository.source.di.RepositoryComponentFactory
import com.imjover.itinerarymaker.repository.source.internal.*
import com.imjover.itinerarymaker.repository.source.internal.DestinationRepositoryImpl
import com.imjover.itinerarymaker.repository.source.internal.ExpenseRepositoryImpl
import com.imjover.itinerarymaker.repository.source.internal.ItineraryRepositoryImpl
import com.imjover.itinerarymaker.repository.source.internal.TravelRepositoryImpl
import com.imjover.itinerarymaker.repository.source.internal.UserRepositoryImpl
import com.imjover.itinerarymaker.source.LocalSource
import com.imjover.itinerarymaker.source.RemoteSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Holds the instances of dao-access-objects (DAOs).
 */
internal class RepositoryImpl(
    defaultDispatcher: CoroutineDispatcher = Dispatchers.IO
) : Repository {

    @Inject internal lateinit var localSource: LocalSource
    @Inject internal lateinit var remoteSource: RemoteSource
    private val userRepository: UserRepository
    private val travelRepository: TravelRepository
    private val itineraryRepository: ItineraryRepository
    private val destinationRepository: DestinationRepository
    private val expenseRepository: ExpenseRepository
    private val notificationRepository: NotificationRepository

    init {
        RepositoryComponentFactory.singleton().inject(this)
        userRepository = UserRepositoryImpl(
            localSource.getUserLocalSource(),
            remoteSource.getUserRemoteSource(),
            defaultDispatcher
        )
        travelRepository = TravelRepositoryImpl(
            localSource.getTravelLocalSource(),
            remoteSource.getTravelRemoteSource(),
            defaultDispatcher
        )
        itineraryRepository = ItineraryRepositoryImpl(
            localSource.getItineraryLocalSource(),
            remoteSource.getItineraryRemoteSource(),
            defaultDispatcher
        )
        destinationRepository = DestinationRepositoryImpl(
            localSource.getDestinationLocalSource(),
            remoteSource.getDestinationRemoteSource(),
            defaultDispatcher
        )
        expenseRepository = ExpenseRepositoryImpl(
            localSource.getExpenseLocalSource(),
            remoteSource.getExpenseRemoteSource(),
            defaultDispatcher
        )
        notificationRepository = NotificationRepositoryImpl(
            localSource.getNotificationLocalSource(),
            remoteSource.getNotificationRemoteSource(),
            defaultDispatcher
        )
    }

    override fun getUserRepository(): UserRepository = userRepository

    override fun getTravelRepository(): TravelRepository = travelRepository

    override fun getItineraryRepository(): ItineraryRepository = itineraryRepository

    override fun getDestinationRepository(): DestinationRepository = destinationRepository

    override fun getExpenseRepository(): ExpenseRepository = expenseRepository

    override fun getNotificationRepository(): NotificationRepository = notificationRepository
}